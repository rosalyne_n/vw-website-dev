package com.vw.cn.dto;

public class CarModelDto {
	
	private String code;
	private String name;
	private String imageUrl;
	private float rrp;
	private String currencyCode;
	private String formatedRRP;
	private String series_code;
	private String url;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public float getRrp() {
		return rrp;
	}
	public void setRrp(float rrp) {
		this.rrp = rrp;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getFormatedRRP() {
		return formatedRRP;
	}
	public void setFormatedRRP(String formatedRRP) {
		this.formatedRRP = formatedRRP;
	}
	public String getSeries_code() {
		return series_code;
	}
	public void setSeries_code(String series_code) {
		this.series_code = series_code;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}


}
