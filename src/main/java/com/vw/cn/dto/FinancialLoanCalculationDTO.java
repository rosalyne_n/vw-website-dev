package com.vw.cn.dto;

public class FinancialLoanCalculationDTO {
	
	private String total_amount;
	private String down_payment_amount;
	private String loan_tenure;
	private String interest_amount;
	private String emi;
	
	
	
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getDown_payment_amount() {
		return down_payment_amount;
	}
	public void setDown_payment_amount(String down_payment_amount) {
		this.down_payment_amount = down_payment_amount;
	}
	public String getLoan_tenure() {
		return loan_tenure;
	}
	public void setLoan_tenure(String loan_tenure) {
		this.loan_tenure = loan_tenure;
	}
	public String getInterest_amount() {
		return interest_amount;
	}
	public void setInterest_amount(String interest_amount) {
		this.interest_amount = interest_amount;
	}
	public String getEmi() {
		return emi;
	}
	public void setEmi(String emi) {
		this.emi = emi;
	}
	
	
	

}
