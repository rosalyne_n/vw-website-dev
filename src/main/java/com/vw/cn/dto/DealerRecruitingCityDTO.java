package com.vw.cn.dto;

public class DealerRecruitingCityDTO {
	
	private long id;	

	private String province;
	
	private String district;
	
	private String city;
	
	private String recruitmentType;
	
	private int page;
	
	private int rowsPerPage;
	
	private String remarks;
	
	private long recruitmentNumber;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRecruitmentType() {
		return recruitmentType;
	}

	public void setRecruitmentType(String recruitmentType) {
		this.recruitmentType = recruitmentType;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getRecruitmentNumber() {
		return recruitmentNumber;
	}

	public void setRecruitmentNumber(long recruitmentNumber) {
		this.recruitmentNumber = recruitmentNumber;
	}
}