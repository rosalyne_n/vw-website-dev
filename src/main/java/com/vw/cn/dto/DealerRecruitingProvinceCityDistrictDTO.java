package com.vw.cn.dto;

import java.util.List;

public class DealerRecruitingProvinceCityDistrictDTO {
	
	private List<DealerProvinceDTO> recruiting_cities;

	public List<DealerProvinceDTO> getRecruiting_cities() {
		return recruiting_cities;
	}

	public void setRecruiting_cities(List<DealerProvinceDTO> recruiting_cities) {
		this.recruiting_cities = recruiting_cities;
	}
	

}
