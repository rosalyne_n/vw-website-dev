package com.vw.cn.dto;

import java.util.List;

public class CarSeriesDto {
	private String code;
	private String name;
	private String lang;
	private boolean s_selected;
	
	List<CarModelDto> listModel;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public List<CarModelDto> getListModel() {
		return listModel;
	}
	public void setListModel(List<CarModelDto> listModel) {
		this.listModel = listModel;
	}
	public boolean isS_selected() {
		return s_selected;
	}
	public void setS_selected(boolean s_selected) {
		this.s_selected = s_selected;
	}

}
