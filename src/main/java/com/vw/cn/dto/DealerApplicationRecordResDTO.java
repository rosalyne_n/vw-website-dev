package com.vw.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class DealerApplicationRecordResDTO {
	int totalRecordCount;
	
	List<DealerRecruitmentDTO> applicationRecords = new ArrayList<DealerRecruitmentDTO>();
	
	public int getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	public List<DealerRecruitmentDTO> getApplicationRecords() {
		return applicationRecords;
	}
	public void setApplicationRecords(List<DealerRecruitmentDTO> applicationRecords) {
		this.applicationRecords = applicationRecords;
	}
	

}
