package com.vw.cn.dto;

import java.util.List;

public class DealerProvinceDTO {
	private String province_name;
	private List<DealerCityDTO> cities;
	
	public String getProvince_name() {
		return province_name;
	}
	public void setProvince_name(String province_name) {
		this.province_name = province_name;
	}
	public List<DealerCityDTO> getCities() {
		return cities;
	}
	public void setCities(List<DealerCityDTO> cities) {
		this.cities = cities;
	}
	

}
