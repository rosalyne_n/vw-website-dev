package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.dao.GenericDetailsDao;
import com.vw.cn.domain.GenericDetails;

@Service
public class GenericService implements IGenericService{
@Autowired
GenericDetailsDao genericData;
	@Override
	public void insertGenericDetails(GenericDetails genericDetails) {
		genericData.insertGenericInfo(genericDetails);
		
	}
	@Override
	public List<GenericDetails> getGenericDetails(String discr) {
			return genericData.getGenericDetails(discr);
	}

}
