package com.vw.cn.service;

import java.util.List;

import com.vw.cn.customDomain.DistrictCityProvince;
import com.vw.cn.customDomain.ProvinceAndCity;
import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.DealerType;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;
import com.vw.cn.domain.SourceSite;
/**
 * @author karthikeyan_v
 */
public interface IMasterDataService{

	// Role

	void insertRole(Role role);

	void updateRole(Role role);

	Role findRoleById(long id);

	List<Role> findAllRoles();	

	void deleteRole(long id);

	// Language

	void insertLanguage(Language lang);

	void updateLanguage(Language lang);

	Language findLanguageById(long id);

	Language findLanguageByCode(String code);

	List<Language> findAllLanguages();	

	void deleteLanguage(long id);

	// Province

	void insertProvince(Province province);

	void updateProvince(Province province);

	Province findProvinceById(long id);

	Province findProvinceByCode(String code,String lang);

	List<Province> findAllProvinces(String lang);	

	void deleteProvince(long id);

	// City

	void insertCity(City city);

	void updateCity(City city);

	City findCityById(long id);
	
	City findCityByName(String name);

	City findCityByCode(String code,String lang);

	List<City> findAllCities(String lang);	

	void deleteCity(long id);

	// District

	void insertDistrict(District district);

	void updateDistrict(District district);

	District findDistrictById(long id);

	District findDistrictByCode(String code,String lang);

	List<District> findAllDistricts(String lang);	

	void deleteDistrict(long id);

	// RecruitmentNetworkType

	void insertRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	void updateRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	RecruitmentNetworkType findRecruitmentNetworkTypeById(long id);

	RecruitmentNetworkType findRecruitmentNetworkTypeByCode(String code,String lang);

	List<RecruitmentNetworkType> findAllRecruitmentNetworkTypes(String lang);	

	void deleteRecruitmentNetworkType(long id);

	// Currency
	void insertCurrency(Currency currency);

	void updateCurrency(Currency currency);

	Currency findCurrencyById(long id);

	List<Currency> findAllCurrencys();	

	void deleteCurrency(long id);

	List<ProvinceAndCity> getallprovinceAndCities(String lang);	

	//## District City Province
	List<DistrictCityProvince> getAllDistrictCityProvince(String lang);

	//## SourceSite

	void insertSourceSite(SourceSite sourceSite);

	void updateSourceSite(SourceSite sourceSite);

	SourceSite findSourceSiteById(long id);

	SourceSite findSourceSiteByCode(String code,String lang);

	List<SourceSite> findAllSourceSites(String lang);	

	void deleteSourceSite(long id);

	//## Dealer type

	public void insertDealerType(DealerType dealerType);

	public void updateDealerType(DealerType dealerType);

	public DealerType findDealerTypeById(long id);

	public List<DealerType> findAllDealerTypes(String lang_code);

	public void deleteDealerType(long id);
}
