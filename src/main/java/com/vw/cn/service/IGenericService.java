package com.vw.cn.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.vw.cn.domain.GenericDetails;

@Service
public interface IGenericService {
	public void insertGenericDetails(GenericDetails genericDetails);
	List<GenericDetails> getGenericDetails(String discr);
}
