package com.vw.cn.service;

import java.util.List;

import com.vw.cn.domain.Budget;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.PurchaseOrder;
import com.vw.cn.domain.TestDrive;
/**
 * @author karthikeyan_v
 */
public interface ICarService{

	void insertCarModel(CarModel carModel);

	void updateCarModel(CarModel carModel);

	CarModel findCarModelById(long id);			

	List<CarModel> findAllCarModels(String lang);	

	void deleteCarModel(CarModel carModel);

	//Test Drive
	void insertTestDrive(TestDrive testDrive);

	void updateTestDrive(TestDrive testDrive);

	TestDrive findTestDriveById(long id);			

	List<TestDrive> findAllTestDrives();	

	void deleteTestDrive(long id);

	//Purchase Order
	void insertPurchaseOrder(PurchaseOrder carModel);

	void updatePurchaseOrder(PurchaseOrder carModel);

	PurchaseOrder findPurchaseOrderById(long id);			

	List<PurchaseOrder> findAllPurchaseOrders();	

	void deletePurchaseOrder(PurchaseOrder order);

	List<CarModel> findBudgetCarMaodels(Budget budget) throws Exception;

	//CarSeries
	void insertCarSeries(CarSeries carSeries);

	//CarSeries findCarSeriesById(long id);

	List<CarSeries> findAllCarSeries(String lang);
	
	List<CarModel> findAllCarModelBySeries(CarModel carmodel);
}