package com.vw.cn.service;

import java.util.List;
import com.vw.cn.domain.User;
import com.vw.cn.domain.UserDetail;
import com.vw.cn.domain.UserRole;
/**
 * @author karthikeyan_v
 */
public interface IUserService{

	void insertUser(User user);

	void updateUser(User user);

	User findUserById(long id);

	User findUserByAccount(String account);

	List<User> findAllUsers();	

	void deleteUser(User user);

	void insertUserDetail(UserDetail userDetail);

	void updateUserDetail(UserDetail userDetail);

	UserDetail findUserDetailById(long id);	

	List<UserDetail> findAllUserDetails();	

	void deleteUserDetail(UserDetail userDetail);

	void insertUserRole(UserRole userRole);

	void updateUserRole(UserRole userRole);

	UserRole findUserRoleById(long id);		

	UserRole findUserRoleByUserId(long user_id);		

	UserRole findUserRoleByRoleId(long role_id);		

	List<UserRole> findAllUserRoles();	

	void deleteUserRole(UserRole userRole);
}
