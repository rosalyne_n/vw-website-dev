package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.dao.CarDao;
import com.vw.cn.domain.Budget;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.PurchaseOrder;
import com.vw.cn.domain.TestDrive;
/**
 * @author karthikeyan_v
 */
@Service
public class CarService implements ICarService{

	@Autowired
	CarDao carDao;

	@Override
	public void insertCarModel(CarModel carModel) {
		carDao.insertCarModel(carModel);
	}

	@Override
	public void updateCarModel(CarModel carModel) {
		carDao.updateCarModel(carModel);
	}

	@Override
	public CarModel findCarModelById(long id) {
		return carDao.findCarModelById(id);
	}

	@Override
	public List<CarModel> findAllCarModels(String lang) {
		return carDao.findAllCarModels(lang);
	}

	@Override
	public void deleteCarModel(CarModel carModel) {
		carDao.deleteCarModel(carModel);		
	}

	@Override
	public void insertTestDrive(TestDrive testDrive) {
		carDao.insertTestDrive(testDrive);		
	}

	@Override
	public void updateTestDrive(TestDrive testDrive) {
		carDao.updateTestDrive(testDrive);		
	}

	@Override
	public TestDrive findTestDriveById(long id) {
		return carDao.findTestDriveById(id);
	}

	@Override
	public List<TestDrive> findAllTestDrives() {
		return carDao.findAllTestDrives();
	}

	@Override
	public void deleteTestDrive(long id) {
		carDao.deleteTestDrive(id);
	}

	@Override
	public void insertPurchaseOrder(PurchaseOrder carModel) {
		carDao.insertPurchaseOrder(carModel);
	}

	@Override
	public void updatePurchaseOrder(PurchaseOrder carModel) {
		carDao.updatePurchaseOrder(carModel);
	}

	@Override
	public PurchaseOrder findPurchaseOrderById(long id) {
		return carDao.findPurchaseOrderById(id);
	}

	@Override
	public List<PurchaseOrder> findAllPurchaseOrders() {
		return carDao.findAllPurchaseOrders();
	}

	@Override
	public void deletePurchaseOrder(PurchaseOrder order) {
		carDao.deletePurchaseOrder(order);
	}
	@Override
	public List<CarModel> findBudgetCarMaodels(Budget budget) throws Exception{
		return carDao.findBugetCarModels(budget);
	}

	@Override
	public void insertCarSeries(CarSeries carSeries){
		carDao.insertCarSeries(carSeries);
	}

	/*@Override
	public CarSeries findCarSeriesById(long id){
		return carDao.findCarSeriesById(id);
	}*/

	@Override
	public List<CarSeries> findAllCarSeries(String lang){
		return carDao.findAllCarSeries(lang);
	}
	@Override
	public List<CarModel> findAllCarModelBySeries(CarModel carmodel){
		return carDao.findAllCarModelBySeries(carmodel);
	}
}