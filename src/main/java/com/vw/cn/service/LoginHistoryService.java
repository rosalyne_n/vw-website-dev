package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.dao.LoginHistoryDao;
import com.vw.cn.domain.LoginHistory;
/**
 * @author karthikeyan_v
 */
@Service
public class LoginHistoryService implements ILoginHistoryService{

	@Autowired
	LoginHistoryDao loginHistoryDao;

	@Override
	public void insertLoginHistory(LoginHistory loginHistory) {
		loginHistoryDao.insertLoginHistory(loginHistory);
	}

	@Override
	public void updateLoginHistory(LoginHistory loginHistory) {
		loginHistoryDao.updateLoginHistory(loginHistory);
	}

	@Override
	public LoginHistory findLoginHistoryById(long id) {
		return loginHistoryDao.findLoginHistoryById(id);
	}

	@Override
	public LoginHistory findLoginHistoryByUserID(long id) {
		return loginHistoryDao.findLoginHistoryByUserID(id);
	}

	@Override
	public LoginHistory findLoginHistoryByToken(String token) {
		return loginHistoryDao.findLoginHistoryByToken(token);
	}

	@Override
	public List<LoginHistory> findAllLoginHistorys() {
		return loginHistoryDao.findAllLoginHistorys();
	}

	@Override
	public void deleteLoginHistory(long id) {
		loginHistoryDao.deleteLoginHistory(id);
	}
}