package com.vw.cn.service;


import com.vw.cn.dto.CarModelComparisionDto;
import com.vw.cn.dto.CarSeriesDto;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;

public interface ICarModelParameterService {
	
	ResponseMessages<CarSeriesDto> getCarSeriesAndModelsByLang(String lang);
	
	ResponseMessage<CarModelComparisionDto> compareCarModels(String modelcode1, String modelcode2, String modelcode3, String lang);

}
