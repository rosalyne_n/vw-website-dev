package com.vw.cn.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.dao.CarModelComparisonDao;
import com.vw.cn.dao.CarDao;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarModelParameter;
import com.vw.cn.domain.CarParameter;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.dto.CarModelComparisionDto;
import com.vw.cn.dto.CarModelDto;
import com.vw.cn.dto.CarModelParameterDisplayDTO;
import com.vw.cn.dto.CarParameterDto;
import com.vw.cn.dto.CarSeriesDto;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.ICarModelParameterService;
import com.vw.cn.utils.CarParameterType;

@Service
public class CarModelParameterService implements ICarModelParameterService {
	@Autowired 
	CarModelComparisonDao carModelComparisonDao; 
	
	@Autowired
	CarDao carDao;
	
	/**
	 * @param listModelcode
	 * @param lang
	 * @return the 
	 */
	public ResponseMessage<CarModelComparisionDto> compareCarModels(String modelcode1, String modelcode2, String modelcode3, String lang){
		ResponseStatus status = null;
		CarModelComparisionDto entity = null;
		
		if(null == modelcode1 && null == modelcode2){
			status=new ResponseStatus(ResponseStatusCode.CAR_MODEL2_CANNOT_BE_EMPTY, "Car model 1 and model 2 cannot be empty");
		} else {
			try{
				entity = prepareComparisonResponse(modelcode1, modelcode2, modelcode3, lang);
				if(null != entity){
					entity.setModel(getModel(modelcode1, lang));
					entity.setModel2(getModel(modelcode2, lang));
					if(null != modelcode3){
						entity.setModel3(getModel(modelcode3, lang));		
					}
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				} else {
					status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "No record found");
				}
			}
			catch (Exception e){				
				status=new ResponseStatus(ResponseStatusCode.ERROR_CAR_COMPARISON, "Car comparison error.");
			}
		}		
		return new ResponseMessage<CarModelComparisionDto>(status, entity);		
	}
	
	/**
	 * @param listModelcode
	 * @param lang
	 * @return the 
	 */
	public ResponseMessage<CarModelComparisionDto> getCarModelConfiguration(String modelcode, String lang){
		ResponseStatus status = null;
		CarModelComparisionDto entity = null;
		
		if(null == modelcode){
			status=new ResponseStatus(ResponseStatusCode.CAR_MODEL1_CANNOT_BE_EMPTY, "Car Model code cannot be empty");
		} else {
			try{
				entity = prepareComparisonResponse(modelcode, null, null, lang);
				if(null != entity){
					entity.setModel(getModel(modelcode, lang));
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				} else {
					status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "No record found");
				}
			}
			catch (Exception e){		
				
				status=new ResponseStatus(ResponseStatusCode.ERROR_CAR_COMPARISON, "Car comparison error.");
			}
		}		
		return new ResponseMessage<CarModelComparisionDto>(status, entity);		
	}
	
	/**
	 * 
	 * @param listModelcode
	 * @param lang
	 * @return
	 */
	private CarModelComparisionDto prepareComparisonResponse(String model_code1, String model_code2, String model_code3, String lang){
		
		CarModelComparisionDto modelDetails = new CarModelComparisionDto();
		List<CarModelParameterDisplayDTO> basics = prepareComparisonResultByType(model_code1, model_code2, model_code3, lang, CarParameterType.STANDARD);
		modelDetails.setBasics(basics);
		List<CarModelParameterDisplayDTO> exteriors = prepareComparisonResultByType(model_code1, model_code2, model_code3, lang, CarParameterType.EXTERIOR);
		modelDetails.setExteriors(exteriors);
		List<CarModelParameterDisplayDTO> interiors = prepareComparisonResultByType(model_code1, model_code2, model_code3, lang, CarParameterType.INTERIOR);
		modelDetails.setInteriors(interiors);
		List<CarModelParameterDisplayDTO> comforts = prepareComparisonResultByType(model_code1, model_code2, model_code3, lang, CarParameterType.COMFORT);
		modelDetails.setComforts(comforts);
		List<CarModelParameterDisplayDTO> safetyequipments = prepareComparisonResultByType(model_code1, model_code2, model_code3, lang, CarParameterType.SAFETY);
		modelDetails.setSafetyequipments(safetyequipments);		
		
		return modelDetails;
	}
	
	private List<CarModelParameterDisplayDTO> prepareComparisonResultByType(String model_code1, String model_code2, String model_code3, String lang, String parameterType){
		
		List<CarModelParameterDisplayDTO> listCarModelParameterDisplay = new ArrayList<CarModelParameterDisplayDTO>();
		Map<String, CarParameterDto> model1ParamMap = null;
		Map<String, CarParameterDto> model2ParamMap = null;
		Map<String, CarParameterDto> model3ParamMap = null;			
		
		if(null !=model_code1){			
			model1ParamMap = getCarParameters(model_code1, lang, parameterType);				
		}
		
		if(null !=model_code2){
			model2ParamMap = getCarParameters(model_code2, lang, parameterType);
		}
		
		if(null !=model_code3){
			model3ParamMap = getCarParameters(model_code3, lang, parameterType);
		}
		
		List<CarParameter>  listParams = carModelComparisonDao.findCarParametersByTypeAndLang(parameterType, lang);
		for (CarParameter carParameter : listParams) {
			CarModelParameterDisplayDTO carModelParameterDisplayDTO = new CarModelParameterDisplayDTO();
			carModelParameterDisplayDTO.setParameterCode(carParameter.getCode());
			carModelParameterDisplayDTO.setParameterName(carParameter.getName());
			if(null != model1ParamMap){
				CarParameterDto car1 = model1ParamMap.get(carParameter.getCode());
				carModelParameterDisplayDTO.setCar1(car1);
			}
			if(null != model2ParamMap){
				CarParameterDto car2 = model2ParamMap.get(carParameter.getCode());
				carModelParameterDisplayDTO.setCar2(car2);
			}
			if(null != model3ParamMap){
				CarParameterDto car3 = model3ParamMap.get(carParameter.getCode());
				carModelParameterDisplayDTO.setCar3(car3);
			}
			listCarModelParameterDisplay.add(carModelParameterDisplayDTO);
		}		
		return listCarModelParameterDisplay;
	}
	
	
	
	/**
	 * Get the Model by model code and language code
	 * @param modelCode
	 * @param lang
	 * @return the modelDto
	 */
	private CarModelDto getModel(String modelCode, String lang){
		CarModelDto modelDto = new CarModelDto();
		CarModel carModel = carModelComparisonDao.getCarModelByCodeAndLanguage(modelCode, lang);
		modelDto.setCode(carModel.getCode());
		modelDto.setSeries_code(carModel.getSeries_code());
		modelDto.setName(carModel.getName());
		modelDto.setImageUrl(carModel.getImage_url());
		modelDto.setRrp(carModel.getRecommended_retail_price());
		modelDto.setCurrencyCode(carModel.getCurrency_code());
		/*CarSeriesDto seriesDto = new CarSeriesDto();
		CarSeries carSeries = carModelComparisonDao.findCarSeriesByCodeAndLang(carModel.getSeries_code(), lang);
		seriesDto.setName(carSeries.getName());
		seriesDto.setCode(carModel.getSeries_code());*/
		//modelDto.setSeries(seriesDto);
		return modelDto;
	}
	
	/**
	 * This method get the car standard parameters based on the model code and the lang
	 * @param modelCode
	 * @param lang
	 * @return carParamMap the standard parameters Map
	 */
	private Map<String, CarParameterDto> getCarParameters(String modelCode, String lang, String paramType){
		
		Map<String,CarParameterDto> carParamMap = new HashMap<String, CarParameterDto>();
	
		List<CarModelParameter> carParamValues = carModelComparisonDao.getCarParametersByModelAndType(modelCode, paramType, lang);
		if(null == carParamValues){
			
		} else {
			
			for (int i=0; i< carParamValues.size(); i++){
				
				CarModelParameter carModelParameter = carParamValues.get(i);				
				String parameterKey = carModelParameter.getParam_code();
				if(!carParamMap.containsKey(parameterKey)){
					CarParameterDto carParameterDto = populateCarParameterDto(carModelParameter); 
					carParamMap.put(parameterKey, carParameterDto);
				}
				else {
					CarParameterDto carParameterDto = carParamMap.get(parameterKey);
					carParameterDto.setValue(carParameterDto.getValue() + "<br/>" + carModelParameter.getValue() );
				}				 
			}			
		}
					
		return carParamMap;
	}
	
	
	/**
	 * Populate the CarParameterDto bean from the CarModelParameter bean
	 * @param carModelParameter
	 * @return
	 */
	private CarParameterDto populateCarParameterDto(CarModelParameter carModelParameter){
		CarParameterDto carParameterDto = new CarParameterDto();
		carParameterDto.setCode(carModelParameter.getParam_code());
		carParameterDto.setDisplayCircle(carModelParameter.isDisplayImage());
		carParameterDto.setCarParameterType(carModelParameter.getParam_type());
		carParameterDto.setValue(carModelParameter.getValue());
		carParameterDto.setCategory(carModelParameter.getCategory());
		carParameterDto.setImageUrl(carModelParameter.getImageUrl());
		carParameterDto.setDisplayImage(carModelParameter.isDisplayImage());		
		return carParameterDto;
	}
	
	
	/**
	 * @param lang
	 * @return
	 */
	public ResponseMessages<CarSeriesDto> getCarSeriesAndModelsByLang(String lang){
		ResponseStatus status = null;
		List<CarSeriesDto> entities = null;
		List<CarSeries> listCarSeries = carDao.findAllCarSeries(lang);
		if(null != listCarSeries && listCarSeries.size() >0) {
			entities =  new ArrayList<CarSeriesDto> ();
			for (CarSeries carSeries : listCarSeries) {
				if(carSeries != null && carSeries.getCode()!= null){
					
					CarSeriesDto carSeriesDto = new CarSeriesDto();
					carSeriesDto.setCode(carSeries.getCode());
					carSeriesDto.setName(carSeries.getName());
					CarModel carModel = new CarModel();
					carModel.setSeries_code(carSeries.getCode());
					carModel.setLang(lang);
					carSeriesDto.setListModel(getListCarModelDto(carModel));
					entities.add(carSeriesDto);
				}
				
			}
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		else {
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "No record found");
		}
		return new ResponseMessages<CarSeriesDto>(status, entities);	
	}
	
	/**
	 * 
	 * @param carModel
	 * @return
	 */
	private List<CarModelDto> getListCarModelDto(CarModel carModel){
		List<CarModelDto> listModelDto = new ArrayList<CarModelDto>();		
		List<CarModel> listModel = carDao.findAllCarModelBySeries(carModel);
		for (CarModel carModel2 : listModel) {
			CarModelDto carModelDto = new CarModelDto();
			carModelDto.setCode(carModel2.getCode());
			carModelDto.setSeries_code(carModel2.getSeries_code());
			carModelDto.setName(carModel2.getName());
			carModelDto.setRrp(carModel2.getRecommended_retail_price());
			carModelDto.setUrl(carModel2.getUrl());
			//parseFloat(Math.round(num3 * 100) / 100).toFixed(2);
			carModelDto.setImageUrl(carModel2.getImage_url());
			listModelDto.add(carModelDto);
		}
		
		return listModelDto;
		
	}
}
