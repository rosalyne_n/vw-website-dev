package com.vw.cn.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import java.security.Principal;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.DealerCityProvince;
import com.vw.cn.customDomain.DealerRecordList;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.DealerRecruitingCity;
import com.vw.cn.domain.DealerRecruitment;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.SearchDealer;
import com.vw.cn.domain.ServiceRating;
import com.vw.cn.domain.User;
import com.vw.cn.dto.DealerCityDTO;
import com.vw.cn.dto.DealerDistrictDTO;
import com.vw.cn.dto.DealerProvinceDTO;
import com.vw.cn.dto.DealerRecruitingCityDTO;
import com.vw.cn.dto.DealerRecruitingCityResponseDTO;
import com.vw.cn.dto.DealerRecruitingProvinceCityDistrictDTO;
import com.vw.cn.dto.DealerRecruitmentDTO;
import com.vw.cn.dto.DealerApplicationRecordResDTO;
import com.vw.cn.response.ResponseListMessage;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.IDealerService;
import com.vw.cn.service.IUserService;
import com.vw.cn.utils.CommonUtils;
/**
 * @author karthikeyan_v
 */
@Service
public class DealerServiceHelper {

	@Autowired 
	private IDealerService dealerService;	

	@Autowired
	private IUserService userService;

	CommonUtils commonutills = CommonUtils.getInstance();

	public long getUserId(final HttpServletRequest request){
		Principal principal = request.getUserPrincipal();
		User user=userService.findUserByAccount(principal.getName());
		return user.getId();
	}

	//Register new dealer
	public ResponseMessage<Dealer> createdealer(Dealer dealerObj){
		ResponseStatus status = null;
		Dealer entity=null;
		try{
			entity=new Dealer();
			entity.setCity(dealerObj.getCity());
			entity.setProvince(dealerObj.getProvince());
			entity.setName(dealerObj.getName());
			entity.setFull_name(dealerObj.getFull_name());
			entity.setAddress(dealerObj.getAddress());
			entity.setDealer_type_code(dealerObj.getDealer_type_code());
			entity.setCreated_date(new Date());
			entity.setUpdate_date(new Date());
			entity.setLattitude(dealerObj.getLattitude());
			entity.setLongitude(dealerObj.getLongitude());
			entity.setWebsite_url(dealerObj.getWebsite_url());
			entity.setPromotion_image_url(dealerObj.getPromotion_image_url());
			entity.setPromotion_page_url(dealerObj.getPromotion_page_url());
			entity.setSales_hotline(dealerObj.getSales_hotline());
			entity.setCustomer_hotline(dealerObj.getCustomer_hotline());
			entity.setAfter_sales(dealerObj.getAfter_sales());
			entity.setPostal_code(dealerObj.getPostal_code());

			dealerService.insertDealer(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Dealer>(status, entity);		
	}

	//update dealer
	public ResponseMessage<Dealer> updatedealer(Dealer dealerObj){
		ResponseStatus status = null;
		Dealer entity=null;
		try{
			entity = dealerService.findDealerById(dealerObj.getId());
			if(entity!=null){	
				entity.setCity(dealerObj.getCity());
				entity.setProvince(dealerObj.getProvince());
				entity.setName(dealerObj.getName());
				entity.setFull_name(dealerObj.getFull_name());
				entity.setAddress(dealerObj.getAddress());
				entity.setDealer_type_code(dealerObj.getDealer_type_code());
				entity.setCreated_date(dealerObj.getCreated_date());
				entity.setUpdate_date(new Date());
				entity.setLattitude(dealerObj.getLattitude());
				entity.setLongitude(dealerObj.getLongitude());
				entity.setWebsite_url(dealerObj.getWebsite_url());
				entity.setPromotion_image_url(dealerObj.getPromotion_image_url());
				entity.setPromotion_page_url(dealerObj.getPromotion_page_url());
				entity.setSales_hotline(dealerObj.getSales_hotline());
				entity.setCustomer_hotline(dealerObj.getCustomer_hotline());
				entity.setAfter_sales(dealerObj.getAfter_sales());
				entity.setPostal_code(dealerObj.getPostal_code());

				dealerService.updateDealer(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Dealer>(status, entity);
	}

	//View dealer
	public ResponseMessage<Dealer> viewdealer(long id){
		ResponseStatus status = null;
		Dealer entity =null;
		try{
			entity = dealerService.findDealerById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Dealer>(status, entity);
	}	

	//get all dealers
	public ResponseMessages<Dealer> getalldealers(){
		ResponseStatus status = null;
		List<Dealer> entity =null;
		try{
			entity = dealerService.findAllDealers();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Dealer>(status, entity);
	}

	// Dealer Registration Contact controllers
	//Register new dealerContact
	public ResponseMessage<DealerContact> createdealercontact(DealerContact dealerContactObj){
		ResponseStatus status = null;
		DealerContact entity=null;
		try{
			entity=new DealerContact();
			entity.setName(dealerContactObj.getName());
			entity.setPhone(dealerContactObj.getPhone());
			entity.setEmail(dealerContactObj.getEmail());
			entity.setJob_title(dealerContactObj.getJob_title());
			entity.setCity(dealerContactObj.getCity());
			entity.setLast_modified_by(dealerContactObj.getLast_modified_by());
			entity.setDealer_id(dealerContactObj.getDealer_id());
			entity.setCreated_date(new Date());
			entity.setLast_modified_date(new Date());
			entity.setContact_type(dealerContactObj.getContact_type());
			dealerService.insertDealerContact(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<DealerContact>(status, entity);		
	}

	//update dealerContact
	public ResponseMessage<DealerContact> updatedealercontact(DealerContact dealerContactObj){
		ResponseStatus status = null;
		DealerContact entity=null;
		try{
			entity = dealerService.findDealerContactById(dealerContactObj.getId());
			if(entity!=null){
				entity.setName(dealerContactObj.getName());
				entity.setPhone(dealerContactObj.getPhone());
				entity.setEmail(dealerContactObj.getEmail());
				entity.setJob_title(dealerContactObj.getJob_title());
				entity.setCity(dealerContactObj.getCity());
				entity.setLast_modified_by(dealerContactObj.getLast_modified_by());
				entity.setDealer_id(dealerContactObj.getDealer_id());
				entity.setCreated_date(dealerContactObj.getCreated_date());
				entity.setLast_modified_date(new Date());
				entity.setContact_type(dealerContactObj.getContact_type());

				dealerService.updateDealerContact(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerContact>(status, entity);
	}

	//View dealerContact
	public ResponseMessage<DealerContact> viewdealercontact(long id){
		ResponseStatus status = null;
		DealerContact entity =null;
		try{
			entity = dealerService.findDealerContactById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerContact>(status, entity);
	}	

	//get all dealerContacts
	public ResponseMessages<DealerContact> getalldealercontacts(){
		ResponseStatus status = null;
		List<DealerContact> entity =null;
		try{
			entity = dealerService.findAllDealerContacts();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DealerContact>(status, entity);
	}	

	//delete dealerContact
	public ResponseMessage<DealerContact> deletedealercontact(long id){
		ResponseStatus status = null;
		DealerContact entity =null;
		try{
			entity = dealerService.findDealerContactById(id);
			if(entity!=null){
				dealerService.deleteDealerContact(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerContact>(status, entity);
	}	

	//Promotion

	//Register new Promotion
	public ResponseMessage<Promotion> createPromotion(Promotion promotionObj){
		ResponseStatus status = null;
		Promotion entity=null;
		try{
			entity=new Promotion();
			entity.setDealer_id(promotionObj.getDealer_id());
			entity.setPromotional_banner_image_url(promotionObj.getPromotional_banner_image_url());
			entity.setPromotion_page_url(promotionObj.getPromotion_page_url());			
			entity.setCreated_date(new Date());
			entity.setValid_from(promotionObj.getValid_from());
			entity.setValid_till(promotionObj.getValid_till());
			dealerService.insertPromotion(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Promotion>(status, entity);		
	}

	//update Promotion
	public ResponseMessage<Promotion> updatePromotion(Promotion promotionObj){
		ResponseStatus status = null;
		Promotion entity=null;
		try{
			entity = dealerService.findPromotionById(promotionObj.getId());
			if(entity!=null){				
				entity.setDealer_id(promotionObj.getDealer_id());
				entity.setPromotional_banner_image_url(promotionObj.getPromotional_banner_image_url());
				entity.setPromotion_page_url(promotionObj.getPromotion_page_url());			
				entity.setCreated_date(promotionObj.getCreated_date());
				entity.setValid_from(promotionObj.getValid_from());
				entity.setValid_till(promotionObj.getValid_till());	
				dealerService.updatePromotion(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Promotion>(status, entity);
	}

	//View Promotion
	public ResponseMessage<Promotion> viewPromotion(long id){
		ResponseStatus status = null;
		Promotion entity =null;
		try{
			entity = dealerService.findPromotionById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Promotion>(status, entity);
	}	

	//get all Promotions
	public ResponseMessages<Promotion> getallPromotions(){
		ResponseStatus status = null;
		List<Promotion> entity =null;
		try{
			entity = dealerService.findAllPromotions();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Promotion>(status, entity);
	}	

	//delete Promotion
	public ResponseMessage<Promotion> deletePromotion(long id){
		ResponseStatus status = null;
		Promotion entity =null;
		try{
			entity = dealerService.findPromotionById(id);
			if(entity!=null){
				dealerService.deletePromotion(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Promotion>(status, entity);
	}

	//Service Rating

	//Register new ServiceRating
	public ResponseMessage<ServiceRating> createServiceRating(ServiceRating serviceRatingObj){
		ResponseStatus status = null;
		ServiceRating entity=null;
		try{
			entity=new ServiceRating();
			entity.setUser_id(serviceRatingObj.getUser_id());
			entity.setDealer_id(serviceRatingObj.getDealer_id());
			entity.setService_rating(serviceRatingObj.getService_rating());
			entity.setRated_on(new Date());
			entity.setDescription(serviceRatingObj.getDescription());
			dealerService.insertServiceRating(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<ServiceRating>(status, entity);		
	}

	//update ServiceRating
	public ResponseMessage<ServiceRating> updateServiceRating(ServiceRating serviceRatingObj){
		ResponseStatus status = null;
		ServiceRating entity=null;
		try{
			entity = dealerService.findServiceRatingById(serviceRatingObj.getId());
			if(entity!=null){				
				entity.setUser_id(serviceRatingObj.getUser_id());
				entity.setDealer_id(serviceRatingObj.getDealer_id());
				entity.setService_rating(serviceRatingObj.getService_rating());
				entity.setRated_on(serviceRatingObj.getRated_on());
				entity.setDescription(serviceRatingObj.getDescription());
				dealerService.updateServiceRating(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<ServiceRating>(status, entity);
	}

	//View ServiceRating
	public ResponseMessage<ServiceRating> viewServiceRating(long id){
		ResponseStatus status = null;
		ServiceRating entity =null;
		try{
			entity = dealerService.findServiceRatingById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<ServiceRating>(status, entity);
	}	

	//get all ServiceRatings
	public ResponseMessages<ServiceRating> getallServiceRatings(){
		ResponseStatus status = null;
		List<ServiceRating> entity =null;
		try{
			entity = dealerService.findAllServiceRatings();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<ServiceRating>(status, entity);
	}	

	//delete ServiceRating
	public ResponseMessage<ServiceRating> deleteServiceRating(long id){
		ResponseStatus status = null;
		ServiceRating entity =null;
		try{
			entity = dealerService.findServiceRatingById(id);
			if(entity!=null){
				dealerService.deleteServiceRating(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<ServiceRating>(status, entity);
	}	

	//Search dealer
	public ResponseListMessage searchDealer(String dealer_province,String dealer_city,String dealer_type){
		ResponseStatus status=null;
		List<SearchDealer> entity=null;
		SearchDealer dealer=new SearchDealer();
		if(!dealer_province.isEmpty()){
			if(!dealer_city.isEmpty()){
				dealer.setCity_code(dealer_city);
				dealer.setProvince_code(dealer_province);
				dealer.setDealer_type(dealer_type);
				entity=dealerService.searchDealer(dealer);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.DEALER_CITY_CANNOT_BE_EMPTY, "Dealer City cannot be empty");
				return new ResponseListMessage(status, entity);
			}

		}else{
			status=new ResponseStatus(ResponseStatusCode.DEALER_PROVINCE_CANNOT_BE_EMPTY, "Dealer Province cannot be empty");
			return new ResponseListMessage(status, entity);
		}	
		return new ResponseListMessage(status, entity);
	}

	//DealerRecruitingCity
	//Register new DealerRecruitingCity
	public ResponseMessage<DealerRecruitingCity> createDealerRecruitingCity(DealerRecruitingCity dealerRecruitingCityObj){
		ResponseStatus status = null;
		DealerRecruitingCity entity=null;
		try{
			entity=new DealerRecruitingCity();
			entity.setCity(dealerRecruitingCityObj.getCity());
			entity.setProvince(dealerRecruitingCityObj.getProvince());
			entity.setDistrict(dealerRecruitingCityObj.getDistrict());
			entity.setRecruitment_type(dealerRecruitingCityObj.getRecruitment_type());
			entity.setRecruitment_number(dealerRecruitingCityObj.getRecruitment_number());
			entity.setRemark(dealerRecruitingCityObj.getRemark());
			entity.setCreated_date(new Date());
			entity.setStatus(dealerRecruitingCityObj.getStatus());
			entity.setUpdated_date(new Date());
			entity.setStatus_updated_date(dealerRecruitingCityObj.getStatus_updated_date());
			dealerService.insertDealerRecruitingCity(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<DealerRecruitingCity>(status, entity);		
	}

	//update DealerRecruitingCity
	public ResponseMessage<DealerRecruitingCity> updateDealerRecruitingCity(DealerRecruitingCity dealerRecruitingCityObj){
		ResponseStatus status = null;
		DealerRecruitingCity entity=null;
		try{
			entity = dealerService.findDealerRecruitingCityById(dealerRecruitingCityObj.getId());
			if(entity!=null){				
				entity.setCity(dealerRecruitingCityObj.getCity());
				entity.setProvince(dealerRecruitingCityObj.getProvince());
				entity.setDistrict(dealerRecruitingCityObj.getDistrict());
				entity.setRecruitment_type(dealerRecruitingCityObj.getRecruitment_type());
				entity.setRecruitment_number(dealerRecruitingCityObj.getRecruitment_number());
				entity.setRemark(dealerRecruitingCityObj.getRemark());
				entity.setCreated_date(dealerRecruitingCityObj.getCreated_date());
				entity.setStatus(dealerRecruitingCityObj.getStatus());
				entity.setUpdated_date(new Date());
				entity.setStatus_updated_date(dealerRecruitingCityObj.getStatus_updated_date());

				dealerService.updateDealerRecruitingCity(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCity>(status, entity);
	}

	//View DealerRecruitingCity
	public ResponseMessage<DealerRecruitingCity> viewDealerRecruitingCity(long id){
		ResponseStatus status = null;
		DealerRecruitingCity entity =null;
		try{
			entity = dealerService.findDealerRecruitingCityById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCity>(status, entity);
	}	

	//get all DealerRecruitingCitys
	public ResponseMessages<DealerRecruitingCityDTO> getallDealerRecruitingCitys(){
		ResponseStatus status = null;

		List<DealerRecruitingCityDTO> entities = null;
		try{
			List<DealerRecruitingCity> listRecruitingCities = dealerService.findAllDealerRecruitingCities();	
			if(listRecruitingCities !=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				entities = new ArrayList<DealerRecruitingCityDTO>();
				for (DealerRecruitingCity dealerRecruitingCity : listRecruitingCities) {
					if(dealerRecruitingCity != null){
						entities.add(populateDealerRecruitingCityDTO(dealerRecruitingCity));
					}
				}
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DealerRecruitingCityDTO>(status, entities);
	}	

	//delete DealerRecruitingCity
	public ResponseMessage<DealerRecruitingCity> deleteDealerRecruitingCity(long id){
		ResponseStatus status = null;
		DealerRecruitingCity entity =null;
		try{
			entity = dealerService.findDealerRecruitingCityById(id);
			if(entity!=null){
				dealerService.deleteDealerRecruitingCity(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCity>(status, entity);
	}

	//get dealer recruiting city by province
	public ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCitiesByProvince(DealerRecruitingCityDTO dealerRecruitingCityDTO,int start,int end){
		ResponseStatus status = null;
		DealerRecruitingCityResponseDTO entity =null;		
		List<DealerRecruitingCityDTO> dealerRecruitCityDTO=new ArrayList<DealerRecruitingCityDTO>();
		try{

			if(null!=dealerRecruitingCityDTO){

				int totalPageCount=dealerService.findTotalCountForDealerRecruitingCitiesByProvince(dealerRecruitingCityDTO.getProvince(), dealerRecruitingCityDTO.getRecruitmentType());

				List<DealerRecruitingCity> response = dealerService.findDealerRecruitingCitiesByProvince(dealerRecruitingCityDTO.getProvince(), dealerRecruitingCityDTO.getRecruitmentType(), start, end);

				if(response !=null){
					for(DealerRecruitingCity obj:response){
						entity= new DealerRecruitingCityResponseDTO();
						if(null != obj) {
							dealerRecruitCityDTO.add(populateDealerRecruitingCityDTO(obj));
						}
					}					
					entity.setRecruitingCities(dealerRecruitCityDTO);
					entity.setTotalRecordCount(totalPageCount);

					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCityResponseDTO>(status, entity);
	}

	//get dealer recruiting city by district
	public ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCitiesByDistrict(DealerRecruitingCityDTO dealerRecruitingCityDTO,int start,int end){
		ResponseStatus status = null;
		DealerRecruitingCityResponseDTO entity =null;		
		List<DealerRecruitingCityDTO> dealerRecruitCityDTO=new ArrayList<DealerRecruitingCityDTO>();
		try{
			if(null!=dealerRecruitingCityDTO){
				int totalPageCount=dealerService.findTotalCountForDealerRecruitingCitiesByDistrict(dealerRecruitingCityDTO.getCity(),
						dealerRecruitingCityDTO.getDistrict(), 
						dealerRecruitingCityDTO.getRecruitmentType());

				List<DealerRecruitingCity> response = dealerService.findDealerRecruitingCitiesByDistrict(dealerRecruitingCityDTO.getCity(), 
						dealerRecruitingCityDTO.getDistrict(),
						dealerRecruitingCityDTO.getRecruitmentType(), start, end);

				if(response !=null){					 
					for(DealerRecruitingCity obj:response){
						entity= new DealerRecruitingCityResponseDTO();
						if(null != obj){
							dealerRecruitCityDTO.add(populateDealerRecruitingCityDTO(obj));
						}
					}
					entity.setRecruitingCities(dealerRecruitCityDTO);
					entity.setTotalRecordCount(totalPageCount);				 	
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else {
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}
			}

		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCityResponseDTO>(status, entity);
	}	

	//get dealer recruiting city by City
	public ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCitiesByCity(DealerRecruitingCityDTO dealerRecruitingCityDTO,int start,int end){
		ResponseStatus status = null;
		DealerRecruitingCityResponseDTO entity =null;		
		List<DealerRecruitingCityDTO> dealerRecruitCityDTO=new ArrayList<DealerRecruitingCityDTO>();

		try{
			if(null!=dealerRecruitingCityDTO){
				int totalPageCount=dealerService.findTotalCountForDealerRecruitingCitiesByCity(dealerRecruitingCityDTO.getCity(), dealerRecruitingCityDTO.getRecruitmentType());
				List<DealerRecruitingCity> response = dealerService.findDealerRecruitingCitiesByCity(dealerRecruitingCityDTO.getCity(), dealerRecruitingCityDTO.getRecruitmentType(), start, end);

				if(response !=null){
					entity= new DealerRecruitingCityResponseDTO();	
					for(DealerRecruitingCity obj:response){

						dealerRecruitCityDTO.add(populateDealerRecruitingCityDTO(obj));
					}
					entity.setRecruitingCities(dealerRecruitCityDTO);
					entity.setTotalRecordCount(totalPageCount);

					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCityResponseDTO>(status, entity);
	}

	private DealerRecruitingCityDTO populateDealerRecruitingCityDTO(DealerRecruitingCity obj) {
		DealerRecruitingCityDTO drcDTO=new DealerRecruitingCityDTO();
		drcDTO.setId(obj.getId());
		drcDTO.setProvince(obj.getProvince());
		drcDTO.setDistrict(obj.getDistrict());
		drcDTO.setCity(obj.getCity());
		drcDTO.setRecruitmentType(obj.getRecruitment_type()+ " " + obj.getRecruitment_req());
		drcDTO.setRemarks(obj.getRemark());
		drcDTO.setRecruitmentNumber(obj.getRecruitment_number());
		return drcDTO;
	}

	//DealerRecruitment
	//Register new DealerRecruitment
	public ResponseMessage<DealerRecruitment> createDealerRecruitment(DealerRecruitment dealerRecruitmentObj,final HttpServletRequest request){
		//long userID=getUserId(request);
		ResponseStatus status = null;
		DealerRecruitment entity=null;
		String cityCode="";

		try{
			DealerRecruitingCity rec_city=dealerService.findDealerRecruitingCityById(dealerRecruitmentObj.getDeclared_city());
			cityCode=rec_city.getCity_code();			

			String pattern = "yyMMdd";		
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			String applicationDate=format.format(new Date());

			String unique_code=null;			
			long declaredCityId=dealerRecruitmentObj.getDeclared_city();
			String formStatus=dealerRecruitmentObj.getForm_status();

			entity=new DealerRecruitment();

			if(null!=formStatus && "SUBMITTED".equalsIgnoreCase(formStatus)){			
				unique_code=cityCode+"-"+ CommonUtils.getNextApplicationSeq(dealerService.getApplicationSequence(declaredCityId))+"-"+applicationDate;				
			}	
			entity.setUnique_code(unique_code);
			entity.setDeclared_city(dealerRecruitmentObj.getDeclared_city());
			entity.setSelected_city(dealerRecruitmentObj.getSelected_city());
			entity.setSelected_province(dealerRecruitmentObj.getSelected_province());
			entity.setSelected_district(dealerRecruitmentObj.getSelected_district());
			entity.setApplicant_name(dealerRecruitmentObj.getApplicant_name());
			entity.setCnt1_name(dealerRecruitmentObj.getCnt1_name());
			entity.setCnt1_position(dealerRecruitmentObj.getCnt1_position());
			entity.setCnt1_phone(dealerRecruitmentObj.getCnt1_phone());
			entity.setCnt1_email(dealerRecruitmentObj.getCnt1_email());
			entity.setCnt2_name(dealerRecruitmentObj.getCnt2_name());
			entity.setCnt2_position(dealerRecruitmentObj.getCnt2_position());
			entity.setCnt2_phone(dealerRecruitmentObj.getCnt2_phone());
			entity.setCnt2_email(dealerRecruitmentObj.getCnt2_email());
			entity.setRegistered_capital(dealerRecruitmentObj.getRegistered_capital());
			entity.setIndustry_experience(dealerRecruitmentObj.getIndustry_experience());
			entity.setBrand_operation(dealerRecruitmentObj.getBrand_operation());
			entity.setMain_site_address(dealerRecruitmentObj.getMain_site_address());
			entity.setMain_site_source(dealerRecruitmentObj.getMain_site_source());
			entity.setAlternate_site_address(dealerRecruitmentObj.getAlternate_site_address());
			entity.setAlternate_site_source(dealerRecruitmentObj.getAlternate_site_source());			
			entity.setForm_status(dealerRecruitmentObj.getForm_status());			
			//entity.setCreated_by(userID);
			entity.setCreated_by(dealerRecruitmentObj.getCreated_by());
			entity.setCreated_date(new Date());
			entity.setSubmitted_date(dealerRecruitmentObj.getSubmitted_date());
			entity.setUpdated_date(new Date());			
			entity.setProvince_code(dealerRecruitmentObj.getProvince_code());
			entity.setCity_code(dealerRecruitmentObj.getCity_code());
			entity.setDistrict_code(dealerRecruitmentObj.getDistrict_code());
			entity.setRecruitment_type(dealerRecruitmentObj.getRecruitment_type());

			dealerService.insertDealerRecruitment(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<DealerRecruitment>(status, entity);		
	}

	//update DealerRecruitment
	public ResponseMessage<DealerRecruitment> updateDealerRecruitment(DealerRecruitment dealerRecruitmentObj){
		ResponseStatus status = null;
		DealerRecruitment entity=null;
		try{
			entity = dealerService.findDealerRecruitmentById(dealerRecruitmentObj.getId());
			if(entity!=null){	
				String unique_code=null;		

				String formStatus=dealerRecruitmentObj.getForm_status();

				if(null != dealerRecruitmentObj.getUnique_code()){
					unique_code = dealerRecruitmentObj.getUnique_code();
				} else {
					long declaredCityId=dealerRecruitmentObj.getDeclared_city();
					DealerRecruitingCity rec_city=dealerService.findDealerRecruitingCityById(dealerRecruitmentObj.getDeclared_city());
					String cityCode=rec_city.getCity_code();			

					String pattern = "yyMMdd";		
					SimpleDateFormat format = new SimpleDateFormat(pattern);
					String applicationDate=format.format(new Date());

					if(null!=formStatus && "SUBMITTED".equalsIgnoreCase(formStatus)){			
						unique_code=cityCode+"-"+ CommonUtils.getNextApplicationSeq(dealerService.getApplicationSequence(declaredCityId))+"-"+applicationDate;	
						entity.setSubmitted_date(dealerRecruitmentObj.getSubmitted_date());
					}	
				}
				entity.setUnique_code(unique_code);
				entity.setDeclared_city(dealerRecruitmentObj.getDeclared_city());
				entity.setSelected_city(dealerRecruitmentObj.getSelected_city());
				entity.setSelected_province(dealerRecruitmentObj.getSelected_province());
				entity.setSelected_district(dealerRecruitmentObj.getSelected_district());
				entity.setApplicant_name(dealerRecruitmentObj.getApplicant_name());
				entity.setCnt1_name(dealerRecruitmentObj.getCnt1_name());
				entity.setCnt1_position(dealerRecruitmentObj.getCnt1_position());
				entity.setCnt1_phone(dealerRecruitmentObj.getCnt1_phone());
				entity.setCnt1_email(dealerRecruitmentObj.getCnt1_email());
				entity.setCnt2_name(dealerRecruitmentObj.getCnt2_name());
				entity.setCnt2_position(dealerRecruitmentObj.getCnt2_position());
				entity.setCnt2_phone(dealerRecruitmentObj.getCnt2_phone());
				entity.setCnt2_email(dealerRecruitmentObj.getCnt2_email());
				entity.setRegistered_capital(dealerRecruitmentObj.getRegistered_capital());
				entity.setIndustry_experience(dealerRecruitmentObj.getIndustry_experience());
				entity.setBrand_operation(dealerRecruitmentObj.getBrand_operation());
				entity.setMain_site_address(dealerRecruitmentObj.getMain_site_address());
				entity.setMain_site_source(dealerRecruitmentObj.getMain_site_source());
				entity.setAlternate_site_address(dealerRecruitmentObj.getAlternate_site_address());
				entity.setAlternate_site_source(dealerRecruitmentObj.getAlternate_site_source());
				entity.setForm_status(dealerRecruitmentObj.getForm_status());					
				entity.setUpdated_date(new Date());			
				entity.setProvince_code(dealerRecruitmentObj.getProvince_code());
				entity.setCity_code(dealerRecruitmentObj.getCity_code());
				entity.setDistrict_code(dealerRecruitmentObj.getDistrict_code());
				entity.setRecruitment_type(dealerRecruitmentObj.getRecruitment_type());

				dealerService.updateDealerRecruitment(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitment>(status, entity);
	}


	/**
	 * Find DealerRecruitment Application Records By User Id
	 * @param dealerRecruitmentDTO
	 * @param start
	 * @param end
	 * @return
	 */
	public ResponseMessage<DealerApplicationRecordResDTO> findDealerRecruitmentByUserId(DealerRecruitmentDTO dealerRecruitmentDTO,int start,int end){

		ResponseStatus status = null;		
		DealerApplicationRecordResDTO  entity= null;
		List<DealerRecruitmentDTO> dealerRecruitList  = new ArrayList<DealerRecruitmentDTO>();

		try{			
			long userId=dealerRecruitmentDTO.getUser_id();
			if(userId>0){
				int totalRecordCount=dealerService.findTotalRecordDealerRecruitmentByUserId(userId);
				List<DealerRecruitment> dealerRecrutimentList = dealerService.findDealerRecruitmentByUserId(userId,start,end);

				if(null != dealerRecrutimentList){

					for(DealerRecruitment obj:dealerRecrutimentList){
						dealerRecruitList.add(populateDealerRecruitmentDTO(obj));
					}
					entity=new DealerApplicationRecordResDTO();
					entity.setTotalRecordCount(totalRecordCount);
					entity.setApplicationRecords(dealerRecruitList);

					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}

			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerApplicationRecordResDTO>(status, entity);
	}	

	/**
	 * Find DealerRecruitment Applications by User Id and status
	 * @param dealerRecruitmentDTO
	 * @param start
	 * @param end
	 * @return
	 */
	public ResponseMessage<DealerApplicationRecordResDTO> findDealerRecruitmentsByUserAndStatus(DealerRecruitmentDTO dealerRecruitmentDTO,int start,int end){
		ResponseStatus status = null;		
		DealerApplicationRecordResDTO  entity= null;
		List<DealerRecruitmentDTO> dealerRecruitList  = new ArrayList<DealerRecruitmentDTO>();

		try{		
			long userId=dealerRecruitmentDTO.getUser_id();
			String formStatus=dealerRecruitmentDTO.getStatus();
			if(userId>0){
				int totalRecordCount=dealerService.findTotalRecordDealerRecruitmentsByStatusAndUserId(userId, formStatus);
				List<DealerRecruitment> dealerRecrutimentList = dealerService.findDealerRecruitmentsByStatusAndUserId(userId, formStatus,start,end);

				if(null != dealerRecrutimentList){

					for(DealerRecruitment obj:dealerRecrutimentList){						

						dealerRecruitList.add(populateDealerRecruitmentDTO(obj));
					}
					entity=new DealerApplicationRecordResDTO();
					entity.setTotalRecordCount(totalRecordCount);
					entity.setApplicationRecords(dealerRecruitList);

					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerApplicationRecordResDTO>(status, entity);
	}	

	private DealerRecruitmentDTO populateDealerRecruitmentDTO (DealerRecruitment obj) {
		DealerRecruitmentDTO dealerRecruitObj = new DealerRecruitmentDTO();	
		dealerRecruitObj.setId(obj.getId());
		dealerRecruitObj.setCity(obj.getSelected_city());
		dealerRecruitObj.setDistrict(obj.getSelected_district());
		dealerRecruitObj.setProvince(obj.getSelected_province());
		dealerRecruitObj.setStatus(obj.getForm_status());
		dealerRecruitObj.setSubmittedDate(obj.getSubmitted_date());
		dealerRecruitObj.setSubmittedDateStr(CommonUtils.getDateAsString(obj.getSubmitted_date()));		
		dealerRecruitObj.setUniqueCode(obj.getUnique_code());
		return dealerRecruitObj;
	}

	//Get DealerRecruitment By Id
	public ResponseMessage<DealerRecruitment> getRecruitmentById(long id){
		ResponseStatus status = null;
		DealerRecruitment entity =null;
		try{
			entity = dealerService.findDealerRecruitmentById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitment>(status, entity);
	}

	//delete DealerRecruitment
	public ResponseMessage<DealerRecruitment> deleteDealerRecruitment(long id){
		ResponseStatus status = null;
		DealerRecruitment entity =null;
		try{
			entity = dealerService.findDealerRecruitmentById(id);
			if(entity!=null){
				dealerService.deleteDealerRecruitment(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitment>(status, entity);
	}

	public ResponseMessages<Dealer> getprvdealers(String province){
		ResponseStatus status = null;
		List<Dealer> entity =null;
		try{
			entity = dealerService.filterDealerByProvince(province);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}  
		}
		catch(Exception ex){
			ex.printStackTrace(); 
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Dealer>(status, entity);
	}

	//Get Dealer Record list
	public ResponseMessages<DealerRecordList> myapplicationInfo(){
		ResponseStatus status = null;
		List<DealerRecordList> entity =null;
		try{
			entity = dealerService.getDealerRecordList();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DealerRecordList>(status, entity);
	}	

	public ResponseMessages<Dealer> filterCityDealers(String city_code){
		ResponseStatus status = null;
		List<Dealer> entity =null;
		try{
			entity = dealerService.filterDealerByCity(city_code);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Dealer>(status, entity);
	}

	public ResponseMessages<Dealer> filterDealersType(String type){
		ResponseStatus status = null;
		List<Dealer> entity =null;
		try{
			entity = dealerService.filterDealerByType(type);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Dealer>(status, entity);
	}
	public ResponseMessages<Dealer> filterDealers(String province,String city,String dealer_type_code){
		ResponseStatus status = null;
		List<Dealer> entity =null;
		try{
			Dealer d= new Dealer();
			d.setProvince(province);
			d.setCity(city);
			d.setDealer_type_code(dealer_type_code);
			entity = dealerService.filterDealer(d);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Dealer>(status, entity);
	}

	public ResponseMessage<Dealer> smsDealer(String DesNo,String Msg,String id){
		ResponseStatus status = null;
		Dealer entity =null;
		try{
			entity = dealerService.findDealerById(Long.parseLong(id));
			JSONObject json = (JSONObject)new JSONParser().parse(Msg);
			StringBuilder sb = new StringBuilder();
			sb.append("Dear Customer, You have selected dealer ");
			sb.append(json.get("name"));
			sb.append(" located in");
			sb.append(json.get(" address"));
			sb.append("，sales hotline：");
			sb.append(json.get("sales_hotline"));
			sb.append("。Thank you for your trust and support!"); 
			String smsdata = sb.toString();
			smsdata+= "【一汽-大众】";
			String code = commonutills.SendDealerSms(DesNo, smsdata);
			if(!code.equalsIgnoreCase("-4"))
			{
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
		}

		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Dealer>(status, entity);
	}
	//get DealerRecruiting drop down values
	public ResponseMessage<DealerRecruitingProvinceCityDistrictDTO> getDealerRecruitingProvinceCityDistrict(){
		ResponseStatus status = null;
		DealerRecruitingProvinceCityDistrictDTO entity = null;
		try{
			List<String> provinces = dealerService.findDealerRecruitingProvinces();
			if(provinces != null){
				List<DealerProvinceDTO> recruiting_cities = new ArrayList<DealerProvinceDTO>();
				for (String province : provinces) {

					if(null != province){
						DealerProvinceDTO provinceDTO = new DealerProvinceDTO();
						provinceDTO.setProvince_name(province);
						provinceDTO.setCities(getDealerCities(province));							
						recruiting_cities.add(provinceDTO);
					}
				}	
				entity = new DealerRecruitingProvinceCityDistrictDTO();
				entity.setRecruiting_cities(recruiting_cities);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingProvinceCityDistrictDTO>(status, entity);
	}

	private List<DealerCityDTO> getDealerCities(String province){
		List<String> cities = dealerService.findDealerRecruitingCitiesByProvinceName(province);
		List<DealerCityDTO> listCity = null;
		if(null != cities){
			listCity = new ArrayList<DealerCityDTO> ();
			for (String city : cities) {
				if(null != city){
					DealerCityDTO dealerCity = new DealerCityDTO();
					dealerCity.setCity_name(city);
					dealerCity.setDistricts(getDealerDistricts(city));
					listCity.add(dealerCity);
				}					
			}
		}
		return listCity;
	}

	private List<DealerDistrictDTO> getDealerDistricts(String city){
		List<String> districts = dealerService.findDealerRecruitingDistrictsByCityName(city);
		List<DealerDistrictDTO> listDistrict = null;
		if(null != districts){
			listDistrict = new ArrayList<DealerDistrictDTO> ();
			for (String district : districts) {
				if(null != district){
					DealerDistrictDTO dealerCity = new DealerDistrictDTO();
					dealerCity.setDistrict_name(district);
					listDistrict.add(dealerCity);
				}					
			}
		}
		return listDistrict;
	}

	//get dealer recruiting cities by recruitment type
	public ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCities(DealerRecruitingCityDTO dealerRecruitingCityDTO,int start,int end){
		ResponseStatus status = null;
		DealerRecruitingCityResponseDTO entity =null;		

		try{
			if(null!=dealerRecruitingCityDTO){
				int totalPageCount=dealerService.findTotalCountForDealerRecruitingCities(dealerRecruitingCityDTO.getRecruitmentType());
				List<DealerRecruitingCity> response = dealerService.findDealerRecruitingCities(dealerRecruitingCityDTO.getRecruitmentType(), start, end);

				if(response !=null){
					List<DealerRecruitingCityDTO> listDealerRecruitCityDTO=new ArrayList<DealerRecruitingCityDTO>();
					entity= new DealerRecruitingCityResponseDTO();
					for(DealerRecruitingCity obj:response){

						DealerRecruitingCityDTO drcDTO=new DealerRecruitingCityDTO();
						drcDTO.setId(obj.getId());
						drcDTO.setProvince(obj.getProvince());
						drcDTO.setDistrict(obj.getDistrict());
						drcDTO.setCity(obj.getCity());
						drcDTO.setRecruitmentType(obj.getRecruitment_type() + " " + obj.getRecruitment_req());
						drcDTO.setRemarks(obj.getRemark());
						drcDTO.setRecruitmentNumber(obj.getRecruitment_number());
						listDealerRecruitCityDTO.add(drcDTO);
					}
					entity.setRecruitingCities(listDealerRecruitCityDTO);
					entity.setTotalRecordCount(totalPageCount);

					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}
			}				

		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerRecruitingCityResponseDTO>(status, entity);
	}	

	public ResponseMessages<DealerCityProvince> getDealerCityProvinceName(String lang_code){
		ResponseStatus status = null;
		List<DealerCityProvince> entity =null;
		try{
			entity=dealerService.getDealerCityProvinceName(lang_code);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DealerCityProvince>(status, entity);
	}
}