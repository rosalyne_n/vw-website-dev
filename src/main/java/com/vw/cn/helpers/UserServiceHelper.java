package com.vw.cn.helpers;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.SmsJson;
import com.vw.cn.customDomain.UserJson;
import com.vw.cn.domain.User;
import com.vw.cn.domain.UserDetail;
import com.vw.cn.domain.UserRole;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.IUserService;
import com.vw.cn.utils.CommonUtils;

/**
 * @author karthikeyan_v
 */
@Service
public class UserServiceHelper {	

	@Autowired 
	private IUserService userService;	

	CommonUtils commonUtils 		= CommonUtils.getInstance();

	ConcurrentHashMap<String, String> verifycode = new ConcurrentHashMap<>();

	//Register New User
	public ResponseMessage<User> createuser(String account,String pass,String accounttype){
		ResponseStatus status = null;
		User entity=null;
		try{
			entity=new User();	
			entity.setAccount(account);			
			if(pass.isEmpty()){
				entity.setPassword("");
			}
			else{
				entity.setPassword(commonUtils.generateEncryptedPwd(pass));
			}			
			entity.setAccount_type(accounttype);
			entity.setIs_enabled(1);
			entity.setCreated_date(new Date());
			entity.setLast_login_date(new Date());
			entity.setLast_modified_date(new Date());
			entity.setDelete_flag(false);
			userService.insertUser(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<User>(status, entity);		
	}

	//update user
	public ResponseMessage<User> updateuser(long id,String account,	String pass,String accounttype){
		ResponseStatus status = null;
		User entity=null;
		try{
			entity = userService.findUserById(id);
			if(entity!=null){
				if(!account.isEmpty()){
					entity.setAccount(account);
				}
				if(!pass.isEmpty()){
					entity.setPassword(commonUtils.generateEncryptedPwd(pass));
				}
				if(pass.isEmpty()){
					entity.setPassword("");
				}
				if(!accounttype.isEmpty()){
					entity.setAccount_type(accounttype);
				}
				entity.setLast_modified_date(new Date());
				userService.updateUser(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<User>(status, entity);
	}

	//View Profile
	public ResponseMessage<User> viewuser(long id){
		ResponseStatus status = null;
		User entity =null;
		try{
			entity = userService.findUserById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<User>(status, entity);
	}	

	//get all users
	public ResponseMessages<User> getallusers(){
		ResponseStatus status = null;
		List<User> entity =null;
		try{
			entity = userService.findAllUsers();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<User>(status, entity);
	}

	//delete Profile
	public ResponseMessage<User> deleteuser(long id){
		ResponseStatus status = null;
		User entity =null;
		try{
			entity = userService.findUserById(id);
			if(entity!=null){
				entity.setLast_modified_date(new Date());
				entity.setDelete_flag(true);
				userService.deleteUser(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<User>(status, entity);
	}	

	// User Details Controller
	//Register New User
	public ResponseMessage<UserDetail> createuserdetail(String firstname,String lastname,String email,
			String phone,String address,String city,long userid){
		ResponseStatus status = null;
		UserDetail entity=null;
		try{
			entity=new UserDetail();
			entity.setFirst_name(firstname);
			entity.setLast_name(lastname);
			entity.setEmail(email);
			entity.setPhone(phone);
			entity.setAddress(address);
			entity.setCity(city);
			entity.setUser_id(userid);
			entity.setCreated_date(new Date());
			entity.setLast_modified_date(new Date());
			entity.setDelete_flag(false);
			userService.insertUserDetail(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<UserDetail>(status, entity);		
	}

	//update user
	public ResponseMessage<UserDetail> updateuserdetail(long id,String firstname,String lastname,String email,
			String phone, String address,String city,Long userid){
		ResponseStatus status = null;
		UserDetail entity=null;
		try{
			entity = userService.findUserDetailById(id);
			if(entity!=null){

				if(!firstname.isEmpty()){
					entity.setFirst_name(firstname);
				}
				if(!lastname.isEmpty()){
					entity.setLast_name(lastname);	
				}
				if(!email.isEmpty()){
					entity.setEmail(email);
				}
				if(!phone.isEmpty()){
					entity.setPhone(phone);
				}
				if(!address.isEmpty()){
					entity.setAddress(address);
				}
				if(!city.isEmpty()){
					entity.setCity(city);
				}
				if(userid!=null){
					entity.setUser_id(userid);
				}
				entity.setLast_modified_date(new Date());
				userService.updateUserDetail(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserDetail>(status, entity);
	}

	//View Profile
	public ResponseMessage<UserDetail> viewuserdetail(long id){
		ResponseStatus status = null;
		UserDetail entity =null;
		try{
			entity = userService.findUserDetailById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserDetail>(status, entity);
	}	

	//get all users
	public ResponseMessages<UserDetail> getalluserdetails(){
		ResponseStatus status = null;
		List<UserDetail> entity =null;
		try{
			entity = userService.findAllUserDetails();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<UserDetail>(status, entity);
	}

	//delete Profile
	public ResponseMessage<UserDetail> deleteuserdetail(long id){
		ResponseStatus status = null;
		UserDetail entity =null;
		try{
			entity = userService.findUserDetailById(id);
			if(entity!=null){
				entity.setDelete_flag(true);
				entity.setLast_modified_date(new Date());
				userService.deleteUserDetail(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserDetail>(status, entity);
	}	

	// User Role Controllers
	//Register new role
	public ResponseMessage<UserRole> createuserrole(long userid,long roleid){
		ResponseStatus status = null;
		UserRole entity=null;
		try{
			entity=new UserRole();
			entity.setUser_id(userid);
			entity.setRole_id(roleid);
			entity.setUpdate_time(new Date());
			entity.setDelete_flag(false);
			userService.insertUserRole(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<UserRole>(status, entity);		
	}

	//update role
	public ResponseMessage<UserRole> updateuserrole(long id,Long userid,Long roleid){
		ResponseStatus status = null;
		UserRole entity=null;
		try{
			entity = userService.findUserRoleById(id);
			if(entity!=null){
				if(userid!=null){
					entity.setUser_id(userid);
				}
				if(roleid!=null){
					entity.setRole_id(roleid);
				}
				entity.setUpdate_time(new Date());
				userService.updateUserRole(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserRole>(status, entity);
	}

	//View role
	public ResponseMessage<UserRole> viewuserrole(long id){
		ResponseStatus status = null;
		UserRole entity =null;
		try{
			entity = userService.findUserRoleById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserRole>(status, entity);
	}	

	//get all roles
	public ResponseMessages<UserRole> getalluserroles(){
		ResponseStatus status = null;
		List<UserRole> entity =null;
		try{
			entity = userService.findAllUserRoles();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<UserRole>(status, entity);
	}

	//delete role
	public ResponseMessage<UserRole> deleteuserrole(long id){
		ResponseStatus status = null;
		UserRole entity =null;
		try{
			entity = userService.findUserRoleById(id);
			if(entity!=null){
				entity.setUpdate_time(new Date());
				entity.setDelete_flag(true);
				userService.deleteUserRole(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<UserRole>(status, entity);
	}	

	public ResponseMessage<User> loginUser(User userObj){
		ResponseStatus status = null;
		User user=null;
		try{
			user=userService.findUserByAccount(userObj.getAccount());
			if(user!=null){
				if(user.getPassword().equals(commonUtils.generateEncryptedPwd(userObj.getPassword()))){
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK,"Success");
				}else{
					status = new ResponseStatus(ResponseStatusCode.STATUS_NOTMATCHED, "incorrect password");
					user = null;
				}

			}else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD,"No record found");
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
			status = new ResponseStatus(ResponseStatusCode.STATUS_INTERNAL_ERROR, "INTERNAL_ERROR");
		}
		return new ResponseMessage<User>(status, user);

	}

	public ResponseMessage<User> registerdealer(UserJson user){
		ResponseStatus status = null;
		User entity=null;
		try{
			entity=new User();	
			entity.setAccount(user.getAccount());			
			String passwordStrength =	commonUtils.passwordValidation(user.getAccount(), user.getPassword());
			boolean fieldemptycheck = checkUserFieldsEmpty(user);
			if(fieldemptycheck){
				if((user.getPassword()).equalsIgnoreCase(user.getConfirm_password())){
					if(passwordStrength.equalsIgnoreCase("valid")){
						entity.setPassword(commonUtils.generateEncryptedPwd(user.getPassword()));
						if(verifycode.size() > 0){
							if((user.getVerification_code()).equalsIgnoreCase(verifycode.get(user.getAccount()))){
								if(user.isAgreed_tc()==true){
									entity.setAccount_type("mo");
									entity.setIs_enabled(1);
									entity.setCreated_date(new Date());
									entity.setLast_login_date(new Date());
									entity.setLast_modified_date(new Date());
									entity.setDelete_flag(false);
									userService.insertUser(entity);
									status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
								}
								else
								{
									status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT,"Data required");
								}

							}
							else
							{
								status = new ResponseStatus(ResponseStatusCode.STATUS_NOTMATCHED, "enter correct verification code");
							}
						}
						else
						{
							status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Issue in sms server");
						}
					}
					else
					{
						status = new ResponseStatus(ResponseStatusCode.STATUS_NOTMATCHED, passwordStrength);
						return new ResponseMessage<User>(status, entity);
					}
				}
				else
				{
					status = new ResponseStatus(ResponseStatusCode.STATUS_NOTMATCHED, "password and confirm password not match");
				}
			}
			else
			{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT,"Data required");
			}

		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<User>(status, entity);		
	}

	// getting verification code
	public ResponseMessage<ResponseStatus> getConfirmationCode(String mobileNumber){
		ResponseStatus status = null;
		try{
			String plusSign = "\u002B";
			String mobileNumber1=plusSign+mobileNumber;
			mobileNumber1=mobileNumber1.replaceAll(" " , "");
			mobileNumber = mobileNumber.replaceAll(" " , "");
			if(mobileNumber1!=null){
				Random rand=new Random();
				int codeval=rand.nextInt(10000);
				String code=String.valueOf(codeval);
			    for (String i : verifycode.keySet()) {
			        if (i == mobileNumber) {
			        	verifycode.remove(i);
			        }
			    }
				verifycode.put(mobileNumber, code);
				System.out.println(code);
				String smscode = code;
				smscode+= "【一汽-大众】";
				System.out.println(smscode);
				commonUtils.SendSms(mobileNumber, smscode);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK,"OTP Sent Successfully");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_INVALID,"Mobile Number Invalid");
				return new ResponseMessage<ResponseStatus>(status);
			}

		}catch (Exception e) {
			e.printStackTrace();
			status = new ResponseStatus(ResponseStatusCode.STATUS_INTERNAL_ERROR, "INTERNAL_ERROR");
		}

		return new ResponseMessage<ResponseStatus>(status);
	}

	private boolean checkUserFieldsEmpty(UserJson userObj) {
		if((userObj.getAccount()!=null && !userObj.getAccount().isEmpty()) 
				&& (userObj.getPassword()!=null && !userObj.getPassword().isEmpty())
				&& (userObj.getConfirm_password()!=null && !userObj.getConfirm_password().isEmpty())
				&& (userObj.getVerification_code()!=null && !userObj.getVerification_code().isEmpty())){
			return true;
		}
		else
		{
			return false;
		}
	}

	public ResponseMessage<ResponseStatus> sendSms(SmsJson smsJson) {
		ResponseStatus status = null;
			try {
				if(smsJson!=null)
				{
				String mobileNumber = smsJson.getPhoneNumber();
				mobileNumber = mobileNumber.replaceAll(" " , "");
				String Msg = smsJson.getMsg();
				Msg+= "【一汽-大众】";
	          	String respCode=commonUtils.SendDealerSms(mobileNumber, Msg);
				if(!respCode.equalsIgnoreCase("-4"))
				{
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
			} 
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return new ResponseMessage<ResponseStatus>(status);
	}
}