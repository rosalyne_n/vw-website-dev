package com.vw.cn.oauth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
/**
 * @author karthikeyan_v
 */
@Configuration
public class OAuth2ServerConfiguration {

	private static final String RESOURCE_ID = "restservice";

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends
	ResourceServerConfigurerAdapter {

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources
			.resourceId(RESOURCE_ID);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {			
			http.authorizeRequests().antMatchers("/getallusers**","/revoke**").authenticated()
			.and().httpBasic();
		}			

		@Configuration
		@EnableAuthorizationServer
		public static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {	

			private TokenStore tokenStore = new InMemoryTokenStore();		

			@Autowired
			@Qualifier("authenticationManagerBean")
			private AuthenticationManager authenticationManager;

			@Autowired
			private CustomUserDetailsService userDetailsService;

			@Override
			public void configure(AuthorizationServerEndpointsConfigurer endPoints){
				endPoints
				.tokenStore(this.tokenStore)
				.authenticationManager(this.authenticationManager)
				.userDetailsService(userDetailsService)
				.tokenEnhancer(tokenEnhancer());
			}

			@Override
			public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

				clients
				.inMemory()  
				.withClient("volkswagen")
				.authorizedGrantTypes("password","refresh_token")
				.authorities("USER")
				.scopes("read","write")
				.resourceIds(RESOURCE_ID)
				.secret("volkswagen")
				.accessTokenValiditySeconds(0);
			}

			@Bean
			@Primary
			public DefaultTokenServices tokenServices() {
				DefaultTokenServices tokenServices = new DefaultTokenServices();
				tokenServices.setSupportRefreshToken(true);
				tokenServices.setTokenStore(this.tokenStore);
				tokenServices.setTokenEnhancer(tokenEnhancer());
				return tokenServices;
			}

			// Some @Bean here like tokenStore
			@Bean
			public TokenEnhancer tokenEnhancer() {
				return new CustomTokenEnhancer();
			}

			public class CustomTokenEnhancer implements TokenEnhancer {
				@Override
				public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
					User user = (User) authentication.getPrincipal();

					final Map<String, Object> additionalInfo = new HashMap<>();

					additionalInfo.put("User", userDetailsService.viewProfile(user.getUsername()));

					((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

					return accessToken;
				}
			}
		}
	}
}