package com.vw.cn.oauth;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.vw.cn.service.IUserService;

/**
 * @author karthikeyan_v
 */
@Service  
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CustomUserDetailsService implements UserDetailsService {  

	@Autowired
	private IUserService userService;

	@Autowired
	public CustomUserDetailsService(IUserService userService) {
		this.userService = userService;
	} 

	@Override  
	public UserDetails loadUserByUsername(String inputUsername) throws UsernameNotFoundException {  
		com.vw.cn.domain.User usr=userService.findUserByAccount(inputUsername);
		if (usr == null) {  
			throw new UsernameNotFoundException("User details not found with this username: " + inputUsername);  
		} 
		String username = usr.getAccount();  
		String password=usr.getPassword();
		String role = "admin";	
		
		List authList = getAuthorities(role); 	
		
		User user = new User(username, password, authList);    
		return user;  
	}  

	private List getAuthorities(String role) {  
		List authList = new ArrayList();  
		authList.add(new SimpleGrantedAuthority("ROLE_USER"));       
		if (role != null && StringUtils.isNotBlank(role)) {  
			if (role.equals("admin")) {  
				authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));  
			} 			 
			if (role.equals("user")) {  
				authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));  
			}  
		}    
		return authList; 
	}
	
	public com.vw.cn.domain.User viewProfile(String account){
		com.vw.cn.domain.User user=null;
		try {
			user=userService.findUserByAccount(account);			
		}catch(Exception e){
			e.printStackTrace();
		}
		return user;
	}	
}  