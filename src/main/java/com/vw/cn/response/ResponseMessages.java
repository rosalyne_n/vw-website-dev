package com.vw.cn.response;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author karthikeyan_v
 */
@XmlRootElement(name="response")
public class ResponseMessages<T> {

	@XmlElement(name="status")
	public ResponseStatus status;

	private Collection<T> entities;

	public ResponseMessages(){
		super();
	}

	public ResponseMessages(ResponseStatus status, Collection<T>  entities){
		super();
		this.status=status;
		this.entities = entities;
	}

	@XmlElement(name="response")
	public Collection<T> getEntities() {
		return entities;
	}
}