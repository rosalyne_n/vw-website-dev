package com.vw.cn.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author karthikeyan_v
 */
@XmlRootElement(name="response")
public class ResponseMessage<T> {

	@XmlElement(name="status")
	public ResponseStatus status;

	@XmlElement(name="response")
	private T _entity;

	public T getEntity() {
		return _entity;
	}
	public void setEntity(T entity) {
		this._entity = entity;
	}

	public ResponseMessage(){
		super();
	}

	public ResponseMessage(T entity){
		super();
		setEntity(entity);
	}

	public ResponseMessage(ResponseStatus status,T entity){
		super();
		this.status=status;
		this._entity=entity;
	}
}