package com.vw.cn.response;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author karthikeyan_v
 */
@XmlRootElement(name="response")
public class ResponseListMessage {

	@XmlElement(name="status")
	public ResponseStatus status;

	private Collection<?> entities;

	public ResponseListMessage(){
		super();
	}

	public ResponseListMessage(ResponseStatus status, Collection<?>  entities){
		super();
		this.status=status;
		this.entities = entities;
	}

	@XmlElement(name="response")
	public Collection<?> getEntities() {
		return entities;
	}
}