package com.vw.cn.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.XMLConstants;

import org.apache.log4j.Logger;

import com.sun.org.apache.xerces.internal.parsers.XMLDocumentParser;
/**
 *  
 * @author Ram
 * Generic functionalities
 */
public class CommonUtils implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** The default date format as shown in the UI (e.g. 2011.09.12) */
	private static final String DEFAULT_DATE_FORMAT = "yyyy.MM.dd";

	private static final Logger log					= Logger.getLogger(CommonUtils.class);

	private static CommonUtils myInstance = null;
	public static CommonUtils getInstance()
	{
		if(myInstance == null)
			myInstance = new CommonUtils();
		return myInstance;
	}

	/**
	 * generate random pwd
	 * pwd contains first 4 digit of input string with 4 random numbers
	 * @param username
	 * @return generated Pwd
	 */
	public String generatePwd(String username)
	{
		String unameSub = username.substring(0, username.length()>4?3:username.length());
		return unameSub+String.valueOf ((int)(Math.random ()*10000));
	}

	/**
	 * encrypting MD5 hash method
	 * @param pwd
	 * @return encrypted text
	 */
	public String generateEncryptedPwd(String pwd){
		try {
			MessageDigest md5;
			md5 = MessageDigest.getInstance ( "MD5" );
			md5.update ( pwd.getBytes () );
			BigInteger hash = new BigInteger ( 1, md5.digest () );
			return hash.toString ( 16 ) ;
		} catch (Throwable e) {
			log.error("Error While generateEncryptedPwd:"+e);
			return null;
		}
	}

	public float RangeCalculation(float emi,float downpayment,int duration)
	{
		float overallemi = emi * duration;
		float totalcost = downpayment + overallemi;
		return totalcost;
	}

	public float calculateEmi(float downpayment,int tenure,float retail_price,float interest)
	{
		float loan_amount = retail_price - downpayment;
		float loan_interest_amount = loan_amount*(1+(interest/100));
		float loan_emi = loan_interest_amount/tenure;
		loan_emi = round(loan_emi, 2);
		return loan_emi;
	}

	public static float round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.floatValue();
	}

	public String passwordValidation(String userName, String password)
	{
		@SuppressWarnings("unused")
		boolean valid = true;
		String result = "valid";

		if (password.indexOf(userName) > -1)
		{
			result="Password Should not be same as user name";
			valid = false;
		}

		if (password.length() > 15 || password.length() < 8)
		{
			result= "Password should be less than 15 and more than 8 characters in length.";
			valid = false;
		}
		String upperCaseChars = "(.*[A-Z].*)";
		if (!password.matches(upperCaseChars ))
		{
			result="Password should contain atleast one upper case alphabet";
			valid = false;
		}
		String lowerCaseChars = "(.*[a-z].*)";
		if (!password.matches(lowerCaseChars ))
		{
			result="Password should contain atleast one lower case alphabet";
			valid = false;
		}
		String numbers = "(.*[0-9].*)";
		if (!password.matches(numbers ))
		{
			result="Password should contain atleast one number.";
			valid = false;
		}

		/*  String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
             if (!password.matches(specialChars ))
             {
                     System.out.println("Password should contain atleast one special character");
                     valid = false;
             }*/
		return result;
	}
	public String SendDealerSms(String number,String message) throws IOException
	{ 
		String httpsURL = "http://h.1069106.com:1210/Services/MsgSend.asmx/SendMsg";
		String userCode = "fawvw" ;
		String userPass = "fawvw9875";
		String chan = "0";
		String query;
		String code = "";
		try {
			query = "userCode="+URLEncoder.encode(userCode,"UTF-8");
			query += "&";
			query += "userPass="+URLEncoder.encode(userPass,"UTF-8") ;
			query += "&";
			query += "DesNo="+URLEncoder.encode(number,"UTF-8") ;
			query += "&";
			query += "Msg="+URLEncoder.encode(message,"UTF-8") ;
			query += "&";
			query += "Channel="+URLEncoder.encode(chan,"UTF-8") ;
			URL myurl = new URL(httpsURL);
			HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-length", String.valueOf(query.length())); 
			con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
			con.setDoOutput(true); 
			con.setDoInput(true); 
			DataOutputStream output = new DataOutputStream(con.getOutputStream());  
			output.writeBytes(query);
			output.close();
			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
			String output1;
			String value="";
			while ((output1 = br.readLine()) != null) {
				value= output1;
			}
			System.out.println("value :: "+ value);	
			String result=value.replace("<string xmlns=\"http://tempuri.org/\">", "").replace("</string>","");
			System.out.println("result :: "+ result);	
			 code = result;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return code; 

	}
	
	 /**
     * Gets the starting row id for the respective page request
     * @param currentPage the current page number
     * @param rowsPerPage the number of rows displayed per page
     * @return the start row number
     */
	 public void SendSms(String number,String message) throws IOException
	 {
	  String httpsURL = "http://h.1069106.com:1210/Services/MsgSend.asmx/SendMsg";
	  String userCode = "fawvw" ;
	  String userPass = "fawvw9875";
	  String chan = "0";
	  String query;
	  try {
	   query = "userCode="+URLEncoder.encode(userCode,"UTF-8");
	   query += "&";
	   query += "userPass="+URLEncoder.encode(userPass,"UTF-8") ;
	   query += "&";
	   query += "DesNo="+URLEncoder.encode(number,"UTF-8") ;
	   query += "&";
	   query += "Msg="+URLEncoder.encode(message,"UTF-8") ;
	   query += "&";
	   query += "Channel="+URLEncoder.encode(chan,"UTF-8") ;
	   URL myurl = new URL(httpsURL);
	   HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
	   con.setRequestMethod("POST");
	   con.setRequestProperty("Content-length", String.valueOf(query.length())); 
	   con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
	   con.setDoOutput(true); 
	   con.setDoInput(true); 
	   DataOutputStream output = new DataOutputStream(con.getOutputStream());  
	   output.writeBytes(query);
	   output.close();
	   DataInputStream input = new DataInputStream( con.getInputStream() ); 
	   for( int c = input.read(); c != -1; c = input.read() ) 
	    System.out.print( (char)c ); 
	   input.close(); 
	   System.out.println("Resp Code:"+con .getResponseCode()); 
	   System.out.println("Resp Message:"+ con .getResponseMessage()); 
	  } catch (UnsupportedEncodingException e) {
	   e.printStackTrace();
	  } 

	 }
	
    public static int getStartRow(int currentPage, int rowsPerPage) {
     return (currentPage - 1) * rowsPerPage;
    }
    
    /**
     * Gets the ending row id for the respective page request
     * @param currentPage the current page number
     * @param rowsPerPage the number of rows displayed per page
     * @return the end row number
     */
    public static int getEndRow(int currentPage, int rowsPerPage) {
     return currentPage * rowsPerPage;
    }
    
    public static String getNextApplicationSeq (int seq){
    	String nextSeqStr= "";
    	int nextSeq = seq +1;
    	if(nextSeq < 10){
    		nextSeqStr = "00" + nextSeq;
    	} else if( nextSeq < 100) {
    		nextSeqStr = "0" + nextSeq;
    	} else {
    		nextSeqStr = "" + nextSeq;
    	}
    	
    	return nextSeqStr;
    }
    
    public static String getDateAsString(Date date) {
		String dateStr = "";
		SimpleDateFormat dateFormater = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		if(date != null)
			dateStr = dateFormater.format(date);
		return dateStr ;
	}
}