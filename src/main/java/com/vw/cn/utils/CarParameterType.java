package com.vw.cn.utils;

public interface CarParameterType {
	String STANDARD 	= "STANDARD";
	String EXTERIOR 	= "EXTERIOR";
	String INTERIOR 	= "INTERIOR";
	String SAFETY 		= "SAFETY";
	String COMFORT 		= "COMFORT";

}
