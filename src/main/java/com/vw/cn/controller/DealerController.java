package com.vw.cn.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vw.cn.customDomain.DealerCityProvince;
import com.vw.cn.customDomain.DealerRecordList;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.DealerRecruitingCity;
import com.vw.cn.domain.DealerRecruitment;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.ServiceRating;
import com.vw.cn.dto.DealerRecruitingCityDTO;
import com.vw.cn.dto.DealerRecruitingCityResponseDTO;
import com.vw.cn.dto.DealerRecruitingProvinceCityDistrictDTO;
import com.vw.cn.dto.DealerRecruitmentDTO;
import com.vw.cn.dto.DealerApplicationRecordResDTO;
import com.vw.cn.helpers.DealerServiceHelper;
import com.vw.cn.response.ResponseListMessage;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.service.IDealerService;
import com.vw.cn.utils.CommonUtils;
/**
 * @author karthikeyan_v
 */
/**
 * @author User
 *
 */
@RestController
public class DealerController{	

	@Autowired
	private DealerServiceHelper dealerServiceHelper;

	@Autowired 
	private IDealerService dealerService;

	CommonUtils commonutills = CommonUtils.getInstance();

	//Register new dealer
	@RequestMapping(value="/adddealer", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Dealer> createdealer(@RequestBody Dealer dealerObj){
		return dealerServiceHelper.createdealer(dealerObj);	
	}

	//update dealer
	@RequestMapping(value="/updatedealer", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Dealer> updatedealer(@RequestBody Dealer dealerObj){
		return dealerServiceHelper.updatedealer(dealerObj);
	}

	//View dealer
	@RequestMapping(value="/viewdealer", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Dealer> viewdealer(@FormParam("id") String id){
		return dealerServiceHelper.viewdealer(Long.parseLong(id));
	}	

	//get all dealers
	@RequestMapping(value="/getalldealers", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Dealer> getalldealers(){
		return dealerServiceHelper.getalldealers();
	}

	// Dealer Registration Contact controllers
	//Register new dealerContact
	@RequestMapping(value="/adddealercontact", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerContact> createdealercontact(@RequestBody DealerContact dealerContactObj){
		return dealerServiceHelper.createdealercontact(dealerContactObj);		
	}

	//update dealerContact
	@RequestMapping(value="/updatedealercontact", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerContact> updatedealercontact(@RequestBody DealerContact dealerContactObj){
		return dealerServiceHelper.updatedealercontact(dealerContactObj);
	}

	//View dealerContact
	@RequestMapping(value="/viewdealercontact", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<DealerContact> viewdealercontact(@FormParam("id") String id){
		return dealerServiceHelper.viewdealercontact(Long.parseLong(id));
	}	

	//get all dealerContacts
	@RequestMapping(value="/getalldealercontacts", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerContact> getalldealercontacts(){
		return dealerServiceHelper.getalldealercontacts();
	}	

	//delete dealerContact
	@RequestMapping(value="/deletedealercontact", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerContact> deletedealercontact(@FormParam("id") String id){
		return dealerServiceHelper.deletedealercontact(Long.parseLong(id));
	}	

	//Promotion
	//Register new Promotion
	@RequestMapping(value="/addPromotion", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Promotion> createPromotion(@RequestBody Promotion promotionObj){
		return dealerServiceHelper.createPromotion(promotionObj);	
	}

	//update Promotion
	@RequestMapping(value="/updatePromotion", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Promotion> updatePromotion(@RequestBody Promotion promotionObj){
		return dealerServiceHelper.updatePromotion(promotionObj);
	}

	//View Promotion
	@RequestMapping(value="/viewPromotion", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Promotion> viewPromotion(@FormParam("id") String id){
		return dealerServiceHelper.viewPromotion(Long.parseLong(id));
	}	

	//get all Promotions
	@RequestMapping(value="/getallPromotions", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Promotion> getallPromotions(){
		return dealerServiceHelper.getallPromotions();
	}	

	//delete Promotion
	@RequestMapping(value="/deletePromotion", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Promotion> deletePromotion(@FormParam("id") String id){
		return dealerServiceHelper.deletePromotion(Long.parseLong(id));
	}

	//Service Rating
	//Register new ServiceRating
	@RequestMapping(value="/addServiceRating", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ServiceRating> createServiceRating(@RequestBody ServiceRating serviceRatingObj){
		return dealerServiceHelper.createServiceRating(serviceRatingObj);	
	}

	//update ServiceRating
	@RequestMapping(value="/updateServiceRating", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ServiceRating> updateServiceRating(@RequestBody ServiceRating serviceRatingObj){
		return dealerServiceHelper.updateServiceRating(serviceRatingObj);
	}

	//View ServiceRating
	@RequestMapping(value="/viewServiceRating", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<ServiceRating> viewServiceRating(@FormParam("id") String id){
		return dealerServiceHelper.viewServiceRating(Long.parseLong(id));
	}	

	//get all ServiceRatings
	@RequestMapping(value="/getallServiceRatings", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<ServiceRating> getallServiceRatings(){
		return dealerServiceHelper.getallServiceRatings();
	}	

	//delete ServiceRating
	@RequestMapping(value="/deleteServiceRating", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ServiceRating> deleteServiceRating(@FormParam("id") String id){
		return dealerServiceHelper.deleteServiceRating(Long.parseLong(id));
	}	

	//Search dealer
	@RequestMapping(value="/searchDealer",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseListMessage searchDealer(@RequestParam(value="dealer_province") String dealer_province,
			@RequestParam(value="dealer_city") String dealer_city,@RequestParam(value="dealer_type") String dealer_type){
		return dealerServiceHelper.searchDealer(dealer_province, dealer_city,dealer_type);
	}

	//DealerRecruitingCity
	//Register new DealerRecruitingCity
	@RequestMapping(value="/addDealerRecruitingCity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCity> createDealerRecruitingCity(@RequestBody DealerRecruitingCity dealerRecruitingCityObj){
		return dealerServiceHelper.createDealerRecruitingCity(dealerRecruitingCityObj);		
	}

	//update DealerRecruitingCity
	@RequestMapping(value="/updateDealerRecruitingCity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCity> updateDealerRecruitingCity(@RequestBody DealerRecruitingCity dealerRecruitingCityObj){
		return dealerServiceHelper.updateDealerRecruitingCity(dealerRecruitingCityObj);
	}

	//View DealerRecruitingCity
	@RequestMapping(value="/viewDealerRecruitingCity", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCity> viewDealerRecruitingCity(@FormParam("id") String id){
		return dealerServiceHelper.viewDealerRecruitingCity(Long.parseLong(id));
	}	

	/*//get all DealerRecruitingCitys
	@RequestMapping(value="/getallDealerRecruitingCitys", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerRecruitingCity> getallDealerRecruitingCitys(){
		return dealerServiceHelper.getallDealerRecruitingCitys();
	}	
	 */
	//delete DealerRecruitingCity
	@RequestMapping(value="/deleteDealerRecruitingCity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCity> deleteDealerRecruitingCity(@FormParam("id") String id){
		return dealerServiceHelper.deleteDealerRecruitingCity(Long.parseLong(id));
	}

	//get DealerRecruitingCitys drop down values
	@RequestMapping(value="/getDealerRecruitingProvincesCitiesDistrict", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingProvinceCityDistrictDTO> getDealerRecruitingProvincesCitiesDistrict(){
		return dealerServiceHelper.getDealerRecruitingProvinceCityDistrict();
	}

	@RequestMapping(value="/getDealerRecruitingCityDropDownData", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerRecruitingCityDTO> getDealerRecruitingCityDropDownData(){
		return dealerServiceHelper.getallDealerRecruitingCitys();
	}

	//get dealer recruiting city by province and recruitment type
	/*@RequestMapping(value="/getDealerRecruitingCityByProvince", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByProvince(
						@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO,
						@RequestParam(value = "page", required = false, defaultValue = "1") 
    					final int page,
					    @RequestParam(value = "rows", required = false, defaultValue = "10") 
					    final int rowsPerPage){
		return dealerServiceHelper.getDealerRecruitingCitiesByProvince(dealerRecruitingCityDTO,CommonUtils.getStartRow(page, rowsPerPage), CommonUtils.getEndRow(page, rowsPerPage));
	}*/
	@RequestMapping(value="/getDealerRecruitingCityByProvince", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByProvince(
			@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO){

		int start=CommonUtils.getStartRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		return dealerServiceHelper.getDealerRecruitingCitiesByProvince(dealerRecruitingCityDTO,start,end);
	}

	
	
	@RequestMapping(value="/getDealerRecruitmentCities", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitment(
						@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO){

		int start=CommonUtils.getStartRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		return dealerServiceHelper.getDealerRecruitingCities(dealerRecruitingCityDTO,start,end);
	}  
	  
	 
	

	//get dealer recruiting city by district and recruitment type
	/*@RequestMapping(value="/getDealerRecruitingCityByDistrict", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByDistrict(
						@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO,
						@RequestParam(value = "page", required = false, defaultValue = "1") 
    					final int page,
					    @RequestParam(value = "rows", required = false, defaultValue = "10") 
					    final int rowsPerPage){
		return dealerServiceHelper.getDealerRecruitingCitiesByDistrict(dealerRecruitingCityDTO,CommonUtils.getStartRow(page, rowsPerPage), CommonUtils.getEndRow(page, rowsPerPage));
	}*/
	@RequestMapping(value="/getDealerRecruitingCityByDistrict", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByDistrict(
			@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO){
		int start=CommonUtils.getStartRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		return dealerServiceHelper.getDealerRecruitingCitiesByDistrict(dealerRecruitingCityDTO,start,end);
	}

	//get dealer recruiting city by city and recruitment type
	/*@RequestMapping(value="/getDealerRecruitingCityByCity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByCity(
						@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO,
						@RequestParam(value = "page", required = false, defaultValue = "1") 
    					final int page,
					    @RequestParam(value = "rows", required = false, defaultValue = "10") 
					    final int rowsPerPage){
		return dealerServiceHelper.getDealerRecruitingCitiesByCity(dealerRecruitingCityDTO,CommonUtils.getStartRow(page, rowsPerPage), CommonUtils.getEndRow(page, rowsPerPage));
	}*/
	@RequestMapping(value="/getDealerRecruitingCityByCity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitingCityResponseDTO> getDealerRecruitingCityByCity(
			@RequestBody DealerRecruitingCityDTO dealerRecruitingCityDTO){
		int start=CommonUtils.getStartRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitingCityDTO.getPage(), dealerRecruitingCityDTO.getRowsPerPage());
		return dealerServiceHelper.getDealerRecruitingCitiesByCity(dealerRecruitingCityDTO,start,end);
	}

	//DealerRecruitment
	//Register new DealerRecruitment
	@RequestMapping(value="/addDealerRecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitment> createDealerRecruitment(@RequestBody DealerRecruitment dealerRecruitmentObj,final HttpServletRequest request){
		return dealerServiceHelper.createDealerRecruitment(dealerRecruitmentObj,request);	
	}

	//update DealerRecruitment
	@RequestMapping(value="/updateDealerRecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitment> updateDealerRecruitment(@RequestBody DealerRecruitment dealerRecruitmentObj){
		return dealerServiceHelper.updateDealerRecruitment(dealerRecruitmentObj);
	}

	//View DealerRecruitment
	@RequestMapping(value="/getDealerRecruitmentByUserId", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerApplicationRecordResDTO> getDealerRecruitmentByUserId(
			@RequestBody DealerRecruitmentDTO dealerRecruitmentDTO){
		int start=CommonUtils.getStartRow(dealerRecruitmentDTO.getPage(), dealerRecruitmentDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitmentDTO.getPage(), dealerRecruitmentDTO.getRowsPerPage());
		return dealerServiceHelper.findDealerRecruitmentByUserId(dealerRecruitmentDTO,start,end);
	}	

	//get all DealerRecruitments
	@RequestMapping(value="/getDealerRecruitmentByStatusUserId", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerApplicationRecordResDTO> getDealerRecruitmentByStatusUserId(@RequestBody DealerRecruitmentDTO dealerRecruitmentDTO){
		int start=CommonUtils.getStartRow(dealerRecruitmentDTO.getPage(), dealerRecruitmentDTO.getRowsPerPage());
		int end=CommonUtils.getEndRow(dealerRecruitmentDTO.getPage(), dealerRecruitmentDTO.getRowsPerPage());
		return dealerServiceHelper.findDealerRecruitmentsByUserAndStatus(dealerRecruitmentDTO,start,end);
	}		

	//Get DealerRecruitment By Id
		@RequestMapping(value="/getRecruitmentById", method=RequestMethod.GET,produces="application/json")
		public @ResponseBody ResponseMessage<DealerRecruitment> getRecruitmentById(@RequestParam("id") String id){			
			return dealerServiceHelper.getRecruitmentById(Long.parseLong(id));
		}

	//delete DealerRecruitment
	@RequestMapping(value="/deleteDealerRecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerRecruitment> deleteDealerRecruitment(@FormParam("id") String id){
		return dealerServiceHelper.deleteDealerRecruitment(Long.parseLong(id));
	}

	@RequestMapping(value="/filterdealerbyprovince", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Dealer> getprvdealers(@RequestParam("province") String province){
		return dealerServiceHelper.getprvdealers(province);
	}

	//Get Dealer Record list
	@RequestMapping(value="/myapplicationInfo", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerRecordList> myapplicationInfo(){
		return dealerServiceHelper.myapplicationInfo();
	}	

	@RequestMapping(value="/filterdealerbycity", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Dealer> filterCityDealers(@RequestParam("city_code") String city_code,final HttpServletResponse response,final HttpServletRequest request){
		return dealerServiceHelper.filterCityDealers(city_code);
	}

	@RequestMapping(value="/filterdealerbytype", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Dealer> filterDealersType(@RequestParam("type") String type,final HttpServletResponse response,final HttpServletRequest request){
		return dealerServiceHelper.filterDealersType(type);
	}
	
	@RequestMapping(value="/filterdealer", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Dealer> filterDealers(@RequestParam("province") String province,
			@RequestParam("city") String city,
			@RequestParam(value = "dealer_type_code", required = false) final String dealer_type_code, 
			final HttpServletResponse response, final HttpServletRequest request){
		return dealerServiceHelper.filterDealers(province, city, dealer_type_code);
	}

	@RequestMapping(value="/smsdealer", method=RequestMethod.POST)
	public @ResponseBody ResponseMessage<Dealer> smsDealer(@FormParam("id") String id,@FormParam("DesNo") String DesNo ,@FormParam("Msg") String Msg  ,final HttpServletResponse response,final HttpServletRequest request){
		return dealerServiceHelper.smsDealer(DesNo, Msg, id);
	}
	
	@RequestMapping(value="/getDealerCityProvinceNames", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerCityProvince> getDealerCityProvinceName(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return dealerServiceHelper.getDealerCityProvinceName(lang);
	}
}