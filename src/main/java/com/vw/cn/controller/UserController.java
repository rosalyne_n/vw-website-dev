package com.vw.cn.controller;

import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vw.cn.customDomain.SmsJson;
import com.vw.cn.customDomain.UserJson;
import com.vw.cn.domain.User;
import com.vw.cn.domain.UserDetail;
import com.vw.cn.domain.UserRole;
import com.vw.cn.helpers.UserServiceHelper;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
/**
 * @author karthikeyan_v
 */
@Controller
public class UserController{	

	@Autowired 
	private UserServiceHelper userServiceHelper;	

	//Register New User
	@RequestMapping(value="/createuser", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<User> createuser(@FormParam("account") String account,	@FormParam("pass") String pass,
			@FormParam("accounttype") String accounttype){
		return userServiceHelper.createuser(account, pass, accounttype);		
	}

	//update user
	@RequestMapping(value="/updateuser", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<User> updateuser(@FormParam("id") String id,@FormParam("account") String account,	
			@FormParam("pass") String pass,	@FormParam("accounttype") String accounttype){
		return userServiceHelper.updateuser(Long.parseLong(id), account, pass, accounttype);
	}

	//View Profile
	@RequestMapping(value="/viewuser", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<User> viewuser(@FormParam("id") String id){
		return userServiceHelper.viewuser(Long.parseLong(id));
	}	

	//get all users
	@RequestMapping(value="/getallusers", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<User> getallusers(){
		return userServiceHelper.getallusers();
	}

	//delete Profile
	@RequestMapping(value="/deleteuser", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<User> deleteuser(@FormParam("id") String id){
		return userServiceHelper.deleteuser(Long.parseLong(id));
	}	

	// User Details Controller
	//Register New User
	@RequestMapping(value="/createuserdetail", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserDetail> createuserdetail(@FormParam("firstname") String firstname,@FormParam("lastname") String lastname,
			@FormParam("email") String email,@FormParam("phone") String phone,@FormParam("address") String address,
			@FormParam("city") String city,@FormParam("userid") String userid){
		return userServiceHelper.createuserdetail(firstname, lastname, email, phone, address, city, Long.parseLong(userid));
	}

	//update user
	@RequestMapping(value="/updateuserdetail", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserDetail> updateuserdetail(@FormParam("id") String id,@FormParam("firstname") String firstname,
			@FormParam("lastname") String lastname, @FormParam("email") String email,@FormParam("phone") String phone,
			@FormParam("address") String address, @FormParam("city") String city,@FormParam("userid") String userid){
		return userServiceHelper.updateuserdetail(Long.parseLong(id), firstname, lastname, email, phone, address, city, Long.parseLong(userid));
	}

	//View Profile
	@RequestMapping(value="/viewuserdetail", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<UserDetail> viewuserdetail(@FormParam("id") String id){
		return userServiceHelper.viewuserdetail(Long.parseLong(id));
	}	

	//get all users
	@RequestMapping(value="/getalluserdetails", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<UserDetail> getalluserdetails(){
		return userServiceHelper.getalluserdetails();
	}

	//delete Profile
	@RequestMapping(value="/deleteuserdetail", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserDetail> deleteuserdetail(@FormParam("id") String id){
		return userServiceHelper.deleteuserdetail(Long.parseLong(id));
	}	

	// User Role Controllers
	//Register new role
	@RequestMapping(value="/createuserrole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserRole> createuserrole(@FormParam("userid") String userid,@FormParam("roleid") String roleid){
		return userServiceHelper.createuserrole(Long.parseLong(userid),Long.parseLong(roleid));		
	}

	//update role
	@RequestMapping(value="/updateuserrole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserRole> updateuserrole(@FormParam("id") String id,@FormParam("userid") String userid,@FormParam("roleid") String roleid){
		return userServiceHelper.updateuserrole(Long.parseLong(id),Long.parseLong(userid),Long.parseLong(roleid));
	}

	//View role
	@RequestMapping(value="/viewuserrole", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<UserRole> viewuserrole(@FormParam("id") String id){
		return userServiceHelper.viewuserrole(Long.parseLong(id));
	}	

	//get all roles
	@RequestMapping(value="/getalluserroles", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<UserRole> getalluserroles(){
		return userServiceHelper.getalluserroles();
	}

	//delete role
	@RequestMapping(value="/deleteuserrole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<UserRole> deleteuserrole(@FormParam("id") String id){
		return userServiceHelper.deleteuserrole(Long.parseLong(id));
	}	

	@RequestMapping(value="/login", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<User> loginUser(@RequestBody User userObj){
		return userServiceHelper.loginUser(userObj);
	}

	@RequestMapping(value="/registerdealer", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<User> registerdealer(@RequestBody UserJson user){
		return userServiceHelper.registerdealer(user);
	}

	// getting verification code
	@RequestMapping(value="/getconfirmationcode", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<ResponseStatus> getConfirmationCode(@FormParam("mobileNumber") String mobileNumber){
		return userServiceHelper.getConfirmationCode(mobileNumber);
	}
	@RequestMapping(value="/sendsms", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ResponseStatus> sendSms(@RequestBody SmsJson SmsJson){
		return userServiceHelper.sendSms(SmsJson);
		
	}
}