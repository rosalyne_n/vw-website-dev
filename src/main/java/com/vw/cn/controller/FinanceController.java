package com.vw.cn.controller;

import javax.ws.rs.FormParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import com.vw.cn.customDomain.FinancialLoan;
import com.vw.cn.domain.FinancialInstitution;
import com.vw.cn.domain.FinancialLoanCalculation;
import com.vw.cn.domain.FinancialProducts;
import com.vw.cn.domain.Loan;
import com.vw.cn.domain.ModelFinancialInterest;
import com.vw.cn.helpers.FinanceServiceHelper;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
/**
 * @author karthikeyan_v
 */
@Controller
public class FinanceController {

	@Autowired 
	private FinanceServiceHelper financeServiceHelper;

	// FinancialInstitution
	//Register new FinancialInstitution
	@RequestMapping(value="/createFinancialInstitution", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialInstitution> createFinancialInstitution(@RequestBody FinancialInstitution financialInstitutionObj){
		return financeServiceHelper.createFinancialInstitution(financialInstitutionObj);	
	}

	//update FinancialInstitution
	@RequestMapping(value="/updateFinancialInstitution", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialInstitution> updateFinancialInstitution(@RequestBody FinancialInstitution financialInstitutionObj){
		return financeServiceHelper.updateFinancialInstitution(financialInstitutionObj);
	}

	//View FinancialInstitution
	@RequestMapping(value="/viewFinancialInstitution", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialInstitution> viewFinancialInstitution(@FormParam("id") String id){
		return financeServiceHelper.viewFinancialInstitution(Long.parseLong(id));
	}	

	//get all FinancialInstitutions
	@RequestMapping(value="/getallFinancialInstitutions", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<FinancialInstitution> getallFinancialInstitutions(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return financeServiceHelper.getallFinancialInstitutions(lang);
	}

	// FinancialProducts
	//Register new FinancialProducts
	@RequestMapping(value="/createFinancialProducts", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialProducts> createFinancialProducts(@RequestBody FinancialProducts financialProductsObj){
		return financeServiceHelper.createFinancialProducts(financialProductsObj);	
	}

	//update FinancialProducts
	@RequestMapping(value="/updateFinancialProducts", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialProducts> updateFinancialProducts(@RequestBody FinancialProducts financialProductsObj){
		return financeServiceHelper.updateFinancialProducts(financialProductsObj);
	}

	//View FinancialProducts
	@RequestMapping(value="/viewFinancialProducts", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<FinancialProducts> viewFinancialProducts(@FormParam("id") String id){
		return financeServiceHelper.viewFinancialProducts(Long.parseLong(id));
	}	

	//get all FinancialProductss
	@RequestMapping(value="/getallFinancialProductss", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<FinancialProducts> getallFinancialProductss(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return financeServiceHelper.getallFinancialProductss(lang);
	}

	// ModelFinancialInterest
	//Register new ModelFinancialInterest
	@RequestMapping(value="/createModelFinancialInterest", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ModelFinancialInterest> createModelFinancialInterest(@RequestBody ModelFinancialInterest modelFinancialInterestObj){
		return financeServiceHelper.createModelFinancialInterest(modelFinancialInterestObj);	
	}

	//update ModelFinancialInterest
	@RequestMapping(value="/updateModelFinancialInterest", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<ModelFinancialInterest> updateModelFinancialInterest(@RequestBody ModelFinancialInterest modelFinancialInterestObj){
		return financeServiceHelper.updateModelFinancialInterest(modelFinancialInterestObj);
	}

	//View ModelFinancialInterest
	@RequestMapping(value="/viewModelFinancialInterest", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<ModelFinancialInterest> viewModelFinancialInterest(@FormParam("modelCode") String modelCode){
		return financeServiceHelper.viewModelFinancialInterest(modelCode);
	}	

	//get all ModelFinancialInterests
	@RequestMapping(value="/getallModelFinancialInterests", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<ModelFinancialInterest> getallModelFinancialInterests(){
		return financeServiceHelper.getallModelFinancialInterests();
	}

	//## Loan
	@RequestMapping(value="/applyloan", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Loan> applyloan(@RequestBody Loan loan){
		return financeServiceHelper.applyloan(loan);	
	}

	@RequestMapping(value="/getallloanrecords", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Loan> getAllLoanRecords(){
		return financeServiceHelper.getAllLoanRecords();	
	}

	@RequestMapping(value="/getloanrecordbyid", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Loan> getLoanRecordById(@FormParam("id") String id ){
		return financeServiceHelper.getLoanRecordById(Long.parseLong(id));	
	}

/*	@RequestMapping(value="/calculateloan", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<LoanCalculator> calculateLoan(@RequestBody Loan loan){
		return financeServiceHelper.calculateLoan(loan);
	}*/
	
	//get all FinancialProductss
	@RequestMapping(value="/getallFinancialProductsByInstitute", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<FinancialProducts> getAllFinancialProductsByInstitute(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang,@RequestParam("financialInstitution") String financialInstitution){
		return financeServiceHelper.getallFinancialProductsByInstitute(lang,financialInstitution);
	}
	
	/*@RequestMapping(value="/calculateFinancialLoan", method=RequestMethod.POST,produces="application/json")
	 public @ResponseBody ResponseMessage<FinancialLoanCalculationDTO> getFinancialLoanCalculation(@RequestBody Loan loan){
	  return financeServiceHelper.getFinancialLoanCalculation(loan);
	 }*/
	 
	 @RequestMapping(value="/calculateFinancialLoan", method=RequestMethod.POST,produces="application/json")
	 public @ResponseBody ResponseMessage<FinancialLoanCalculation> getFinancialLoanCalculations(@RequestBody Loan loan){
	  return financeServiceHelper.getFinancialLoanCalculations(loan);
	 }
	 
	 @RequestMapping(value="/calculateFinancialLoans", method=RequestMethod.POST,produces="application/json")
	 public @ResponseBody ResponseMessages<FinancialLoan> getFinancialLoanCalculation(@RequestBody Loan loan){
	  return financeServiceHelper.getFinancialLoanCalculation(loan);
	 }
}