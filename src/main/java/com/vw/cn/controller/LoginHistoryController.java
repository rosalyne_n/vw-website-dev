package com.vw.cn.controller;

import java.util.Date;
import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vw.cn.domain.LoginHistory;
import com.vw.cn.helpers.LoginHistoryServiceHelper;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;

/**
 * @author karthikeyan_v
 */
@RestController
public class LoginHistoryController{	

	@Autowired 
	private LoginHistoryServiceHelper loginHistoryServiceHelper;	

	//Register new loginhistory
	@RequestMapping(value="/addlogintime", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<LoginHistory> createloginhistory(@FormParam("userid") String userid,@FormParam("token") String token,
			@FormParam("logintime") Date logintime){
		return loginHistoryServiceHelper.createloginhistory(userid, token, logintime);
	}

	//update loginhistory
	@RequestMapping(value="/addlogouttime", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<LoginHistory> updateloginhistory(@FormParam("token") String token,
			@FormParam("logouttime") Date logouttime){
		return loginHistoryServiceHelper.updateloginhistory(token, logouttime);
	}

	//View loginhistory
	@RequestMapping(value="/viewloginhistory", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<LoginHistory> viewloginhistory(@FormParam("id") String id){
		return loginHistoryServiceHelper.viewloginhistory(Long.parseLong(id));
	}	

	//get all loginhistorys
	@RequestMapping(value="/getallloginhistories", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<LoginHistory> getallloginhistories(){
		return loginHistoryServiceHelper.getallloginhistories();
	}

	//delete loginhistory
	@RequestMapping(value="/deleteloginhistory", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<LoginHistory> deleteloginhistory(@FormParam("id") String id){
		return loginHistoryServiceHelper.deleteloginhistory(Long.parseLong(id));
	}	
}