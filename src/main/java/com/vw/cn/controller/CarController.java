package com.vw.cn.controller;

import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vw.cn.domain.Budget;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.PurchaseOrder;
import com.vw.cn.domain.TestDrive;
import com.vw.cn.dto.CarModelComparisionDto;
import com.vw.cn.dto.CarSeriesDto;
import com.vw.cn.helpers.CarServiceHelper;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.service.impl.CarModelParameterService;

/**
 * @author karthikeyan_v
 */
@Controller
public class CarController{	

	@Autowired 
	private CarServiceHelper carServiceHelper;
	
	@Autowired
	private CarModelParameterService carModelParamerterService;

	//Register New CarModel
	@RequestMapping(value="/createcar", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<CarModel> createcar(@RequestBody CarModel carModelObj){
		return carServiceHelper.createcar(carModelObj);	
	}

	//View Profile
	@RequestMapping(value="/viewcar", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<CarModel> viewcar(@FormParam("id") String id){
		return carServiceHelper.viewcar(Long.parseLong(id));
	}	

	//get all cars
	@RequestMapping(value="/getallcars", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<CarModel> getallcars(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return carServiceHelper.getallcars(lang);
	}

	//delete Profile
	@RequestMapping(value="/deletecar", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<CarModel> deletecar(@FormParam("id") String id){
		return carServiceHelper.deletecar(Long.parseLong(id));
	}

	//## Test Drive
	//Register New TestDrive
	@RequestMapping(value="/createtestdrive", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<TestDrive> createtestdrive(@RequestBody TestDrive testDriveReq){ 
		return carServiceHelper.createtestdrive(testDriveReq);	
	}

	//View Profile
	@RequestMapping(value="/viewtestdrive", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<TestDrive> viewtestdrive(@FormParam("id") String id){
		return carServiceHelper.viewtestdrive(Long.parseLong(id));
	}	

	//get all testdrives
	@RequestMapping(value="/getalltestdrives", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<TestDrive> getalltestdrives(){
		return carServiceHelper.getalltestdrives();
	}

	//delete Profile
	@RequestMapping(value="/deletetestdrive", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<TestDrive> deletetestdrive(@FormParam("id") String id){
		return carServiceHelper.deletetestdrive(Long.parseLong(id));
	}

	//## Purchase Order
	//Register New PurchaseOrder
	@RequestMapping(value="/createpurchaseorder", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<PurchaseOrder> createpurchaseorder(@RequestBody PurchaseOrder purchaseOrderObj){
		return carServiceHelper.createpurchaseorder(purchaseOrderObj);		
	}

	//View Profile
	@RequestMapping(value="/viewpurchaseorder", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<PurchaseOrder> viewpurchaseorder(@FormParam("id") String id){
		return carServiceHelper.viewpurchaseorder(Long.parseLong(id));
	}	

	//get all purchaseorders
	@RequestMapping(value="/getallpurchaseorders", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<PurchaseOrder> getallpurchaseorders(){
		return carServiceHelper.getallpurchaseorders();
	}

	//delete Profile
	@RequestMapping(value="/deletepurchaseorder", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<PurchaseOrder> deletepurchaseorder(@FormParam("id") String id){
		return carServiceHelper.deletepurchaseorder(Long.parseLong(id));
	}

	@RequestMapping(value="/findCarWithinBudget", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessages<CarModel> budgetapi(@RequestBody Budget budgetdetail){
		return carServiceHelper.budgetapi(budgetdetail);	
	}

	//CarSeries
	@RequestMapping(value="/createcarseries",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<CarSeries> createcarseries(@RequestBody CarSeries carSeriesObj){
		return carServiceHelper.createcarseries(carSeriesObj);
	}

	//get all cars series
	@RequestMapping(value="/getallcarseries", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<CarSeries> getallcarseries(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return carServiceHelper.getallcarseries(lang);
	}
	
	@RequestMapping(value="/getcarModelsbyseries", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<CarModel> getCarsBySeries(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang,@RequestParam(value="series") String series){
		CarModel carmodelEntity=new CarModel();
		carmodelEntity.setLang(lang);
		carmodelEntity.setSeries_code(series);
		return carServiceHelper.getCarModelsBySeries(carmodelEntity);
	}
	
	@RequestMapping(value="/getCarComparisonModelsDetails", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<CarModelComparisionDto> getCarComparisonModelsDetails(@RequestParam(value="modelCode1") String modelCode1,
			@RequestParam(value="modelCode2") String modelCode2,
			@RequestParam(value="modelCode3", required=false) String modelCode3,
			@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		
		 return carModelParamerterService.compareCarModels(modelCode1,modelCode2,modelCode3,lang);
	}
	
	@RequestMapping(value="/getCarConfigurationParameters", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<CarModelComparisionDto> getCarCConfigurationParameters(@RequestParam(value="modelCode") String modelCode,
			@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		
		 return carModelParamerterService.getCarModelConfiguration(modelCode, lang);
	}
	
	//get all cars series
	@RequestMapping(value="/getAllCarSeriesAndModels", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<CarSeriesDto> getAllCarSeriesAndModels(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return carModelParamerterService.getCarSeriesAndModelsByLang(lang);
	}	
	
	
}