package com.vw.cn.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vw.cn.domain.User;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
/**
 * @author karthikeyan_v
 */
@Controller
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class TokenController{

	@Autowired
	private DefaultTokenServices tokenService;

	@RequestMapping(value = "/revoke", method = RequestMethod.GET)
	public @ResponseBody ResponseMessage<User> logout(HttpServletRequest request)throws ServletException {
		ResponseStatus status = null;
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null) {
			String tokenValue = authHeader.replace("bearer", "").trim();
			//System.out.println("token value::->"+tokenValue);
			tokenService.revokeToken(tokenValue);
			tokenService.setAccessTokenValiditySeconds(1);
			tokenService.setRefreshTokenValiditySeconds(1);
			//System.out.println("success....");
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK,"User LoggedOut Successfully");			
		}else{
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK,"Cannot able to logged out");
		}	
		return new ResponseMessage<User>(status, null);
	}
}