package com.vw.cn.controller;

import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vw.cn.customDomain.DistrictCityProvince;
import com.vw.cn.customDomain.ProvinceAndCity;
import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.DealerType;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;
import com.vw.cn.domain.SourceSite;
import com.vw.cn.helpers.MasterDataServiceHelper;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
/**
 * @author karthikeyan_v
 */
@RestController
public class MasterDataController{	

	@Autowired 
	private MasterDataServiceHelper masterDataServiceHelper;	

	// Role
	//Register new role
	@RequestMapping(value="/createrole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Role> createrole(@FormParam("name") String name,@FormParam("description") String description){
		return masterDataServiceHelper.createrole(name, description);	
	}

	//update role
	@RequestMapping(value="/updaterole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Role> updaterole(@FormParam("id") String id,@FormParam("name") String name,@FormParam("description") String description){
		return masterDataServiceHelper.updaterole(Long.parseLong(id), name, description);
	}

	//View role
	@RequestMapping(value="/viewrole", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Role> viewrole(@FormParam("id") String id){
		return masterDataServiceHelper.viewrole(Long.parseLong(id));
	}	

	//get all roles
	@RequestMapping(value="/getallroles", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Role> getallroles(){
		return masterDataServiceHelper.getallroles();
	}

	//delete role
	@RequestMapping(value="/deleterole", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Role> deleterole(@FormParam("id") String id){
		return masterDataServiceHelper.deleterole(Long.parseLong(id));
	}	

	//Language
	//Register new language
	@RequestMapping(value="/addlanguage", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Language> createlanguage(@FormParam("name") String name,@FormParam("code") String code){
		return masterDataServiceHelper.createlanguage(name, code);	
	}

	//update language
	@RequestMapping(value="/updatelanguage", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Language> updatelanguage(@FormParam("id") String id,@FormParam("name") String name,@FormParam("code") String code){
		return masterDataServiceHelper.updatelanguage(Long.parseLong(id), name, code);
	}

	//View language
	@RequestMapping(value="/viewlanguage", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Language> viewlanguage(@FormParam("id") String id){
		return masterDataServiceHelper.viewlanguage(Long.parseLong(id));
	}	

	//get all languages
	@RequestMapping(value="/getalllanguages", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Language> getalllanguages(){
		return masterDataServiceHelper.getalllanguages();
	}

	//delete language
	@RequestMapping(value="/deletelanguage", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Language> deletelanguage(@FormParam("id") String id){
		return masterDataServiceHelper.deletelanguage(Long.parseLong(id));
	}	

	//Province
	@RequestMapping(value="/addprovince", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Province> createprovince(@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("langcode") String langcode){
		return masterDataServiceHelper.createprovince(code, name, langcode);	
	}

	@RequestMapping(value="/updateprovince", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Province> updateprovince(@FormParam("id") String id,@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("langcode") String langcode){
		return masterDataServiceHelper.updateprovince(Long.parseLong(id), code, name, langcode);
	}

	@RequestMapping(value="/viewprovince", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Province> viewprovince(@FormParam("id") String id){
		return masterDataServiceHelper.viewprovince(Long.parseLong(id));
	}	

	@RequestMapping(value="/getallprovinces", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Province> getallprovinces(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallprovinces(lang);
	}

	@RequestMapping(value="/deleteprovince", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Province> deleteprovince(@FormParam("id") String id){
		return masterDataServiceHelper.deleteprovince(Long.parseLong(id));
	}	

	//City
	//Register new city
	@RequestMapping(value="/addcity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<City> addcity(@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("provincecode") String provincecode, @FormParam("langcode") String langcode){
		return masterDataServiceHelper.addcity(code, name, provincecode, langcode);	
	}

	//update city
	@RequestMapping(value="/updatecity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<City> updatecity(@FormParam("id") String id,@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("provincecode") String provincecode, @FormParam("langcode") String langcode){
		return masterDataServiceHelper.updatecity(Long.parseLong(id), code, name, provincecode, langcode);
	}

	//View city
	@RequestMapping(value="/viewcity", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<City> viewcity(@FormParam("id") String id){
		return masterDataServiceHelper.viewcity(Long.parseLong(id));
	}	
	
	//Get city be name
	@RequestMapping(value="/getCityByName", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<City> getCityByName(@RequestParam("name") String name){
		return masterDataServiceHelper.getCityByName(name);
	}

	//get all citys
	@RequestMapping(value="/getallcities", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<City> getallcities(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallcities(lang);
	}

	//delete city
	@RequestMapping(value="/deletecity", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<City> deletecity(@FormParam("id") String id){
		return masterDataServiceHelper.deletecity(Long.parseLong(id));
	}	

	//District
	//Register new district
	@RequestMapping(value="/adddistrict", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<District> createdistrict(@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("citycode") String citycode, @FormParam("langcode") String langcode){
		return masterDataServiceHelper.createdistrict(code, name, citycode, langcode);	
	}

	//update district
	@RequestMapping(value="/updatedistrict", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<District> updatedistrict(@FormParam("id") String id,@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("citycode") String citycode, @FormParam("langcode") String langcode){
		return masterDataServiceHelper.updatedistrict(Long.parseLong(id), code, name, citycode, langcode);
	}

	//View district
	@RequestMapping(value="/viewdistrict", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<District> viewdistrict(@FormParam("id") String id){
		return masterDataServiceHelper.viewdistrict(Long.parseLong(id));
	}	

	//get all districts
	@RequestMapping(value="/getalldistricts", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<District> getalldistricts(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getalldistricts(lang);
	}

	//delete district
	@RequestMapping(value="/deletedistrict", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<District> deletedistrict(@FormParam("id") String id){
		return masterDataServiceHelper.deletedistrict(Long.parseLong(id));
	}	

	//RecruitmentNetworkType 
	@RequestMapping(value="/createrecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<RecruitmentNetworkType> createrecruitment(@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("langcode") String langcode){
		return masterDataServiceHelper.createrecruitment(code, name, langcode);
	}

	@RequestMapping(value="/updaterecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<RecruitmentNetworkType> updaterecruitment(@FormParam("id") String id,@FormParam("code") String code,@FormParam("name") String name,
			@FormParam("langcode") String langcode){
		return masterDataServiceHelper.updaterecruitment(Long.parseLong(id), code, name, langcode);
	}

	@RequestMapping(value="/viewrecruitment", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<RecruitmentNetworkType> viewrecruitment(@FormParam("id") String id){
		return masterDataServiceHelper.viewrecruitment(Long.parseLong(id));
	}	

	@RequestMapping(value="/getallrecruitments", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<RecruitmentNetworkType> getallrecruitments(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallrecruitments(lang);
	}

	@RequestMapping(value="/deleterecruitment", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<RecruitmentNetworkType> deleterecruitment(@FormParam("id") String id){
		return masterDataServiceHelper.deleterecruitment(Long.parseLong(id));
	}

	// Currency
	//Register new Currency
	@RequestMapping(value="/createCurrency", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Currency> createCurrency(@RequestBody Currency currencyObj){
		return masterDataServiceHelper.createCurrency(currencyObj);	
	}

	//update Currency
	@RequestMapping(value="/updateCurrency", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Currency> updateCurrency(@RequestBody Currency currencyObj){
		return masterDataServiceHelper.updateCurrency(currencyObj);
	}

	//View Currency
	@RequestMapping(value="/viewCurrency", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<Currency> viewCurrency(@FormParam("id") String id){
		return masterDataServiceHelper.viewCurrency(Long.parseLong(id));
	}	

	//get all Currencys
	@RequestMapping(value="/getallCurrencys", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<Currency> getallCurrencys(){
		return masterDataServiceHelper.getallCurrencys();
	}

	//delete Currency
	@RequestMapping(value="/deleteCurrency", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<Currency> deleteCurrency(@FormParam("id") String id){
		return masterDataServiceHelper.deleteCurrency(Long.parseLong(id));
	}	

	@RequestMapping(value="/getallprovinceandcities", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<ProvinceAndCity> getallprovinceandcities(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallprovinceandcities(lang);
	}

	//get all DistrictCityProvince
	@RequestMapping(value="/getAllDistrictCityProvince", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DistrictCityProvince> getAllDistrictCityProvince(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getAllDistrictCityProvince(lang);
	}

	// SourceSite
	//Register new SourceSite
	@RequestMapping(value="/createSourceSite", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<SourceSite> createSourceSite(@RequestBody SourceSite sourceSiteObj){
		return masterDataServiceHelper.createSourceSite(sourceSiteObj);	
	}

	//update SourceSite
	@RequestMapping(value="/updateSourceSite", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<SourceSite> updateSourceSite(@RequestBody SourceSite sourceSiteObj){
		return masterDataServiceHelper.updateSourceSite(sourceSiteObj);
	}

	//View SourceSite
	@RequestMapping(value="/viewSourceSite", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<SourceSite> viewSourceSite(@FormParam("id") String id){
		return masterDataServiceHelper.viewSourceSite(Long.parseLong(id));
	}	

	//get all SourceSites
	@RequestMapping(value="/getallSourceSites", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<SourceSite> getallSourceSites(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallSourceSites(lang);
	}

	//delete SourceSite
	@RequestMapping(value="/deleteSourceSite", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<SourceSite> deleteSourceSite(@FormParam("id") String id){
		return masterDataServiceHelper.deleteSourceSite(Long.parseLong(id));
	}	

	//DealerType
	//Register new DealerType
	@RequestMapping(value="/addContactType", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerType> createDealerType(@RequestBody DealerType ContactTypeObj){
		return masterDataServiceHelper.createDealerType(ContactTypeObj);		
	}

	//update DealerType
	@RequestMapping(value="/updateContactType", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerType> updateDealerType(@RequestBody DealerType ContactTypeObj){
		return masterDataServiceHelper.updateDealerType(ContactTypeObj);
	}

	//View DealerType
	@RequestMapping(value="/viewContactType", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessage<DealerType> viewDealerType(@FormParam("id") String id){
		return masterDataServiceHelper.viewDealerType(Long.parseLong(id));
	}	

	//get all DealerTypes
	@RequestMapping(value="/getallContactTypes", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody ResponseMessages<DealerType> getallDealerTypes(@RequestParam(value="lang", required=false, defaultValue = "CN") String lang){
		return masterDataServiceHelper.getallDealerTypes(lang);
	}	

	//delete DealerType
	@RequestMapping(value="/deleteContactType", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody ResponseMessage<DealerType> deleteDealerType(@FormParam("id") String id){
		return masterDataServiceHelper.deleteDealerType(Long.parseLong(id));
	}	
}