;(function(){
	$('.recruit-reg .clause span.check').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('checked')){
			$(this).removeClass('checked')

		}else{
			$(this).addClass('checked')
			
		}
	});

	$('.recruit-reg .form a.submit').on('click', function(event) {
		event.preventDefault();
		var tel = $('input#tel').val(),
			code = $('input#code').val(),
			password1 = $('input#password1').val(),
			password2 = $('input#password2').val();

			countDown.isPhoneNum();

			if(code==''){
				$('input#code').parent('.line').addClass('warning');
			}else{
				$('input#code').parent('.line').removeClass('warning');
			}

			if(password1=='' || password1.length<6){
				$('input#password1').parent('.line').addClass('warning');
			}else{
				$('input#password1').parent('.line').removeClass('warning');
			}

			if(password1 != password2){
				$('input#password2').parent('.line').addClass('warning');
			}else{
				$('input#password2').parent('.line').removeClass('warning');
			}
			console.log($('div.warning').length);

			if($('div.warning').length==0){
				$('#reg-ok').popup({content:'<h3>注册成功</h3>\
	                <p>想成为一汽大众授权经销商？立即登录进行在线申请</p>\
	                <a href="#" class="btn-login">登录</a>'});	
			}
			
	});

	$('.form .btn-clause').on('click', function(event) {
		event.preventDefault();
		$('#clause-content').popup({
			content: '<h3>注册协议</h3>\
                <p>一汽-大众汽车有限公司保证所有个人数据都将按照隐私保护规定进行处理。在您许可的情况下，您在联系表格中所提供的数据将被保留和进行相应处理。<br />\
本人在此同意此表格的信息可由一汽-大众汽车有限公司及一汽-大众授权经销商共享，并可由一汽-大众汽车有限公司及一汽-大众授权经销商用于其正常的商业活动， 包括但不限于根据本人所提供的信息，通过电话，手机，电子邮件，信件及其它合理方式与本人联系。</p>'
		})
	});

	$('.form a.get-code').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('no-click')) return;
		countDown.sendCode($(this));
	});

	formEvent();

	//手机验证
	var countDown = (function(){
		//发送验证码时添加cookie
		function addCookie(name,value,expiresHours){ 
		    var cookieString=name+"="+escape(value); 
		    //判断是否设置过期时间,0代表关闭浏览器时失效
		    if(expiresHours>0){ 
		        var date=new Date(); 
		        date.setTime(date.getTime()+expiresHours*1000); 
		        cookieString=cookieString+";expires=" + date.toUTCString(); 
		    } 
		        document.cookie=cookieString; 
		} 
		//修改cookie的值
		function editCookie(name,value,expiresHours){ 
		    var cookieString=name+"="+escape(value); 
		    if(expiresHours>0){ 
		      var date=new Date(); 
		      date.setTime(date.getTime()+expiresHours*1000); //单位是毫秒
		      cookieString=cookieString+";expires=" + date.toGMTString(); 
		    } 
		      document.cookie=cookieString; 
		} 
		//根据名字获取cookie的值
		function getCookieValue(name){ 
		      var strCookie=document.cookie; 
		      var arrCookie=strCookie.split("; "); 
		      for(var i=0;i<arrCookie.length;i++){ 
		        var arr=arrCookie[i].split("="); 
		        if(arr[0]==name){
		          return unescape(arr[1]);
		          break;
		        }else{
		             return ""; 
		             break;
		         } 
		      } 
		}

		//发送验证码
		function sendCode(obj){
		    var phone = $("input#tel").val();
		    var result = isPhoneNum();
		    if(result){
		        doPostBack('getcode.php',phone);
		        addCookie("secondsremained",60,60);//添加cookie记录,有效时间60s
		        settime(obj);//开始倒计时
		    }
		}
		//将手机利用ajax提交到后台的发短信接口
		function doPostBack(url,queryParam) {
			//initInfo['phone'] = queryParam;
			var d = {'phone':queryParam}
		    $.ajax({
		        async : false,
		        cache : false,
		        type : 'get',
		        url : url,// 请求的action路径
		        data:d,
		        dataType:'jsonp', 
		        jsonp:'callback',  
		        success : function(data){
		        	
				   	console.log(data)
			        
				    	
		        },
		        error : function() {// 请求失败处理函数
		        }
		    });
		}
		//开始倒计时
		var countdown;
		function settime(obj) { 
		    countdown=getCookieValue("secondsremained");
		    if (countdown == 0) { 
		        obj.removeClass('no-click');
		        obj.text("重新发送"); 
		        return;
		    } else { 
		        obj.addClass('no-click');
		        obj.text("重新获取("+countdown+")");  
		        countdown--;
		        editCookie("secondsremained",countdown,countdown+1);
		    } 
		    setTimeout(function() { settime(obj) },1000) //每1000毫秒执行一次
		} 
		//校验手机号是否合法
		function isPhoneNum(){
		    var phonenum = $("input#tel").val();
		    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
		    if(!myreg.test(phonenum)){
		    	$('input#tel').parents('.line').addClass('warning');
		        //alert('请输入有效的手机号码！'); 
		        return false; 
		    }else{
		    	$('input#tel').parents('.line').removeClass('warning');
		        return true;
		    }
		}

		return {
			sendCode: sendCode,
			getCookieValue:getCookieValue,
			settime:settime,
			isPhoneNum:isPhoneNum
		}
	})();	
})();

