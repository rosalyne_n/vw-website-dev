(function(){
	$('.select-box select').chosen({
		disable_search:true,
		width:'100%'
	});

	$('.radio-box').on('click', 'span.radio', function(event) {
		event.preventDefault();
		if(!$(this).hasClass('selected')){

			$(this).addClass('selected').siblings('span.radio').removeClass('selected')
		}
	});

	$('.check-box').on('click', 'span.check', function(event) {
		event.preventDefault();
		if(!$(this).hasClass('checked')){

			$(this).addClass('checked');
		}else{
			$(this).removeClass('checked');
		}
	});
})();