/**
 * Created by zhan on 2016/5/26.
 */


//导航中搜索框初始值切换
$('.searchformtext').each(function() {
	var default_value = this.value;
	$(this).focus(function(){
		if(this.value == default_value) {
			this.value = '';
		}
	});
	$(this).blur(function(){
		if(this.value == '') {
			this.value = default_value;
		}
	});
});


function resz(){
    $(".top_link").css({"width":$(window).width()-96 +"px"});
    if($(window).width() > 768){
        //$("#main_nav_more").css({"left": "96px"});
        $(".fullscreenbox").css({"height":$(window).height()});
    }else{
        //$("#main_nav_more").css({"left": "100%"});
        $(".fullscreenbox").removeAttr("style");
    }
	if($(window).width() < 640){
		$(".fullscreenbox").css({"height":"240px"});
	}
}
resz();
$(window).resize(function(){
    resz();
	
	//footer
	if($(window).width() <= 991){
		$('.hidden_mobile').hide();
		$('.pc_link').hide();
		$('.mobile_link').css('display','inline-block');
		$('.slide_list').addClass('slide_list_unactive');
		$('.footer span.glyphicon').show();
		$('.footer span.glyphicon').removeClass('glyphicon-menu-up').removeClass('glyphicon-menu-dowm').addClass('glyphicon-menu-dowm');
		$('.list_title').click(function(){
			var slide_list = $(this).next();
			var icon = $(this).children('.glyphicon');
			
			if(slide_list.hasClass('slide_list_unactive')){
				slide_list.slideUp('fast');
				slide_list.removeClass('slide_list_unactive');
				
				icon.removeClass('glyphicon-menu-down');
				icon.addClass('glyphicon-menu-up');
				
				return false;
			}else{
				slide_list.slideDown('fast');
				slide_list.addClass('slide_list_unactive');
				
				icon.removeClass('glyphicon-menu-up');
				icon.addClass('glyphicon-menu-down');
			}
		});


	}
	if($(window).width() > 991){
		$('.hidden_mobile').show();
		$('.pc_link').show();
		$('.mobile_link').hide();
		$('.slide_list').removeClass('slide_list_unactive');
		$('.footer span.glyphicon').hide();

		var Wheight = window.innerHeight;
		$('.fullscreenbox').css({'height':Wheight});
	}
})


/* pc 端主导航点击更多 */
var MenuMore = $("#main_nav_more");
$("#main_nav li").last().click(function(){
        MenuMore.css({"left":"96px"});
        if(MenuMore.attr("data-count") == 1){
            MenuMore.stop().animate({"width":"250px"});
            MenuMore.attr({"data-count":0})
            $(".mask").fadeIn();
        }else{
            MenuMore.stop().animate({"width":"0"});
            MenuMore.attr({"data-count":1})
            $(".mask").fadeOut();
        }
})


$(".mask").click(function(){
    if(MenuMore.attr("data-count") == 0) {
        $("#main_nav li").last().click();
    }
})

if($(window).width() < 640){
    $(".burger_menu").click(function(){
        if(MenuMore.attr("data-count") == 1) {
            MenuMore.stop().animate({"left": "0"});
            MenuMore.attr({"data-count":0});
            $(this).children("div").eq(1).addClass("hideline");
            $(this).children("div").eq(0).addClass("rotate");
            $(this).children("div").eq(2).addClass("re_rotate");
        }else{
            MenuMore.stop().animate({"left": "100%"})
            MenuMore.attr({"data-count":1})
            $(this).children("div").eq(1).removeClass("hideline");
            $(this).children("div").eq(0).removeClass("rotate");
            $(this).children("div").eq(2).removeClass("re_rotate");
        }
    })

}



//footer 下拉
if($(window).width() < 991){
	$('.hidden_mobile').hide();
	$('.pc_link').hide();
	$('.mobile_link').css('display','block');
	$('.slide_list').addClass('slide_list_unactive');
	$('.footer span.glyphicon').show();
	$('.footer span.glyphicon').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-dowm');
	$('.list_title').click(function(){
		var slide_list = $(this).next();
		var icon = $(this).children('.glyphicon');
		
		if(slide_list.hasClass('slide_list_unactive')){
			slide_list.slideUp('fast');
			slide_list.removeClass('slide_list_unactive');
			
			icon.removeClass('glyphicon-menu-down');
			icon.addClass('glyphicon-menu-up');
		}else{
			slide_list.slideDown('fast');
			slide_list.addClass('slide_list_unactive');
			
			icon.removeClass('glyphicon-menu-up');
			icon.addClass('glyphicon-menu-down');
		}
	});
}

if($(window).width() > 991){
	//weixin
	$('.weixin').mouseover(function(){
		$('.erweima').show();	
	}).mouseout(function(){
		$('.erweima').hide();	
	});
}else{
	var flag = true;
	$('.weixin').click(function(){
		$('.erweima').empty();	
		if(flag){
			$('.weixin>img').attr('src','images/erweima.jpg');
			flag = false;
		}else{
			$('.weixin>img').attr('src','images/weixin.png');
			flag = true;
		}
	});
}









