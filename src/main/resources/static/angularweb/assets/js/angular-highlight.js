angular.module('angular-highlight', []).directive('highlight', function() {
	var matchList=[];
	var isMatch=false;
	var component = function(scope, element, attrs) {		
		if (!attrs.highlightClass) {
			attrs.highlightClass = 'angular-highlight';
		}
		
		var replacer = function(match, item) {
			isMatch=false;
            if(matchList != null){
				for(var i=0;i<matchList.length;i++){
					if(matchList[i]==match){
						isMatch=true;
					}
				}
			}   
            if(!isMatch){			
		    matchList.push(match);
			}
            localStorage.setItem('match_list',matchList);			
					
			return '<span class="'+attrs.highlightClass+'" data-toggle="modal" data-target="#myKeywordModal" ">'+match+'</span>';			
		}
		var tokenize = function(keywords) {
			keywords = keywords.replace(new RegExp(',$','g'), '').split(',');
			var i;
			var l = keywords.length;
			for (i=0;i<l;i++) {
				keywords[i] = '\\b'+keywords[i].replace(new RegExp('^ | $','g'), '')+'\\b';
			}
			return keywords;
		}
		
		scope.$watch('keywords', function() {
			//console.log("scope.keywords",scope.keywords);
			if (!scope.keywords || scope.keywords == '') {
				element.html(scope.highlight);
				return false;
			}
			
			
			var tokenized	= tokenize(scope.keywords);
			var regex 		= new RegExp(tokenized.join('|'), 'gmi');
			
			console.log("regex",regex);
			//console.log("replacer",replacer);
			// Find the words
			//console.log(scope.highlight);
			var html = scope.highlight.replace(regex, replacer);
			
			element.html(html);
		});
		matchList=[];
	}
	return {
		link: 			component,
		replace:		false,
		scope:			{
			highlight:	'=',
			keywords:	'='
		}
	};
});
