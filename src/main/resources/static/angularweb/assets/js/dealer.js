/*var dealerData = [];
dealerData[0]={//北京
	province: '北京',
	city: '北京',
	dealer: {
		name:'北京华信宏业',
		phone:'010-88888888',
		address:'北京市怀柔区北房镇龙云路2号',
		longitude:'116.710632',
		latitude:'40.332511',
		type:'1'// 1,经销商;2,二手经销商;3,救援
	}
}
dealerData[1]={//北京
	province: '北京',
	city: '北京',
	dealer: {
		name:'北京华昌',
		phone:'010-88888888',
		address:'北京市海淀区杏石口路15号',
		longitude:'116.266846',
		latitude:'39.954508',
		type:'1'
	}
}

dealerData[2]={//河北
	province: '河北',
	city: '石家庄',
	dealer: {
		name:'石家庄冀中',
		phone:'0316-88888888',
		address:'石家庄市西二环南路188号',
		longitude:'114.443075',
		latitude:'38.026477',
		type:'1'
	}
}

dealerData[3]={//河北
	province: '河北',
	city: '石家庄',
	dealer: {
		name:'河北世纪',
		phone:'0311-88888888',
		address:'石家庄市南二环东路3号',
		longitude:'114.520286',
		latitude:'38.007775',
		type:'1'
	}
}

dealerData[4]={//河北
	province: '河北',
	city: '廊坊',
	dealer: {
		name:'廊坊冀东',
		phone:'0316-88888888',
		address:'河北省廊坊市开发区金源道100号',
		longitude:'116.753895',
		latitude:'39.579564',
		type:'3'
	}
}

dealerData[5]={//河北
	province: '河北',
	city: '廊坊',
	dealer: {
		name:'廊坊庞大一众',
		phone:'0316-88888888',
		address:'廊坊市安次区安次工业园区',
		longitude:'116.795422',
		latitude:'39.345157',
		type:'2'
	}
}*/
// 百度地图API功能
/*var map = new BMap.Map("dealer-map");    // 创建Map实例
map.centerAndZoom(new BMap.Point(116.404, 39.915), 11);  // 初始化地图,设置中心点坐标和地图级别
map.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的
map.enableScrollWheelZoom(true);
*/
$('#province').html(writeOption(0, 0, 'province'));
$('#province').bind('change', function() {
	if (dealerData.length == 0) {
		alert('0');
		return;
	}
	var val = $(this).find('option:checked').val();
	var data = writeOption(val, "province", "city");
	$('#city').html(data);
	$("#city").trigger('chosen:updated');
});
/*$('#city').bind('change', function(event) {
	
	//$('#dealer-type option[value=""]').prop('selected',true).trigger('chosen:updated');
	$('#dealer-type').trigger('change')
})*/

/*$('#dealer-type').bind('change', function() {
	if (dealerData.length == 0) {
		alert('0');
		return;
	}
	if($('#city').val()==''){
		alert('请选择城市');
		$(this).val('');
		return;
	}
	var val = $(this).val();//经销商类型
	var city = $('#city').val();//取城市名
	map.centerAndZoom(city,11); //跳转城市地图
	map.clearOverlays();  //删除现有点
	var index = 0;
	var html='';
	var $searchBox = $('#search-result-box ul');
		$searchBox.html('');
	var marker = [];
	for (var i = 0; i < dealerData.length; i++) {
		
		if (dealerData[i]['city'] == city) {
			var sContent =
				"<p style='font-size:18px;'>"+(index+1)+"."+dealerData[i]['dealer']['name']+"</p>" +
				"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerData[i]['dealer']['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'><div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>销售热线:<span style='color:#1597d9;'>"+dealerData[i]['dealer']['phone']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;'>预约试驾</a></div><div style='float:left;padding-left:20px;'><img src='images/dealer-popup-img.jpg' /><div style='margin-top:12px;overflow: hidden;text-align:center;line-height:44px;color:#1597d9;font-size:18px;'><input type='tel' id='tel' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'><a href='javascript:;' style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo(this);'>发送</a></div></div></div>";
			if (val == 1) {
				
				var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
					offset: new BMap.Size(10, 25),
				    imageOffset: new BMap.Size(0, 0 - index * 25)
				});

				marker[index] = new BMap.Marker(new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']),{icon: myIcon}); // 创建点
				map.addOverlay(marker[index]);             // 将标注添加到地图中

				addClickHandler(sContent,marker[index]);
				
				if(index%2 ==0){
					html += '<li><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:'+dealerData[i]['dealer']['phone']+'</p></li>';
				}else{
					html += '<li class="li-bg"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:'+dealerData[i]['dealer']['phone']+'</p></li>';
				};
				index++;
			} else{
				if(dealerData[i]['dealer']['type']==val){
					var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
						offset: new BMap.Size(10, 25),
					    imageOffset: new BMap.Size(0, 0 - index * 25)
					});

					marker[index] = new BMap.Marker(new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']),{icon: myIcon}); // 创建点
					map.addOverlay(marker[index]);             // 将标注添加到地图中

					addClickHandler(sContent,marker[index]);
					
					if(index%2 ==0){
						html = '<li><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:'+dealerData[i]['dealer']['phone']+'</p></li>';
					}else{
						html = '<li class="li-bg"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:'+dealerData[i]['dealer']['phone']+'</p></li>';
					}
					index++;
				}
			}
			
		}

	};
	$('h3.search-title').html('共找到'+index+'条结果');
	$searchBox.html(html);
	$searchBox.find('li').each(function(index, el) {
		$(this).on('click', function(event) {
			event.preventDefault();
			BMapLib.EventWrapper.trigger(marker[index], 'click', {'type': 'onclick', target: marker[index]});
		});
	});
	var opts = {
		width : 550,     // 信息窗口宽度
		height: 280,     // 信息窗口高度
		//title : "信息窗口" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	};
	function addClickHandler(content,marker){
		// marker.addEventListener("click",function(e){
		// 	openInfo(content,e)}
		// );
		BMapLib.EventWrapper.addListener(marker, 'click', function(e){
		    openInfo(content,e)
		});
	}
	function openInfo(content,e){
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	}
});*/


//数据赋值
/*function writeOption(val, a, b) {
	var html = '<option value=""></option>',
		arr = [];
	for (var i = 0; i < dealerData.length; i++) {
		if (dealerData[i][a] == val || !val) {
			arr.push(dealerData[i][b]);
		}
	};
	$.unique(arr);
	for (var i = 0; i < arr.length; i++) {
		html += '<option value="' + arr[i] + '">' + arr[i] + '</option>';
	};
	return html;
};

$('select').chosen({
	disable_search:true,
});

//发送信息

function sendInfo(t){
	if(isPhoneNum($('input#tel').val())){
		var $o = $(t).parent('div');
		var html = $o.html();
		$(t).parent('div').text('信息已发至您的手机');
		setTimeout(function(){
			$o.html(html);
		},3000)	
	}else{
		alert('手机号码格式不正确');
	}
	
}*/