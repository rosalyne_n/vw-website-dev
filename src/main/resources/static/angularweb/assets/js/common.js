(function(){
	/**
	 * Created by zhan on 2016/5/26.
	 */


	//导航中搜索框初始值切换
	$('.searchformtext').each(function() {
		var default_value = this.value;
		$(this).focus(function(){
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function(){
			if(this.value == '') {
				this.value = default_value;
			}
		});
	});

	function resz(){
	    $(".top_link").css({"width":$(window).width()-96 +"px"});
	    if($(window).width() > 768){
	        //$("#main_nav_more").css({"left": "96px"});
	        $(".fullscreenbox").css({"height":$(window).height()});
	    }else{
	        //$("#main_nav_more").css({"left": "100%"});
	        $(".fullscreenbox").removeAttr("style");
	    }
		if($(window).width() < 640){
			$(".fullscreenbox").css({"height":"240px"});
		}
	}
	resz();
	$(window).resize(function(){
	    resz();
		
		//footer
		if($(window).width() <= 991){
			$('.hidden_mobile').hide();
			$('.pc_link').hide();
			$('.mobile_link').css('display','inline-block');
			$('.slide_list').addClass('slide_list_unactive');
			$('.footer span.glyphicon').show();
			$('.footer span.glyphicon').removeClass('glyphicon-menu-up').removeClass('glyphicon-menu-dowm').addClass('glyphicon-menu-dowm');
			$('.list_title').click(function(){
				var slide_list = $(this).next();
				var icon = $(this).children('.glyphicon');
				
				if(slide_list.hasClass('slide_list_unactive')){
					slide_list.slideUp('fast');
					slide_list.removeClass('slide_list_unactive');
					
					icon.removeClass('glyphicon-menu-down');
					icon.addClass('glyphicon-menu-up');
					
					return false;
				}else{
					slide_list.slideDown('fast');
					slide_list.addClass('slide_list_unactive');
					
					icon.removeClass('glyphicon-menu-up');
					icon.addClass('glyphicon-menu-down');
				}
			});


		}
		if($(window).width() > 991){
			$('.hidden_mobile').show();
			$('.pc_link').show();
			$('.mobile_link').hide();
			$('.slide_list').removeClass('slide_list_unactive');
			$('.footer span.glyphicon').hide();

			var Wheight = window.innerHeight;
			$('.fullscreenbox').css({'height':Wheight});
		}
	})
	
	/* pc 端主导航点击更多 */
	/* pc 端主导航点击更多 */
	var MenuMore = $("#main_nav_more");
	$("#main_nav li").last().click(function(){
        MenuMore.css({"left":"96px"});
        if(MenuMore.attr("data-count") == 1){
            MenuMore.stop().animate({"width":"250px"});
            MenuMore.attr({"data-count":0})
            $(".mask").fadeIn();
        }else{
            MenuMore.stop().animate({"width":"0"});
            MenuMore.attr({"data-count":1})
            $(".mask").fadeOut();
        }
	})


	$(".mask").click(function(event){
		event.preventDefault();
	    if(MenuMore.attr("data-count") == 0) {
	        $("#main_nav li").last().click();
	    }
	})

	if($(window).width() < 640){
	    $(".burger_menu").click(function(){
	        if(MenuMore.attr("data-count") == 1) {
	            MenuMore.stop().animate({"left": "0"});
	            MenuMore.attr({"data-count":0});
	            $(this).children("div").eq(1).addClass("hideline");
	            $(this).children("div").eq(0).addClass("rotate");
	            $(this).children("div").eq(2).addClass("re_rotate");
	        }else{
	            MenuMore.stop().animate({"left": "100%"})
	            MenuMore.attr({"data-count":1})
	            $(this).children("div").eq(1).removeClass("hideline");
	            $(this).children("div").eq(0).removeClass("rotate");
	            $(this).children("div").eq(2).removeClass("re_rotate");
	        }
	    })

	}

	//footer 下拉
	if($(window).width() < 991){
		$('.hidden_mobile').hide();
		$('.pc_link').hide();
		$('.mobile_link').css('display','block');
		$('.slide_list').addClass('slide_list_unactive');
		$('.footer span.glyphicon').show();
		$('.footer span.glyphicon').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-dowm');
		$('.list_title').click(function(){
			var slide_list = $(this).next();
			var icon = $(this).children('.glyphicon');
			
			if(slide_list.hasClass('slide_list_unactive')){
				slide_list.slideUp('fast');
				slide_list.removeClass('slide_list_unactive');
				
				icon.removeClass('glyphicon-menu-down');
				icon.addClass('glyphicon-menu-up');
			}else{
				slide_list.slideDown('fast');
				slide_list.addClass('slide_list_unactive');
				
				icon.removeClass('glyphicon-menu-up');
				icon.addClass('glyphicon-menu-down');
			}
		});
	}

	if($(window).width() > 991){
		//weixin
		$('.weixin').mouseover(function(){
			$('.erweima').show();	
		}).mouseout(function(){
			$('.erweima').hide();	
		});
	}else{
		var flag = true;
		$('.weixin').click(function(){
			$('.erweima').empty();	
			if(flag){
				$('.weixin>img').attr('src','images/erweima.jpg');
				flag = false;
			}else{
				$('.weixin>img').attr('src','images/weixin.png');
				flag = true;
			}
		});
	}

})();

function formEvent(){
	$('input').each(function(index, el) {
		$(this).blur(function(event) {

			if($(this).parent('.input-box').hasClass('warning') && $(this).val()!='' && $(this).attr('type') != 'tel' && !$(this).hasClass('tel') && !$(this).hasClass('mail')){

				$(this).parent('.input-box').removeClass('warning');
			}else if($(this).attr('type') == 'tel') {
				if(isPhoneNum($(this).val())){
					$(this).parent('.input-box').removeClass('warning');
				}

			}else if($(this).hasClass('tel')){
				if(!checkTel($(this).val()) && $(this).val()!=''){
					$(this).parent('.input-box').find('p.warning').text('*电话格式不正确')
				}else if($(this).val()==''){
					$(this).parent('.input-box').find('p.warning').text('*电话输入为空')
				}else{
					$(this).parent('.input-box').removeClass('warning');
				}

			}else if($(this).hasClass('mail')){
				if(!chkEmail($(this).val()) && $(this).val()!=''){
					$(this).parent('.input-box').find('p.warning').text('*邮箱格式不正确')
				}else if($(this).val()==''){
					$(this).parent('.input-box').find('p.warning').text('*邮箱输入为空')
				}else{
					$(this).parent('.input-box').removeClass('warning');
				}

			}
		});
	});

	$('select').each(function(index, el) {
		$(this).on('change', function(event) {
			event.preventDefault();
			if($(this).val() != ''){
				$(this).parent('.select-box').removeClass('warning');
			}	
		});
		
	});
}

//校验手机号是否合法
function isPhoneNum(phonenum){
    
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
    if(!myreg.test(phonenum)){ 
       
        return false; 
    }else{
        return true;
    }
};
//电话格式验证
function checkTel(tel){
   var mobile = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/ , phone = /^0\d{2,3}-?\d{7,8}$/;
   return mobile.test(tel) || phone.test(tel);
}

//邮箱格式验证
function chkEmail(strEmail) {
	if (!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(strEmail)) {
		return false;
	} else {
		return true;
	}
}

//只允许输入数字限制
function keyPress(ob) {
	if (!ob.value.match(/^[\+\-]?\d*?\.?\d*?$/)) ob.value = ob.t_value;
	else ob.t_value = ob.value;
	if (ob.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)) ob.o_value = ob.value;
}

function keyUp(ob) {
	if (!ob.value.match(/^[\+\-]?\d*?\.?\d*?$/)) ob.value = ob.t_value;
	else ob.t_value = ob.value;
	if (ob.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)) ob.o_value = ob.value;
}

function onBlur(ob) {
	if (!ob.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?|\.\d*?)?$/)) ob.value = ob.o_value;
	else {
		if (ob.value.match(/^\.\d+$/)) ob.value = 0 + ob.value;
		if (ob.value.match(/^\.$/)) ob.value = 0;
		ob.o_value = ob.value
	};
}

//提示浮层公用
(function ($) {
    $.fn.extend({
        "popup": function (options) {
            var opts = $.extend({}, defaluts, options);  
            $(this).show().find('.content-box').html(opts.content);
			var $content = $(this).find('.popup-content');
            var w = $content.width(),
            	h = $content.height();
            	if($(window).width()<=640){
            		w=0;
            	}
            	$content.css({
	            	'margin-top': -h/2,
	            	'margin-left': -w/2
            	}).find('a.popup-close').on('click', function(event) {
	            	event.preventDefault();
	            	$(this).parents('.popup').hide();
	            });

        }
    });
    //默认参数
    var defaluts = {
        content: ''
    };
})(window.jQuery);

//css3动画
$.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            if(callback) callback($(this));
            return;
        });
    }
});