(function(angular) {  
    "use strict";  
  
    var defaults = {  
        navCtrl: true,  
        scaleCtrl: true,  
        overviewCtrl: true,  
        enableScrollWheelZoom: true,  
        zoom: 10  
    };  
  
    var checkNull = function(obj) {  
        return obj === null || obj === undefined;  
    };  
  
    var checkMandatory = function(prop, desc) {  
        if (!prop) {  
            throw new Error(desc);  
        }  
    };  
  
    /** 
     * Construction function 
     * 
     * @constructor 
     */  
    var baiduMapDir = function() {  
  
        // Return configured, directive instance  
  
        return {  
            restrict: 'E',  
            scope: {  
                'options': '='  
            },  
            link: function($scope, element, attrs) {  
  
                var ops = {};  
                ops.navCtrl = checkNull($scope.options.navCtrl) ? defaults.navCtrl : $scope.options.navCtrl;  
                ops.scaleCtrl = checkNull($scope.options.scaleCtrl) ? defaults.scaleCtrl : $scope.options.scaleCtrl;  
                ops.overviewCtrl = checkNull($scope.options.overviewCtrl) ? defaults.overviewCtrl : $scope.options.overviewCtrl;  
                ops.enableScrollWheelZoom = checkNull($scope.options.enableScrollWheelZoom) ? defaults.enableScrollWheelZoom : $scope.options.enableScrollWheelZoom;  
                ops.zoom = checkNull($scope.options.zoom) ? defaults.zoom : $scope.options.zoom;  
  
                checkMandatory($scope.options.width, 'options.width must be set');  
                checkMandatory($scope.options.height, 'options.height must be set');  
                checkMandatory($scope.options.center, 'options.center must be set');  
                checkMandatory($scope.options.center.longitude, 'options.center.longitude must be set');  
                checkMandatory($scope.options.center.latitude, 'options.center.latitude must be set');  
                checkMandatory($scope.options.city, 'options.city must be set');  
  
                ops.width = $scope.options.width;  
                ops.height = $scope.options.height;  
                ops.center = {  
                    longitude: $scope.options.center.longitude,  
                    latitude: $scope.options.center.latitude  
                };  
                ops.city = $scope.options.city;  
                ops.markers = $scope.options.markers;  
  
                element.find('div').css('width', ops.width + 'px');  
                element.find('div').css('height', ops.height + 'px');  
  
                // create map instance  
                var map = new BMap.Map(element.find('div')[0]);  
  
                // init map, set central location and zoom level  
                map.centerAndZoom(new BMap.Point(ops.center.longitude, ops.center.latitude), ops.zoom);  
                if (ops.navCtrl) {  
                    // add navigation control  
                    map.addControl(new BMap.NavigationControl());  
                }  
                if (ops.scaleCtrl) {  
                    // add scale control  
                    map.addControl(new BMap.ScaleControl());  
                }  
                if (ops.overviewCtrl) {  
                    //add overview map control  
                    map.addControl(new BMap.OverviewMapControl());  
                }  
                if (ops.enableScrollWheelZoom) {  
                    //enable scroll wheel zoom  
                    map.enableScrollWheelZoom();  
                }  
                // set the city name  
                map.setCurrentCity(ops.city);  
  
  
                if (!ops.markers) {  
                    return;  
                }  
                //create markers  
  
                var openInfoWindow = function(infoWin) {  
                    return function() {  
                        this.openInfoWindow(infoWin);  
                    };  
                };  
                for (var i in ops.markers) {  
                    var marker = ops.markers[i];  
                    var pt = new BMap.Point(marker.longitude, marker.latitude);  
                    var marker2;  
                    if (marker.icon) {  
                        var icon = new BMap.Icon(marker.icon, new BMap.Size(marker.width, marker.height));  
                        marker2 = new BMap.Marker(pt, {  
                            icon: icon  
                        });  
                    }  
                    else {  
                        marker2 = new BMap.Marker(pt);  
                    }  
  
                    // add marker to the map  
                    map.addOverlay(marker2); // 将标注添加到地图中  
  
                    if (!marker.title && !marker.content) {  
                        return;  
                    }  
                    var infoWindow2 = new BMap.InfoWindow("<p>" + (marker.title ? marker.title : '') + "</p><p>" + (marker.content ? marker.content : '') + "</p>", {  
                        enableMessage: checkNull(marker.enableMessage) ? false : marker.enableMessage  
                    });  
                    marker2.addEventListener("click", openInfoWindow(infoWindow2));  
                }  
  
  
            },  
            template: '<div></div>'  
        };  
    };  
  
    var baiduMap = angular.module('baiduMap', []);  
    baiduMap.directive('baiduMap', [baiduMapDir]);  
  
})(angular); 