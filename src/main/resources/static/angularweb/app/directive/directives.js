//Directive to restrict the user to entering characters or special chars or negative values
myApp.directive('numbersOnly',function(){
  return{
    require: 'ngModel',
    link: function(scope,element,attrs,modelCtrl){
      modelCtrl.$parsers.push(function(inputValue){
        if(inputValue == undefined) return '';
        var transformedInput = inputValue.replace(/[^0-9]/g, '');
        if(transformedInput!=inputValue){
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
    }
  };
});

myApp.directive('readMore', function() {
    return {
      restrict: 'A',
      transclude: true,
      replace: true,
      template: '<p></p>',
      scope: {
        moreText: '@',
        lessText: '@',
        words: '@',
        ellipsis: '@',
        char: '@',
        limit: '@',
        content: '@'
      },
      link: function(scope, elem, attr, ctrl, transclude) {
        var moreText = angular.isUndefined(scope.moreText) ? ' <a class="read-more">Read More...</a>' : ' <a class="read-more">' + scope.moreText + '</a>',
          lessText = angular.isUndefined(scope.lessText) ? ' <a class="read-less">Less</a>' : ' <a class="read-less">' + scope.lessText + '</a>',
          ellipsis = angular.isUndefined(scope.ellipsis) ? '' : scope.ellipsis,
          limit = angular.isUndefined(scope.limit) ? 150 : scope.limit;

        attr.$observe('content', function(str) {
          readmore(str);
        });

        transclude(scope.$parent, function(clone, scope) {
          readmore(clone.text().trim());
        });

        function readmore(text) {

          var text = text,
            orig = text,
            regex = /\s+/gi,
            charCount = text.length,
            wordCount = text.trim().replace(regex, ' ').split(' ').length,
            countBy = 'char',
            count = charCount,
            foundWords = [],
            markup = text,
            more = '';

          if (!angular.isUndefined(attr.words)) {
            countBy = 'words';
            count = wordCount;
          }

          if (countBy === 'words') { // Count words

            foundWords = text.split(/\s+/);

            if (foundWords.length > limit) {
              text = foundWords.slice(0, limit).join(' ') + ellipsis;
              more = foundWords.slice(limit, count).join(' ');
              markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
            }

          } else { // Count characters

            if (count > limit) {
              text = orig.slice(0, limit) + ellipsis;
              more = orig.slice(limit, count);
              markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
            }

          }

          elem.append(markup);
          elem.find('.read-more').on('click', function() {
            elem.find('.read-more').hide();
            elem.find('.more-text').addClass('show').slideDown();
          });
          elem.find('.read-less').on('click', function() {
            elem.find('.read-more').show();
            elem.find('.more-text').hide().removeClass('show');
          });

        }
      }
    };
  });

   

myApp.directive("slider", function() {
    return {
        restrict: 'A',
        scope: {
            config: "=config",
            low: "=low",
            high: "=high"
        },
        link: function(scope, elem, attrs) {
            $(elem).slider({
                range: true,
              min: scope.config.min,
              max: scope.config.max,
                step: scope.config.step,
                values: [scope.low, scope.high],
                slide: function(event, ui) { 
                    scope.$apply(function() {
                        scope.low = ui.values[0];
                        scope.high = ui.values[1];
                    });
              }
          });
      }
    };
});

myApp.directive('validPasswordConfirm', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.resetPasswordForm.newpassword.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            });
        }
    };
});
myApp.directive('showErrors', function($timeout) {
    return {
      restrict: 'A',
      require: '^form',
      link: function (scope, el, attrs, formCtrl) {
        // find the text box element, which has the 'name' attribute
        var inputEl   = el[0].querySelector("[name]");
        // convert the native text box element to an angular element
        var inputNgEl = angular.element(inputEl);
        // get the name on the text box
        var inputName = inputNgEl.attr('name');
        
        // only apply the has-error class after the user leaves the text box
        inputNgEl.bind('blur', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        scope.$on('show-errors-check-validity', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        scope.$on('show-errors-reset', function() {
          $timeout(function() {
            el.removeClass('has-error');
          }, 0, false);
        });
      }
    }
  });

myApp.directive('validNumber', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          if(!ngModelCtrl) {
            return; 
          }

          ngModelCtrl.$parsers.push(function(val) {
            if (angular.isUndefined(val)) {
                var val = '';
            }
            
            var clean = val.replace(/[^-0-9\.]/g, '');
            var negativeCheck = clean.split('-');
            var decimalCheck = clean.split('.');
            if(!angular.isUndefined(negativeCheck[1])) {
                negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                clean =negativeCheck[0] + '-' + negativeCheck[1];
                if(negativeCheck[0].length > 0) {
                  clean =negativeCheck[0];
                }
                
            }
              
            if(!angular.isUndefined(decimalCheck[1])) {
                decimalCheck[1] = decimalCheck[1].slice(0,2);
                clean =decimalCheck[0] + '.' + decimalCheck[1];
            }

            if (val !== clean) {
              ngModelCtrl.$setViewValue(clean);
              ngModelCtrl.$render();
            }
            return clean;
          });

          element.bind('keypress', function(event) {
            if(event.keyCode === 32) {
              event.preventDefault();
            }
          });
        }
      };
    });
myApp.directive('scroll', function($timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      scope.$watchCollection(attr.scroll, function(newVal) {
        $timeout(function() {
         element[0].scrollTop = element[0].scrollHeight;
        });
      });
    }
  }
});