angular.module('vwApp').controller('searchMobileController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','localizationService',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,localizationService) {

	var dealerdata =[];
	maps = [];
	/**
	 * All the provinces and cities
	 */
	var map_address = "Beijing";
	$scope.provinces_cities = [];
	$scope.provinces =[];
	$scope.cities =[];
	$scope.dealerTypes =[];
	var map = new BMap.Map("dealer-map");
	$scope.city = {city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915', province_code: '10001', province_name: 'Beijing'}; // get the default city details using API
	$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
	$scope.dealerType = {};	
	
	$scope.temp_province = [];
	
	$scope.location = true;
	var lat = 116.404;
	 var lang = 39.915;
	$scope.offlineOpts = {
			retryInterval: 10000,
			txt: 'Offline Mode'
	};

	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();

	$('select').chosen({
		disable_search:true,
	});
	
	$('a#btn-map').on('click',function(event) {
		event.preventDefault();
		
		var city = $scope.city.city_name;		
		if(city){
			$('#popup-map').show();
			//$('.dealer-mobile-form').hide();
			//map.centerAndZoom(city,11); //跳转城市地图

		}else{
			$('#popup-location').show();
		}
		
	});
	
	$('#btn-map-menu').on('click', function(event) {
		event.preventDefault();
		var obj = $(this).parent('.map-menu');
		if(obj.hasClass('open')){
			obj.removeClass('open');
		}else{
			obj.addClass('open');
		}
		
	});
	
	$('#popup-map').on('click','.popup-map-top a', function(event) {
		event.preventDefault();
		$('.dealer-mobile-form').show();
		$('#popup-map').hide();
	});
	
	
	$scope.getAllcityprovince = function () {
		
		homeService.getAllprovincesAndcities(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;
				var prv= $scope.provinces_cities;
				$scope.searchResult = dealerdata;
				$scope.searchResultCount = dealerdata.length;
				for (var j = 0; j < prv.length; j++) {
					if($scope.temp_province.indexOf(prv[j].province_name) < 0) {
						$scope.temp_province.push(prv[j].province_name);
						$scope.provinces.push(prv[j]);
					}
				}	
				$scope.province = $scope.provinces[0];				
				if($scope.province){										
					$scope.changeCityList();
				}
			}
		}); 
	};
	
	$scope.changeCityList = function() {
		
		$scope.populateCityForSelectedProvince();
		if($scope.cities){
			$scope.city = $scope.cities[0]; // assigning the first city as default			
			localStorage.setItem("city_name",$scope.cities[0].city_name);			
			$scope.searchDealer();
		}
		
	};
	
	
	$scope.populateCityForSelectedProvince = function() {
		var allCities = $scope.provinces_cities;
		console.log("Selected Province>>>>  ");
		console.log($scope.province);
		$scope.cities = [];
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_code === $scope.province.province_code) {
				console.log("Adding  city >>>>  " + i);
				console.log(allCities[i]);
				$scope.cities.push(allCities[i]);
			}
		}
	};
	
	$scope.setSelectedCityProvince = function (city_code) {
		var allCities = $scope.provinces_cities;
		
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].city_code === city_code) {
				$scope.city = allCities[i];							
				break;
			}
		}
		var selCity = $scope.city;	
		$scope.searchDealer();
		$scope.setSelectedProvince(selCity.province_code);
	};
	
	$scope.setSelectedProvince = function (province_code) {
		var uniqueProvinces = $scope.provinces;
		
		for (var i = 0; i < uniqueProvinces.length; i++) {
			if (uniqueProvinces[i].province_code === province_code) {
				$scope.province = uniqueProvinces[i];
				break;
			}
		}
		
		$scope.populateCityForSelectedProvince();
	};
	
	$scope.getDealerTypeData = function() {
		
		homeService.getAlldealerType(function (response){
			if(response.status.status==200){				
				$scope.dealerTypes = response.entities;				
			}
		});
	};
	
	$scope.dealerInfo = function() {
		//load all the cities and provinces
		$scope.translate();
		$scope.getAllcityprovince();
		$scope.getDealerTypeData();
		// detect user location and set default city and province and 
		
		
		
		//showlocation(setUserLocation, setDefaultLocation);	
		//$scope.setMapOptions();
		
	};
	$scope.dealerInfo(); 
	
	$scope.setCity = function (cityName){
		homeService.getCityByName(cityName, function (response){				
			if(response.status.status == 200){
				var cityObj = response.entity;				
				console.log(response);
				console.log(cityObj);
				$scope.setSelectedCityProvince(cityObj.code);
			}
		});		
	};
	
	
	
	
	
	$scope.searchDealer = function () {
		$scope.searchResult = [];
		var selectPrv, selectCity, selectDType = null;
		console.log("In search Dealer ======  ");
		if($scope.province) {
			selectPrv = $scope.province.province_code; 
			console.log(selectPrv);
		}
		if($scope.city) {
			selectCity = $scope.city.city_code;
			localStorage.setItem("city_name",$scope.city.city_name);			
			if($scope.city){
				map_address = $scope.city.city_name;
				lat = $scope.city.latitude;
				lang = $scope.city.longitude;
				console.log(lat+ "  " +lang);				
				}
		}
		if($scope.dealerType) {
			selectDType = $scope.dealerType.code;
			console.log(selectDType);
		}	
		
		console.log("=======================  ");
		
		homeService.getdealerCityProvincedealerTypefilter(selectPrv, selectCity, selectDType, 
				function (response){
					if(response.status.status==200 && response.entities){
						dealerdata = response.entities;
						$scope.searchResult = dealerdata;
						$scope.searchResultCount = $scope.searchResult.length;
						$scope.mapdata();
					}				
			}); 
	};
	
	$scope.mapdata = function()
	{		
		//alert(lat + "  " + lang);
		//map.centerAndZoom(new BMap.Point(lat, lang), 12); 	
		//alert(map_address);
		map.centerAndZoom(map_address,12);
		//map.setCurrentCity("Beijing");          // 设置地图显示的城市 此项是必须设置的
		map.enableScrollWheelZoom(true);		
		for (var i = 0; i <dealerdata.length; i++) {
			var json = dealerdata[i];
			var p0 = json.lattitude;
			var p1 = json.longitude;
			var point = new BMap.Point(p1, p0);
			var myIcon = new BMap.Icon("https://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
				imageOffset: new BMap.Size(0, 0 - i * 25)
			});
			var marker = new BMap.Marker(point, { icon: myIcon });
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
			marker.setLabel(label);
			map.addOverlay(marker);
			label.setStyle({
				color : "#f00", 
				fontSize : "10px", 
				backgroundColor :"0.05",
				border :"0", 
				fontWeight :"bold" 
			});
			(function() {
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click", function() {
					this.openInfoWindow(_iw);
				});
				_iw.addEventListener("open", function() {
					_marker.getLabel().hide();
				})
				_iw.addEventListener("close", function() {
					_marker.getLabel().show();
				})
				label.addEventListener("click", function() {
					_marker.openInfoWindow(_iw);
				})
				if (!!json.isOpen) {
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()
		}
	
	//Create a marker popup window
		function createInfoWindow(i) {
			var opts = {
					width : 550,     // 信息窗口宽度
					height: 300,     // 信息窗口高度
					//title : "信息窗口" , // 信息窗口标题
					enableMessage:true//设置允许信息窗发送短息
				};
	       
			var json = dealerdata[i];
			 var deta = JSON.stringify(json);
			var content = "<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
			"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
			"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'>" +
			"<div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>" +
			"销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(angularweb/assets/images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>" +
					"进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;' onclick='sendDealerId("+dealerdata[i].id+")'>预约试驾</a></div><div style='float:left;padding-left:20px;'>" +
					"<img src='angularweb/assets/images/dealer-popup-img.jpg' /><div style='margin-top:12px;text-align:center;height:64px; line-height:44px;color:#1597d9;font-size:18px;position: relative;'><input type='tel' id='tel"+i+"' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'>" +
					"<a  style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo("+i+","+deta+")'>发送</a><p class='warning'></p></div></div></div>";
			var iw = new BMap.InfoWindow(content, opts);
			
			return iw;
		}
	//Create a Icon
	function createIcon(json) {
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
	};
		
	$scope.getBgClass = function (row){
		if(row%2 == 1){
			return "li-bg";
		} else {
			return "";
		}
	};
	
	$('.line.clause').on('click', 'a', function(event) {
		event.preventDefault();
		$('#popup-clause').show();
	});

	$('#popup-ok').on('click', 'a.ok', function(event) {
		$(this).parents('#popup-ok').hide();
		$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		$('input').val('');
	});
	$('.popup').on('click', 'a.popup-close', function(event) {
		$(this).parents('.popup').hide();
		$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		$('input').val('');
	});

	
	//Create a marker popup window
/*	function createInfoWindow(i) {
		var opts = {
				width : 550,     // 信息窗口宽度
				height: 300,     // 信息窗口高度
				//title : "信息窗口" , // 信息窗口标题
				enableMessage:true//设置允许信息窗发送短息
			};
       
		var json = dealerdata[i];		
		 var deta = JSON.stringify(json);
		var content = "<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
		"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
		"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'>" +
		"<div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>" +
		"销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(angularweb/assets/images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>" +
				"进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;'  onclick='sendDealerId("+dealerdata[i].id+")'>预约试驾</a></div><div style='float:left;padding-left:20px;'>" +
				"<img src='angularweb/assets/images/dealer-popup-img.jpg' /><div style='margin-top:12px;text-align:center;height:64px; line-height:44px;color:#1597d9;font-size:18px;position: relative;'><input type='tel' id='tel"+i+"' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'>" +
				"<a  style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo("+i+","+deta+")'>发送</a><p class='warning'></p></div></div></div>";
		var iw = new BMap.InfoWindow(content, opts);
		
		return iw;
	}*/
	//Create a Icon
	function createIcon(json) {		
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
}]);