angular.module('vwApp').controller('recruitLoginController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','localizationService','AuthService', 'apiFactory', function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,testDriveService,localizationService,AuthService, apiFactory) {
	

	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
			if(localStorage.getItem("loginpageorgin") == null){
				localStorage.setItem("loginpageorgin","login");
			}
		});			   	 
	};
	
	$scope.translate();
	
    $scope.recruitDetailsPageUrl = apiFactory.getRecruitDetailsPageUrl();
    $scope.recruitmentHomeUrl = apiFactory.getRecruitmentHomeUrl();
		
	$scope.registerClicked = function() {
		$state.go('dealerRecruitmentReg');
	};	
		
	$scope.setRecruitmentType = function (val) {
		apiFactory.setRecruitmentType(val);
	};	
	
	function isPhoneNum(val){
		var phonenum = val;
		var myreg = /^(((1[0-9]{0})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{10})$/; 
		/*var myreg = /^[0-9]*$/;*/
		if(!myreg.test(phonenum)){ 
			return false; 
		}else{
			return true;
		}
	}
	
	$scope.login = function(){
		var tel = $('input#tel').val(),
		password = $('input#password').val();
		if(!isPhoneNum(tel)) {
			$('input#tel').parent('.line').addClass('warning').find('p.warning').show()
			return;
		}else{
			$('input#tel').parent('.line').removeClass('warning').find('p.warning').hide();
		}
		if(password==''){
			$('input#password').parent('.line').addClass('warning').find('p.warning').show();
			return;
		}else{
			$('input#password').parent('.line').removeClass('warning').find('p.warning').hide();
		}
		if(($scope.user.username != '')  && ($scope.user.password != '')){
		$scope.user.grant_type ="password";
			AuthService.authenticateUser($scope.user)
			.success(function(data){
	    // alert('controller'+angular.toJson(data));
	       localStorage.setItem("loginOauthToken",data.access_token);
	       localStorage.setItem("currentUser", JSON.stringify(data.User));
	       if(localStorage.getItem("loginpageorgin") == "details"){
	    	   localStorage.removeItem("loginpageorgin");
	    	   $state.go('dealerRecruitmentApply');
	       }
	       else {
	    	   $state.go('recruitRecord');
	       }
	       
		});
	}
	};
	
	$scope.logout = function(){
		 localStorage.removeItem("loginOauthToken");
		 localStorage.removeItem("currentUser");
		 $state.go('recruitLogin');
	};
	
}]);
