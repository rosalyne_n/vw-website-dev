angular.module('vwApp').controller('financeController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','financeService','localizationService',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,testDriveService,financeService,localizationService) {

	$scope.loan_tenure="12";
	$scope.down_payment="20";
	$scope.default_str="一 一";
	$scope.carModelImage = "car-bg.png";
	$scope.recommended_retail_price="";

	$scope.selected_series={};
	$scope.selected_model={};
	$scope.selected_finance_institution={};
	$scope.selected_finance_product={};
	var allModels =[];
	$scope.carSeries=[];
	$scope.carModel=[];
	$scope.finance_institutes=[];
	$scope.products={};
	$scope.data = {};
	$scope.values = {};
	$scope.is_submit=false;
	$scope.rdSel = false;
	$scope.is_apply = false;

	$scope.sliderSelectedImg = 'angularweb/assets/images/finance/drag-selected.png';
	$scope.sliderImg = 'angularweb/assets/images/finance/drag.png';

	$scope.down_payment_rates = [{'value': 20, 'selected': true, 'src': $scope.sliderSelectedImg}, 
	                             {'value': 30, 'selected': false,  'src': $scope.sliderImg},
	                             {'value': 40, 'selected': false,  'src': $scope.sliderImg},
	                             {'value': 50, 'selected': false, 'src': $scope.sliderImg},
	                             {'value': 60, 'selected': false, 'src': $scope.sliderImg},
	                             {'value': 70, 'selected': false,  'src': $scope.sliderImg},
	                             {'value': 80, 'selected': false, 'src': $scope.sliderImg}];

	$scope.loan_terms = 		[{'value': 12, 'selected': true, 'src': $scope.sliderSelectedImg}, 
	                    		 {'value': 18, 'selected': false,  'src': $scope.sliderImg},
	                    		 {'value': 24, 'selected': false,  'src': $scope.sliderImg},
	                    		 {'value': 36, 'selected': false, 'src': $scope.sliderImg},
	                    		 {'value': 48, 'selected': false, 'src': $scope.sliderImg},
	                    		 {'value': 60, 'selected': false,  'src': $scope.sliderImg}];

	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();	
	
	 $scope.contentResult = function() {		
		if($scope.selected_model.code && $scope.selected_series.code && $scope.selected_finance_institution.code && $scope.selected_finance_product.code && $scope.emiPopulated.down_payment_amount != $scope.default_str){			
			var price = Number($('#price').val());
			var $contentResult = $('.content-result');
			$scope.is_submit=true;
			var data;

			var m = Math.floor(Math.random()*2);
			data = $scope.emiPopulated;
			/*if(m==1){
				data = [20000,50000,'12个月',10000,2000]
			}else{
				data = [30000,60000,'18个月',20000,40000]
			}*/
			$('.content-result').find('.line').each(function(index, el) {
				$(this).find('span').text(data[index]);
			}).parents('.content-result').find('a').removeClass('noClick')	
		}else{
			console.log('no')
		}
		

	}
	
	
	/***Down payment slider functions*/
	$scope.getDownPaymentSliderClass = function(d, index){
		var className = ""; 
		if(index ==  $scope.down_payment_rates.length -1){
			className = "end ";
		}
		if(d.selected){
			className += "selected";
		}

		return className;
	};


	$scope.setDownPaymentSelected = function(index){
		for(var i = 0; i < $scope.down_payment_rates.length; i++){
			if(i == index) {
				$scope.down_payment_rates[i].selected = true;	
				$scope.down_payment_rates[i].src = $scope.sliderSelectedImg;	   
				$scope.down_payment=$scope.down_payment_rates[i].value;
			}
			else {
				$scope.down_payment_rates[i].selected = false; 
				$scope.down_payment_rates[i].src = $scope.sliderImg
			}
		}
		$scope.populateEmi();
		$scope.allFinanceResult();
	};
	/***
	 * Loan terms slider functions
	 */
	$scope.getLoanTermsSliderClass = function(d, index){
		var className = ""; 
		if(index ==  $scope.loan_terms.length -1){
			className = "end ";
		}
		if(d.selected){
			className += "selected";
		}

		return className;
	};	


	$scope.settLoanTermsSelected = function(index){
		for(var i = 0; i < $scope.loan_terms.length; i++){
			if(i == index) {
				$scope.loan_terms[i].selected = true;	
				$scope.loan_terms[i].src = $scope.sliderSelectedImg;	   
				$scope.loan_tenure=$scope.loan_terms[i].value;
			}
			else {
				$scope.loan_terms[i].selected = false; 
				$scope.loan_terms[i].src = $scope.sliderImg
			}
		}
		$scope.populateEmi();
		$scope.allFinanceResult();
	};

	$scope.financialInstitutions = function(){
		financeService.getAllFinancialInstitutions(function (response){
			if(response.status.status==200){
				$scope.finance_institutes = response.entities;
				console.log($scope.finance_institutes);				
			}
		});  
	};

	$scope.populateCarSeries = function() {
		$scope.financialInstitutions();
		testDriveService.getCarSeries(function (response){
			if(response.status.status==200){
				$scope.carSeries = response.entities;				
			}
		});  
	}; 


	$scope.populateEmi = function()	{	
		console.log($scope.selected_model);
		console.log($scope.selected_finance_product);
		console.log($scope.recommended_retail_price);
		console.log($scope.loan_tenure);
		console.log($scope.down_payment);

		if($scope.selected_model && $scope.selected_finance_product 
				&& $scope.recommended_retail_price && $scope.loan_tenure && $scope.down_payment){
			$scope.data.car_model = $scope.selected_model.code;
			$scope.data.financial_product= $scope.selected_finance_product.code;	
			$scope.data.loan_tenure  = parseFloat($scope.loan_tenure, 10);
			$scope.data.down_payment  = parseFloat($scope.down_payment) * 0.01;
			$scope.data.total_payment = parseFloat($scope.recommended_retail_price);
			console.log($scope.data);
			financeService.loanCalculate($scope.data, function (response){
				if(response.data.status.status==200){
					$scope.emiPopulated = response.data.entity;						
					$scope.emiPopulated.loan_tenure = $scope.emiPopulated.loan_tenure + $scope.translation.finance.months;
					console.log($scope.emiPopulated);
					$scope.contentResult();
				}
				else {
					$scope.resetResult();
				}
			});

		} else {
			$scope.resetResult();
		}
	};

	$scope.allFinanceResult = function()	{	
		console.log($scope.selected_model);
		console.log($scope.selected_finance_product);
		console.log($scope.recommended_retail_price);
		console.log($scope.loan_tenure);
		console.log($scope.down_payment);

		if($scope.selected_model  && $scope.recommended_retail_price && $scope.loan_tenure && $scope.down_payment){
			$scope.data.car_model = $scope.selected_model.code;
			$scope.data.loan_tenure  = parseFloat($scope.loan_tenure, 10);
			$scope.data.down_payment  = parseFloat($scope.down_payment) * 0.01;
			$scope.data.total_payment = parseFloat($scope.recommended_retail_price);
			console.log($scope.data);
			financeService.allfinancial($scope.data, function (response){
				if(response.data.status.status==200){

					$scope.allfinances = response.data.entities;	
					// $scope.allfinance.loan_tenure = $scope.emiPopulated.loan_tenure + $scope.translation.finance.months;
				}
				/* else {
					 $scope.resetResult();
				 }*/
			});

		} /*else {
			 $scope.resetResult();
		 }*/
	};
	$scope.resetResult = function () {
		$scope.emiPopulated = {};
		$scope.emiPopulated['total_amount'] = $scope.default_str;
		$scope.emiPopulated['down_payment_amount'] = $scope.default_str;
		$scope.emiPopulated['loan_tenure']= $scope.default_str;
		$scope.emiPopulated['interest_amount']=$scope.default_str;
		$scope.emiPopulated['monthly_installment'] = $scope.default_str;
		$scope.emiPopulated['rr_price'] = $scope.default_str;
	};

	$scope.carSeriesChange = function()	{		
		console.log($scope.selected_series.code);
		financeService.getallModelsBySeries($scope.selected_series.code, function (response){
			if(response.status.status==200){				
				$scope.carModels = response.entities;
				if($scope.carModels){
					$scope.selected_model=$scope.carModels[0];
					if($scope.carModels[0].image_url != null)
					{
						$scope.carModelImage = $scope.carModels[0].image_url;
					}
					else
					{
						$scope.carModelImage = "car-bg.png";
					}
					$scope.carModelChange();
				}
				$scope.contentResult();
			}
		});
	};

	$scope.carModelChange = function() {

		var model = $scope.selected_model;
		$scope.recommended_retail_price = model.recommended_retail_price;
		if(model.image_url!=null){
			$scope.carModelImage = allModels[i].image_url;
		}
		else
		{
			$scope.carModelImage = "car-bg.png";
		}

		$scope.populateEmi();
		$scope.allFinanceResult();
		$scope.contentResult();
	};

	$scope.showOfficialPrice = function () {
		var showPrice = false;
		console.log($scope.selected_model);
		if($scope.selected_model && $scope.selected_model.recommended_retail_price) {
			showPrice = true;
		}
		return showPrice;
	};

	$scope.financialInstituteChange = function () {
		var financeInst = $scope.selected_finance_institution;

		financeService.getAllFinancialProductsByInstitution(financeInst.code,function (response){
			if(response.status.status==200){
				$scope.products = response.entities;
				if($scope.products){
					$scope.selected_finance_product=$scope.products[0];
					console.log($scope.selected_finance_product);
					$scope.financialProductChange();
				}
				$scope.contentResult();
			}
		});
	};
	$scope.financialProductChange = function(){
		$scope.populateEmi();
		$scope.contentResult();
	};


	$scope.resetResult();



	function selectWidth(){
		if($(window).width()<640) {
			$('.chosen-container ').css('width','98%');
		}else{
			$('.chosen-container ').css('width','48%');
		}
	};

	selectWidth();

	$(window).resize(function(event) {
		selectWidth();
	});


	$scope.getBgClass = function (row){
		if(row%2 == 1){
			return "bg";
		} else {
			return "";
		}
	};
	$scope.allfinance = function(){
		if($scope.is_submit){
		angular.element("#all-finance").show();
		}
	};

	$scope.close = function()
	{
		angular.element("#all-finance").hide();
	};
	$scope.radClass = function(rec) {
		var radioClass = "check";	
		if($scope.selectedRow != undefined ){
			if($scope.selectedRow.financial_product_code == rec.financial_product_code){
				radioClass = radioClass + " selected";
			}
		}
		if(radioClass === "check")
			{
			$scope.rdSel = false;
			}
		else
			{
			$scope.rdSel = true;
			}

		return radioClass;

	};
	$scope.selectRow = function (rec) {
		$scope.is_apply = true;
		$('#apply_loan').parents('.content').find('a').removeClass('noClick');
		$scope.selectedRow = rec;
		console.log($scope.selectedRow);
	};	

	$scope.applyloanpopup = function()
	{
		if($scope.rdSel && $scope.is_apply){
			var j =$scope.selectedRow;
			/*$state.go('testdrive', );*/
			$state.go('applyLoan',{obj:j});
		}
		
	};
	$scope.applyfrompage = function()
	{

		if($scope.selected_model && $scope.selected_finance_product && $scope.recommended_retail_price && $scope.loan_tenure && $scope.down_payment){
			$scope.populateEmi();	
			$scope.values.series_code = $scope.selected_series.code;
			$scope.values.series_name = $scope.selected_series.name;
			$scope.values.modal_Code =  $scope.selected_model.code;
			$scope.values.model_name =  $scope.selected_model.name;
			$scope.values.financial_Institution_code = $scope.selected_finance_institution.code;
			$scope.values.financial_institution_name = $scope.selected_finance_institution.bank_name;
			$scope.values.financial_product_code = $scope.selected_finance_product.code;
			$scope.values.financial_product_name=$scope.selected_finance_product.name;
			$scope.values.down_payment_rate = $scope.emiPopulated.down_payment_rate;
			$scope.values.down_payment_amount = $scope.emiPopulated.down_payment_amount;
			$scope.values.monthly_installment = $scope.emiPopulated.monthly_installment;
			$scope.values.fee_rate =$scope.emiPopulated.fee_rate;
			$scope.values.loan_amount = $scope.emiPopulated.loan_amount;
			$scope.values.loan_tenure = $scope.loan_tenure;
			$scope.values.rr_price = $scope.emiPopulated.rr_price;
			$scope.values.interest_amount =  $scope.emiPopulated.interest_amount;
			var j= $scope.values;
			
			if($scope.emiPopulated.interest_amount != $scope.default_str)
				{
				$state.go('applyLoan',{obj:j});
				}
		
		}

	};

}]);
