angular.module('vwApp').controller('dealerRecruitDetailsController',
		['$scope','$localStorage','$state','localizationService','dealerRecruitService','homeService','testDriveService','apiFactory', function($scope,$localStorage,$state,localizationService,dealerRecruitService,homeService,testDriveService, apiFactory) {
			localStorage.setItem("loginpageorgin","login");
			$scope.data ={};
			$scope.pageno = 1; // initialize page no to 1
			$scope.total_count = 100;
			$scope.itemsPerPage = 10;
			$scope.province ="";	
			$scope.city="";
			$scope.recruitcity =[];
			$scope.cities = [];
			$scope.recruitProvinces = [];
			var citySize = {};
			$scope.checkbox= false;
			$scope.count = [];
			$scope.translate = function(){	
				localizationService.getBundle(function(data){
					$scope.translation = data;
				});			   	 
			};
			$scope.translate();
			
			$scope.isUserLoggedIn = homeService.isUserLoggedIn();	
			
			$scope.registerClicked = function() {
				$state.go('dealerRecruitmentReg');
			};
			
			$scope.loginClicked = function() {
				$state.go('recruitLogin');
			};
			
			$scope.logoutClicked = function() {
				homeService.logoutUser();		
			};
			
			$scope.myRecordClicked = function(){
				$state.go('recruitRecord');
			};			
			
			$scope.recruitCityDetails = []; //declare an empty array
			$scope.pageno = 1; // initialize page no to 1
			$scope.total_count = 0;
			$scope.itemsPerPage = 3; //this could be a dynamic value from a drop down

			$scope.recruitmentType = apiFactory.getRecruitmentType();			
			$scope.recruitmentHomeUrl = apiFactory.getRecruitmentHomeUrl();
			
			$scope.getNavClass = function(val){
				var navClass = "";
				if($scope.recruitmentType == val){
					navClass = "active";
				} 
				return navClass;
				
			};
			
			$scope.setRecruitNetworkType = function(val){
				$scope.recruitmentType = val;
				apiFactory.setRecruitmentType(val);
				$scope.pageno=1;
				$scope.getData($scope.pageno);
			};

			$scope.allprovince = function()
			{
				dealerRecruitService.getRecruitCityByAllProvince(function (response){
					$scope.recruitProvinces = response.entities;
					$scope.filteredprovinces = [];
					var prv = $scope.recruitProvinces;
					for(var h=0;h<prv.length;h++)
					{
						if($scope.filteredprovinces.indexOf(prv[h].province) < 0) {
							$scope.filteredprovinces.push(prv[h].province);
						}
					}
				});

			};

			$scope.provinceRecruitChange = function()
			{
				$scope.recruit_city ="";
				$scope.recruitcity  = [];
				var ct=[];
				selectProvince = $scope.recruit_province;
				//$scope.getData($scope.pageno);
				allCities = $scope.recruitProvinces;
				$scope.filteredCities=[];
				$scope.recruitcity=[];
				for (var i = 0; i < allCities.length; i++) {
					if (allCities[i].province === selectProvince) {
						if($scope.filteredCities.indexOf(allCities[i].city) < 0) {
							$scope.filteredCities.push(allCities[i].city);
							$scope.recruitcity.push(allCities[i]);
						}
					}
				}
				$scope.recruit_city = $scope.filteredCities[0];
				$scope.cityChange();
		};
		$scope.cityChange = function(){
			var selectcity = $scope.recruit_city ;
			//$scope.getData($scope.pageno);
			alldistricts = $scope.recruitProvinces;
			$scope.filteredDistricts=[];
			$scope.recruitdistrict=[];
			for (var i = 0; i < alldistricts.length; i++) {
				if (alldistricts[i].city === selectcity) {
					if($scope.filteredDistricts.indexOf(alldistricts[i].district) < 0) {
						$scope.filteredDistricts.push(alldistricts[i].district);
						$scope.recruitdistrict.push(alldistricts[i]);
					}
				}
			}
			$scope.recruit_select_district = $scope.recruitdistrict[0];
		};
		
		$scope.filterData = function (){
			$scope.getData(1);
		};

			$scope.getData = function(pageno){ // This would fetch the data on page change.
				//In practice this should be in a factory.
				$scope.currentPage = pageno;
				$scope.data.rowsPerPage = 3;
				$scope.data.page = pageno;
				$scope.itemsPerPage = $scope.data.rowsPerPage;

				$scope.allprovince();
				$scope.recruitCityDetails = [];  
				$scope.data.province = $scope.recruit_province;
				$scope.data.city  = $scope.recruit_city;
				$scope.data.district = $scope.recruit_select_district;
				$scope.data.recruitmentType = $scope.recruitmentType;
				dealerRecruitService.getRecruitCityDetails($scope.data,function (response){
					if(response.data.status.status==200){
						$scope.recruitCityDetails = response.data.entity; 
						$scope.selectedRow = $scope.recruitCityDetails.recruitingCities[0];
						console.log("In getData >>>>>");
						console.log($scope.selectedRow);
					}
				});
			};
			$scope.getData($scope.pageno);

			(function(){

				$('#recruit ul li h3').unbind().click(function(event) {
					//event.preventDefault();

					if($(this).parent('li').hasClass('show')){
						$(this).parent('li').removeClass('show');

					}else{
						$(this).parent('li').addClass('show').siblings('li').removeClass('show');
						//window.location.hash = $(this).parents('li').index();
					}


				});

				
				/*$('.city-list .radio').on('click', function(event) {
					event.preventDefault();
					if($(this).hasClass('selected')) return;
					$(this).addClass('selected').parents('tr').siblings('tr').find('.radio').removeClass('selected');
				});*/


			})();
			$scope.expandTab4 = function()
			{
				$('#tab4').parent('li').addClass('show').siblings('li').removeClass('show');
			};
			$scope.exapndTab3 = function()
			{
				$('#tab3').parent('li').addClass('show').siblings('li').removeClass('show');
			};
			
			$scope.radClass = function(rec) {
				var radioClass = "radio";	
						
				if($scope.selectedRow.id == rec.id){
					radioClass = radioClass + " selected";
				}
				return radioClass;
			    				
			};
			
			$scope.selectRow = function (rec) {
				$scope.selectedRow = rec;
			};			
			
			      
			$scope.registerPage = function(){
				$('#save-ok').hide();
				$state.go('dealerRecruitmentReg');
			
			};
			$scope.loginPage = function(){
				$('#save-ok').hide();
				$state.go('recruitLogin');
			
			};
			$scope.popupClose = function(){
			/*	localStorage.removeItem('loginpageorgin');
				localStorage.removeItem('registerOrgin');*/
				$('div#save-ok').hide();
				
			};	
			
			$scope.getTrClass = function(index) {
				if(index % 2 == 0) {
					return "bg";			
				}
				else {
					return "";
				}
				
			};

			$scope.apply = function() {
				//localStorage.removeItem('loginOauthToken');
				//alert(localStorage.getItem("loginOauthToken"));	
				var recCity = $scope.selectedRow;
				var selectedRecruitCity = {
						selected_province: recCity.province,
						selected_city: recCity.city,
						selected_district: recCity.district,
						recruitment_type: recCity.recruitmentType,		
						declared_city:recCity.id
				};
				// to be used by the recruit_apply page to prefill
				localStorage.setItem("selectedRecruitCity", JSON.stringify(selectedRecruitCity));		

				if(localStorage.getItem("loginOauthToken") != null) {
					
					$state.go('dealerRecruitmentApply');
				}
				else {
					localStorage.setItem("loginpageorgin","details");
					/*localstorage.setItem("registerOrgin","details");*/
					$('#save-ok').show();
				}
				
			};
			
			
		}]);









