angular.module('vwApp').controller('carConfigController',
		['$scope','$rootScope', '$sce', 'localizationService','apiFactory', '$stateParams','carService',
		  function($scope, $rootScope, $sce, localizationService, apiFactory, $stateParams, carService) {
		
	
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();	
	
	//$scope.modelCode = $stateParams.modelCode;
	$rootScope.pageTitle = $stateParams.pageTitle || "FAW CN Wolkswagen app";
		
	$scope.carConfigDetails = {};
	
	$scope.getConfigData = function (){
		//console.log("IN getConfigData ------");	
		//console.log($stateParams);		
		var modelCode = $stateParams.modelCode;
		//console.log("modelCode  ------");	
		//console.log(modelCode);
		carService.getCarModelConfiguration(modelCode, function (response){
			if(response.status.status == 200){		
				//console.log("iN rrrrrrrrrrrrr >>>>>");
				$scope.carConfigDetails = response.entity;
				//console.log($scope.carConfigDetails);
				for(var i=0;i<$('.config_table tr').length;i++){
					if(i%2 == 0){
						$($('.config_table tr')[i]).css('background','#fff');	
					}
					if(i%2 == 1){
						$($('.config_table tr')[i]).css('background','#f3f5f6');	
					}
				}
			}
		});
	};		
		
	$scope.getConfigData();
	
	$scope.renderHtml = function(html_code){
	   return $sce.trustAsHtml(html_code);
	};
	
	/*$scope.getTrClass = function(index) {
		if(index % 2 == 0) {
			return "config_tr_br_even";			
		}
		else {
			return "config_tr_br_odd";
		}		
	};
	
	$scope.getTrStyle = function(index) {
	    var even = "background-color:#fff";
		var odd = "background-color:#f3f5f6";
		if(index % 2 == 0) {
			return even;			
		}
		else {
			return odd;	
		}		
	};*/
	
  /*for(var i=0;i<$('.config_table tr').length;i++){
		if(i%2 == 0){
			$($('.config_table tr')[i]).css('background','#fff');	
		}
		if(i%2 == 1){
			$($('.config_table tr')[i]).css('background','#f3f5f6');	
		}
	}
	*/
	
	function scrollOffset(top,time){
		$('html,body').stop().animate({scrollTop:top},time);
	}

	var flag = false;
	//pc滚动
	if($(window).width() > 640){
		
		$(window).scroll(function(){
			var Dtop = $(document).scrollTop();
			if(Dtop !=0 ){
				flag = true;
				
				$('.top_default').hide();
				$('.top_change').show();
				
				$('.config_content').css('padding-top','320px');
				$('.btn-close').hide();
			}
			if(Dtop == 0){
				flag = false;
				
				$('.top_default').show();
				$('.top_change').hide();
				
				$('.config_content').css('padding-top','499px');
				$('.btn-close').show();
			}
			
			var top = $(this).scrollTop();
			$(".anchorlink").removeClass("active");
		
			var s = [];
			for(i=0;i<$(".anchorlink").size();i++){
				var sid = $(".anchorlink").eq(i).attr("href");
				var _top = $(sid).offset().top-320;
				var b = _top - top;
				if(b<0) b=$(".anchorlink").size()-i;
				s.push(b);
			}
			//console.log(s);
			var f = s.indexOf(Math.min.apply(Math, s));
		
			$(".anchorlink").eq(f).addClass("active");
		});
	}
	//手机滚动
	var scroll = function(){
		var Dtop = $(document).scrollTop();
		if(Dtop > 460 ){
			flag = true;
			$('.config_top').css('position','fixed');
			$('.config_top').css('top','0');
			
			$('.top_default').hide();
			$('.top_change').show();
			
			
			$('.config_content').css('padding-top','600px');
		}
		if(Dtop < 460){
			flag = false;
			$('.config_top').removeAttr('style');
			$('.config_top').css('position','static');
			
			$('.top_default').show();
			$('.top_change').hide();
			
			
			$('.config_content').css('padding-top','0px');
			//$('.config_content').css('padding-top','400px');
		}
		
		var top = $(this).scrollTop();
		$(".m_anchorlink").removeClass("active");

		var s = [];
		for(i=0;i<$(".m_anchorlink").size();i++){
			var sid = $(".m_anchorlink").eq(i).attr("href");
			var _top = $(sid).offset().top-250;
			var b = _top - top;
			if(b<0) b=$(".m_anchorlink").size()-i;
			s.push(b);
		}
		//console.log(s);
		var f = s.indexOf(Math.min.apply(Math, s));

		$(".m_anchorlink").eq(f).addClass("active");
		var text = $(".select_list .active").html();
		$('.btn-select').html(text);
	}
	if($(window).width() < 641){
		$(window).bind({'touchstart':function(){
			scroll();
		},'touchmove':function(){
			scroll();
		},'touchend':function(){
			scroll();
		},'scroll':function(){
			scroll();
		}});
	}

	$(".anchorlink").click(function(){
		$(".anchorlink").removeClass('active');
		$(this).addClass('active');
		
		var sid = $(this).attr("href");
		if(flag){
			var top = $(sid).offset().top-250;
		}else{
			var top = $(sid).offset().top-400;	
		}
		
		scrollOffset(top,0);
		return false;
	});
	$(".m_anchorlink").click(function(){
		$(".m_anchorlink").removeClass('active');
		$(this).addClass('active');
		
		var sid = $(this).attr("href");
		if(flag){
			var top = $(sid).offset().top-200;
		}else{
			var top = $(sid).offset().top-250;	
		}
		
		scrollOffset(top,0);
		return false;
	});

	var canSlide = true;
	$('.btn-select,.select_icon').click(function(){
		if(canSlide){
			$('.select_list').slideDown('fast');
			$('.select_icon').css({'transform':'rotate(0deg)','-webkit-transform':'rotate(0deg)','-moz-transform':'rotate(0deg)'});
			canSlide = false;
			
			var Dtop = $(document).scrollTop();
		}else{
			$('.select_list').slideUp('fast');
			$('.select_icon').css({'transform':'rotate(180deg)','-webkit-transform':'rotate(180deg)','-moz-transform':'rotate(180deg)'});
			canSlide = true;	
			
			var Dtop = $(document).scrollTop();
		}
	});

	$('.select_list li a').click(function(){
		canSlide = true;
		var text = $(this).html();
		$('.btn-select').html(text);
		$('.select_list').hide();
		
		$('.select_icon').css({'transform':'rotate(180deg)','-webkit-transform':'rotate(180deg)','-moz-transform':'rotate(180deg)'});
	});

}]);
