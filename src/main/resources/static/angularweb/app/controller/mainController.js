angular.module('vwApp').controller('mainController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','localizationService',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,financeService,testDriveService,localizationService) {

	$scope.isMobileAgent = function(){

		$scope.isMobile = (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/iPhone|iPad|iPod/i) || navigator.userAgent.match(/Opera Mini/i)
				|| navigator.userAgent.match(/IEMobile/i));
	}	
}]);