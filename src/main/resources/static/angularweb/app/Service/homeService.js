myApp.service('homeService', ['apiFactory', '$localStorage', '$state', function(apiFactory, $localStorage, $state ){
	var param = '';
	this.currentUser = null;
   	// User CRUD operation
   	this.getDealerData = function(callback) {
		   	apiFactory.getLang('getalldealers',param).then(function(results) {
	        callback(results);
	    });
   	};
   	this.getAllProvince = function(callback) {
		apiFactory.getLang('getallprovinces' , param).then(function(results) {
	        callback(results);
	    });
   	};
 	this.getAllcities = function(callback) {
	  	apiFactory.getLang('getallcities',param).then(function(results) {
	        callback(results);
	    });
   	};
   	this.getAllprovincesAndcities = function(callback) {
	  	apiFactory.getLang('getallprovinceandcities', param).then(function(results) {
	        callback(results);
	    });
   	};
   	this.getDealerByProvince = function(province,callback) {
		if(province){
			param = '?province=' + province;
		}
	   	apiFactory.getLang('filterdealerbyprovince' , param).then(function(results) {
	        callback(results);
	    });
   	};
 	this.getdealerCityProvincefilter = function(city_code,callback) {
		if(city_code){
			param = '?city_code=' + city_code;
		}
	   	apiFactory.getLang('filterdealerbycity' , param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.getAlldealerType =  function(callback){
	   	apiFactory.getLang('getallContactTypes',param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.getdealerCityProvincedealerTypefilter = function(province_code,city_code,type,callback) {
   		
		if(province_code){
			param = '?province=' + province_code;
		}
		
		if(city_code){
			param = param +'&city=' + city_code;
		}
		
		if(type){
			param = param +'&dealer_type_code=' + type;
			apiFactory.getLang('filterdealer' , param).then(function(results) {
		        callback(results);
		    });
		} else {
			if(city_code){
				var city = '?city_code=' + city_code;
				apiFactory.getLang('filterdealerbycity' , city).then(function(results) {
			        callback(results);
			    });
			}
		}
		
	   	
   	};
   	
   	this.getExternal = function(lat,lang,callback){
   		var param = '?ak=4U6z88V8GZTUyvT6QnMUczUzQetGe5bs';
   		latlang = lat+','+lang;
   		param = param + '&callback =' + "renderReverse";
   		param =param+ '&location=' + latlang;
   		param = param + '&output=json';
   		param = param+ '&pois=0';
	   	apiFactory.getExt('http://api.map.baidu.com/geocoder/v2/' , param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.getCityByName = function(city_name, callback){
   		if(city_name){
			param = '?name=' + city_name;
		}
				
	   	apiFactory.get('getCityByName' + param).then(function(results) {
	        callback(results);
	    });
   	};
   	this.getAllDistrictCityProvince = function(callback) {
	  	apiFactory.getLang('getAllDistrictCityProvince', param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.isUserLoggedIn = function(){
   		if(localStorage.getItem("currentUser")){
   			this.currentUser = localStorage.getItem("currentUser");
   			return true;
   		}else {
   			return false;
   		}
   		
   	};
   	this.getCurrentUser = function (){
   		if(localStorage.getItem("currentUser")){
   			var userStr = localStorage.getItem("currentUser");
   			this.currentUser = JSON.parse(userStr);
   		}
   		return this.currentUser;
   	};
   	
   	this.getSelectedRecruitingCity = function (){
   		if(localStorage.getItem("selectedRecruitCity")){
   			return JSON.parse(localStorage.getItem("selectedRecruitCity"));
   		}	
   	};
   	
   	this.clearSelectedRecruitingCity = function (){
   		if(localStorage.getItem("selectedRecruitCity")){
   			localStorage.removeItem("selectedRecruitCity");
   		}   		
   	};
   	
   	this.getDealerCityProvinceNames = function(callback) {
	  	apiFactory.getLang('getDealerCityProvinceNames', param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.logoutUser = function(){
   		if(localStorage){
   			
   			if(localStorage.getItem("loginOauthToken")){   		
   				localStorage.removeItem("loginOauthToken");
   			}
   			if(localStorage.getItem("currentUser")){   		
   				localStorage.removeItem("currentUser");
   			}   			
   		}
   		$state.go('recruitLogin');
   	};

	this.sendSms = function(data,callback){
		if(data)
			{
			apiFactory.postjsonupload('sendsms'+param, data).then(function(results) {
				callback(results);
			});
			}
	};
}]);