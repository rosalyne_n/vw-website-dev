myApp.service('localizationService',['$http','$resource','apiFactory',function ($http,$resource,apiFactory) {
		var service={
	
			getBundle:getBundle
			
		};return service;
	
	function getBundle(callback) {
		if(apiFactory.languageCode=="en"){	
			var languageFilePath = 'angularweb/app/resources/lang_en.json';
			}else
				{
				var languageFilePath = 'angularweb/app/resources/lang_cn.json';
				}
	
			$resource(languageFilePath).get(function (response) {
				console.log(response);
			callback(response);			
		});
 }
		
}]); 