myApp.service('carService', ['apiFactory', '$localStorage',  function(apiFactory, $localStorage ){
	var param = '';
	
   	this.getCarModelConfiguration = function(modelcode,callback) {
		
		var param = '?modelCode=' + modelcode;
		
	   	apiFactory.getLang('getCarConfigurationParameters' , param).then(function(results) {
	        callback(results);
	    });
   	};
 	
   
   	this.getCarComparisonModelsDetails = function(model1, model2 , model3, callback) {
   		
		param = '?modelCode1=' + model1;		
		
		if(model2){
			param = param +'&modelCode2=' + model2;
		}
		
		if(model3){
			param = param +'&modelCode3=' + model3;
			
		} 	
		apiFactory.getLang('getCarComparisonModelsDetails' , param).then(function(results) {
			        callback(results);
		 });	
		
	   	
   	};
   	this.getAllCarSeriesAndModels = function (callback) {
   		apiFactory.getLang('getAllCarSeriesAndModels' , param).then(function(results) {
	        callback(results);
   		});
   
   	};
   	
   

}]);